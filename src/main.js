import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import Vuelidate from 'vuelidate'
import * as uiv from 'uiv'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueSelect from 'vue-cool-select'
import i18n from '@/plugins/i18n';

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

var VueCookie = require('vue-cookie');

library.add(fas)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(VueAxios, axios)
Vue.use(BootstrapVue)
Vue.use(Vuelidate)
Vue.use(uiv, { prefix: 'uiv' })
Vue.use(VueCookie);
Vue.use(VueSelect, {
  theme: 'bootstrap' // or 'material-design'
})

window.axios = axios;
// axios.defaults.timeout = 10000;
window.$ = window.jQuery = require('jquery')

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  axios,
  i18n,
  render: h => h(App)
}).$mount('#app')
