import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios';

import router from './router'
import moment from 'moment';

Vue.use(Vuex)

var VueCookie = require('vue-cookie');
Vue.use(VueCookie);

export default new Vuex.Store({
  state: {
    diasSemana: ["lun", "mar", "mie", "jue", "vie", "sab", "dom"],
    meses: [
      "enero",
      "febrero",
      "marzo",
      "abril",
      "mayo",
      "junio",
      "julio",
      "agosto",
      "septiembre",
      "octubre",
      "noviembre",
      "diciembre"
    ],
    centroCosteVuelo: [],
    empleados: [{ chrCodigoEmpleado: '', fechaNacimiento: '', nombreCompleto: '' }],
    empleadoSelected: null,
    user: { login: '', password: '' },
    userLogin: {},
    userLogged: !!Vue.cookie.get('cookieControlViajes'),

    // basePath: 'http://192.168.15.14:8081/',
    //basePath: 'http://ccviajes.gservicio.com:8081/',
    basePath: 'http://10.125.3.35/SistemaReservaciones/', 

    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    autorizadorSuplenteSelected: null,

    tipoViaje: [],
    tipoVuelo: [],
    tipoHabitacion: [],
    tipoSolicitud: [],
    semaforo: [],
    estatusSolicitud: [],
    datosBancarios: {},
    datosFiscales: {},
    spinnerCustom: false,
    alertaLogin: false,
    alertaSolicitud: false,
    solicitudEdit: false,
    objVueloHotel: {},
    imagenesCotizacionesVuelo: [],
    imagenesCotizacionesHospedaje: [],
    imagenesCotizacionesVueloRechazadas: [],
    imagenesCotizacionesHospedajeRechazadas: [],
    itemsTableVuelo: [],
    itemsTableHospedaje: [],
    notificacion: [],
    operacionVuelo: {
      fechaRespuesta: moment().format("YYYY-MM-DD"),
      aerolinea: "",
      tipoVuelo: "",
      origen: "",
      destino: "",
      salida: "",
      regreso: "",
      intFKDetalleSolicitudViaje: 0,
      listadoOperacionesAgregadas:[]
    },
    operacionHospedaje: {
      fechaRespuesta: moment().format("YYYY-MM-DD"),
      fechaEntrada: "",
      fechaSalida: "",
      numHabitaciones: "",
      tipoHabitacion: "",
      intFKIDDetalleSolicitudHotel: 0,
      listadoOperacionesAgregadasHotel:[]
    },

    listaEmpleadosHuespedes: "",

    operacionesGuardadas: false,
    alertaEnviadaSolicitud: false,
    alertaPrioridadesUsuario: false,
    alertaPrioridadAutorizador: false,
    datosGeneralesOperacion: {},
    alertaReservacion: false,
    alertaCotizacionRechazada: false,
    centroCostoCargados: false,
    mensajeLoginConfig: {
      title: "Sistema de reservación de vuelos & hospedaje",
      subtitle: "Mensaje configurable desde Configuración por el administrador"
    },
    rememberMe: false,
    loadingSelect: false,
    pantallasUsuarios: [],
    enlacehome: '',
    datosdetallesids: []

  },
  mutations: {
    setCentroCosteVuelo(state, payload) {
      state.centroCosteVuelo = payload;
    },
    setUserLogin(state, payload) {
      state.userLogin = payload;
    },
    setUserLogged(state, payload) {
      state.userLogged = payload;
    },
    setTipoViaje(state, payload) {
      state.tipoViaje = payload;
    },
    setTipoVuelo(state, payload) {
      state.tipoVuelo = payload;
    },
    setSemaforo(state, payload) {
      state.semaforo = payload;
    },
    setTipoHabitacion(state, payload) {
      state.tipoHabitacion = payload;
    },
    setTipoSolicitud(state, payload) {
      state.tipoSolicitud = payload;
    },
    setEstatusSolicitud(state, payload) {
      state.estatusSolicitud = payload;
    },
    setDatosBancarios(state, payload) {
      state.datosBancarios = payload;
    },
    setDatosFiscales(state, payload) {
      state.datosFiscales = payload;
    },
    setSpinnerCustom(state, payload) {
      state.spinnerCustom = payload;
    },
    setAlertaLogin(state, payload) {
      state.alertaLogin = payload;
    },
    setAlertaSolicitud(state, payload) {
      state.alertaSolicitud = payload;
    },
    setSolicitudEdit(state, payload) {
      state.solicitudEdit = payload;
    },
    setObjVueloHotel(state, payload) {
      state.objVueloHotel = payload;
    },
    setEmpleados(state, payload) {
      state.empleados = payload;
    },
    setImagenesCotizacionesVuelo(state, payload) {
      state.imagenesCotizacionesVuelo = payload;
    },
    setImagenesCotizacionesHospedaje(state, payload) {
      state.imagenesCotizacionesHospedaje = payload;
    },
    setImagenesCotizacionesVueloRechazadas(state, payload) {
      state.imagenesCotizacionesVueloRechazadas = payload;
    },
    setImagenesCotizacionesHospedajeRechazadas(state, payload) {
      state.imagenesCotizacionesHospedajeRechazadas = payload;
    },
    setItemsTableVuelo(state, payload) {
      state.itemsTableVuelo = payload;
    },
    setItemsTableHospedaje(state, payload) {
      state.itemsTableHospedaje = payload;
    },
    setNotificaciones(state, payload) {
      state.notificacion = payload;
    },
    setOperacionesGuardadas(state, payload) {
      state.operacionesGuardadas = payload;
    },
    setAlertaEnviadaSolicitud(state, payload) {
      state.alertaEnviadaSolicitud = payload;
    },
    setAlertaPrioridadesUsuario(state, payload) {
      state.alertaPrioridadesUsuario = payload;
    },
    setAlertaPrioridadAutorizador(state, payload) {
      state.alertaPrioridadAutorizador = payload;
    },
    setAlertaReservacion(state, payload) {
      state.alertaReservacion = payload;
    },
    setOperacionVuelo(state, payload) {

      state.operacionVuelo.intIDDetalleSolicitudViaje = payload.intIDDetalleSolicitudViaje;
      state.operacionVuelo.aerolinea = payload.chrAerolinea;
      state.operacionVuelo.tipoVuelo = payload.intFKIDTipoVuelo;
      state.operacionVuelo.origen = payload.chrClaveOrigen;
      state.operacionVuelo.destino = payload.chrClaveDestino;
      state.operacionVuelo.salida = moment(payload.dtdFechaSalidad).format("YYYY-MM-DD");
      state.operacionVuelo.regreso = moment(payload.dtdFechaRegreso).format("YYYY-MM-DD");

    },
    setOperacionHospedaje(state, payload) {
      state.operacionHospedaje.intIDDetalleSolicitudHotel = payload.intIDDetalleSolicitudHotel;
      state.operacionHospedaje.fechaEntrada = moment(payload.dtdFechaSalidad).format("YYYY-MM-DD");
      state.operacionHospedaje.fechaSalida = moment(payload.dtdFechaRegreso).format("YYYY-MM-DD");
      state.operacionHospedaje.numHabitaciones = payload.intNoHabitaciones;
      state.operacionHospedaje.tipoHabitacion = payload.intFKIDTipoHabitacion;
    },
    setNombresHuespedes(state, payload) {
      state.listaEmpleadosHuespedes = payload
    },
    setAlertaCotizacionRechazada(state, payload) {
      state.alertaCotizacionRechazada = payload;
    },
    setEmpleadoSelected(state, payload) {
      state.empleadoSelected = payload;
    },
    setCentrosCostoCargados(state, payload) {
      state.centroCostoCargados = payload;
    },
    setMensajeLoginConfig(state, payload) {
      state.mensajeLoginConfig = payload;
    },
    setLoadingSelect(state, payload) {
      state.loadingSelect = payload;
    },
    setPantallasUsuarios(state, payload) {
      state.pantallasUsuarios = payload;
    },
    setConsultaRutaHome(state, payload) {
      state.enlacehome = payload;
    },
    setObtieneIDsDetalleViajeHospedaje(state, payload) {
      state.datosdetallesids = payload;
    }
  },
  actions: {
    entrar({ state, commit, dispatch }, payload) {

      var loginObj = {
        chrCodigoEmpleado: payload.login,
        chrPassword: payload.password
      }

      const qs = require('qs');

      commit('setSpinnerCustom', true);

      axios.post(state.basePath + 'Api/Empleados/Login/',
        qs.stringify(loginObj),
        state.headers
      ).then(response => {

        if (state.rememberMe) {
          sessionStorage.setItem("gte_control", JSON.stringify(payload));
        } else {
          sessionStorage.removeItem("gte_control");
        }
        if (response.data.message != "Error" && response.data.result != null) {

          var pantallas = response.data.result.pantallasUsuarios;

          response.data.result.pantallasUsuarios = [];

          if (response.data.result.empledoAutorizador.length <= 0) {
            response.data.result.empledoAutorizador = [];

            let objAutorizador = {
              intIDEmpleado: 0,
              chrNombre: "-",
              chrDepartamento: "-",
            }

            response.data.result.empledoAutorizador.push(objAutorizador)
          }

          Vue.cookie.set('cookieControlViajes', JSON.stringify(response.data.result), { expires: 1 });
          Vue.cookie.set('cookieControlViajesPantallas', JSON.stringify(pantallas), { expires: 1 });

          commit('setUserLogin', response.data.result)
          var arrayPantallas = [];
          pantallas.forEach(pantalla => {
            let objPantalla = {
              id: pantalla.intIDPantallas,
              path: pantalla.chrUrl,
              descripcion: pantalla.chrDescripcionModulo,
              isMenu: pantalla.chrMenuPrincipal == "SI" ? true : false
            }
            arrayPantallas.push(objPantalla);
          })
          commit('setPantallasUsuarios', arrayPantallas)
          commit('setUserLogged', true);
          commit('setSpinnerCustom', false);

          dispatch("getResumenVueloPorEmpleado", response.data.result.intIDEmpleado)
          dispatch("getResumenHotelPorEmpleado", response.data.result.intIDEmpleado)
          dispatch("getEmpleados", response.data.result.intIDEmpleado);
          dispatch("notificacionesSolicitudes", response.data.result.intIDEmpleado)
          dispatch("consultaRutaHome")
          router.push({ name: 'home' });
        } else {
          Vue.cookie.delete('cookieControlViajes')
          Vue.cookie.delete('cookieControlViajesPantallas')
          Vue.cookie.delete('cookieControlViajesPantallaHome')
          Vue.cookie.delete('cookieControlViajesUser')
          commit('setSpinnerCustom', false);
          commit('setAlertaLogin', true);
          setTimeout(() => {
            commit('setAlertaLogin', false);
          }, 3000);
        }
      })
        .catch(error => {

          Vue.cookie.delete('cookieControlViajes')
          Vue.cookie.delete('cookieControlViajesPantallas')
          Vue.cookie.delete('cookieControlViajesPantallaHome')
          Vue.cookie.delete('cookieControlViajesUser')
          commit('setSpinnerCustom', false);
          commit('setAlertaLogin', true);
          setTimeout(() => {
            commit('setAlertaLogin', false);
          }, 3000);
        })
    },

    recuperarPassword({ state }, payload) {

      const hd = state.headers

      var email = { email: payload.email };

      axios.post(state.basePath + 'Api/Empleados/contrasena_olvido/',
        email,
        { hd }
      )
        .then(response => {
        })
        .catch(error => {
        })
    },

    timeOutLogout({ commit }) {
      Vue.cookie.delete('cookieControlViajes')
      Vue.cookie.delete('cookieControlViajesPantallas')
      Vue.cookie.delete('cookieControlViajesPantallaHome')
      Vue.cookie.delete('cookieControlViajesUser')
      setTimeout(() => {
        router.push({ name: 'login' });
        commit('setUserLogged', false);
      }, 3000);
    },

    saveSolicitud({ state, dispatch, commit }, payload) {

      var solicitudObj = {
        intIDSolicitud: payload.intIDSolicitud,
        intFolio: payload.intIDSolicitud,
        dtdFechaSolicitud: payload.fechaActual,
        intNoSolicitante: payload.numSolicitud,
        intNoEmpleadoCreador: payload.user,
        dtdFechaNacimiento: payload.dtdFechaNacimiento,
        intFKIDEstatus: payload.estatus,
        chrComentarios: payload.comentariosSolicitud,
        intFKIDTipoSolicitud: payload.tipoSolicitud,
        intIDAutorizadorSuplente: state.autorizadorSuplenteSelected != null ? state.autorizadorSuplenteSelected.chrCodigoEmpleado : ""
      }

      const hd = state.headers;

      axios.post(state.basePath + 'Api/Solicitud/SaveSolicitud/',
        solicitudObj,
        { hd }
      )
        .then(response => {
          commit('setSpinnerCustom', true);
          if (response.data.result.intFKIDTipoSolicitud == 1) {

            dispatch('detalleSolicitud', ({ resp: response.data.result, formVuelo: payload }))
            dispatch('detalleSolicitudHotel', ({ resp: response.data.result, formHospedaje: payload }))
          } else if (response.data.result.intFKIDTipoSolicitud == 2) {
            dispatch('detalleSolicitud', ({ resp: response.data.result, formVuelo: payload }))

          } else if (response.data.result.intFKIDTipoSolicitud == 3) {
            dispatch('detalleSolicitudHotel', ({ resp: response.data.result, formHospedaje: payload }))
          }

        })
        .catch(error => {
        })
    },

    detalleSolicitud({ state, dispatch }, payload) {

      payload.formVuelo.arraylistaVuelo.forEach(viaje => {

        let dataDetalleSolicitud = {
          intFKIDSolicitud: payload.resp.intIDSolicitud,
          intIDDetalleSolicitudViaje: viaje.intIDDetalleSolicitudViaje,//payload.formVuelo.intIDDetalleSolicitudViaje,
          chrFolio: payload.resp.intFolio,
          intFKIDTipoVuelo: viaje.tipoVueloSelected,
          chrClaveOrigen: viaje.origen,
          chrClaveDestino: viaje.destino,
          dtdFechaSalidad: viaje.fechaSalida,
          dtdFechaRegreso: viaje.fechaRegreso,
          chrPresupuesto: "",
          chrComentarios: viaje.comentarioVuelo,
          intFKIDTipoViaje: viaje.tipoViajeSelected,
          chrCentroCostoViaje: viaje.centroCosteVuelo,
          chrAerolinea: viaje.aerolinea,
          chrMotivoViaje: viaje.motivoViaje
        }
        const hd = state.headers;
        axios.post(state.basePath + 'Api/Solicitud/DetalleSolicitudViaje/',
          dataDetalleSolicitud,
          { hd }
        )
          .then(response => {

            if (viaje.colaboradorSelected == "Todos") {

              payload.formVuelo.objFormVuelo.listaEmpleados.forEach(empleadosLista => {

                if (viaje.intIDDetalleSolicitudViaje > 0) {
                  if (empleadosLista.intIDDetalleSolicitudViaje == viaje.intIDDetalleSolicitudViaje) {
                    dispatch('detalleViajeros', {
                      resp: response.data.result,
                      form: payload,
                      intIDDetalleSolicitudViaje: response.data.result.intIDDetalleSolicitudViaje,
                      intFKIDEmpleado: empleadosLista.numero,
                      intIDDetalleViajeros: empleadosLista.intIDDetalleViajeros
                    })
                  }
                } else {
                  dispatch('detalleViajeros', {
                    resp: response.data.result,
                    form: payload,
                    intIDDetalleSolicitudViaje: response.data.result.intIDDetalleSolicitudViaje,
                    intFKIDEmpleado: empleadosLista.numero,
                    intIDDetalleViajeros: empleadosLista.intIDDetalleViajeros
                  })
                }

              })
            }
            else {
              let intIDDetalleViajerosSelect = "";
              let intFKIDDetalleSolicitudViaje = "";
              payload.formVuelo.objFormVuelo.listaEmpleados.forEach(empleadosLista => {
                if (empleadosLista.numero == viaje.colaboradorSelected) {
                  intIDDetalleViajerosSelect = empleadosLista.intIDDetalleViajeros;
                  intFKIDDetalleSolicitudViaje = empleadosLista.intIDDetalleSolicitudViaje;
                }
              })
              if (intFKIDDetalleSolicitudViaje > 0) {
                if (intFKIDDetalleSolicitudViaje == viaje.intIDDetalleSolicitudViaje) {
                  dispatch('detalleViajeros', {
                    resp: response.data.result,
                    form: payload,
                    intIDDetalleSolicitudViaje: response.data.result.intIDDetalleSolicitudViaje,
                    intFKIDEmpleado: viaje.colaboradorSelected,
                    intIDDetalleViajeros: intIDDetalleViajerosSelect,
                  })
                }
              } else {
                dispatch('detalleViajeros', {
                  resp: response.data.result,
                  form: payload,
                  intIDDetalleSolicitudViaje: response.data.result.intIDDetalleSolicitudViaje,
                  intFKIDEmpleado: viaje.colaboradorSelected,
                  intIDDetalleViajeros: intIDDetalleViajerosSelect,
                })
              }


            }
            if (viaje.costoExtraVuelo != null || viaje.costoExtaHotel > 0) {

              dispatch('updateCostoExtraViaje', {
                intFKDetalleSolicitudViaje: response.data.result.intIDDetalleSolicitudViaje,
                costoExtraVuelo: viaje.costoExtraVuelo
              })
            }
          })
          .catch(error => {
          })
      })


    },
    detalleViajeros({ state, dispatch, commit }, payload) {

      const hd = state.headers;

      let dataDetalleViajeros = {
        intIDDetalleViajes: payload.intIDDetalleViajeros != null ? payload.intIDDetalleViajeros : "",
        intFKIDDetalleSolicitudViaje: payload.resp.intIDDetalleSolicitudViaje,
        intFKIDEmpleado: payload.intFKIDEmpleado
      }

      axios.post(state.basePath + 'Api/Solicitud/DetalleViajeros/',
        dataDetalleViajeros,
        { hd })
        .then(response => {

          commit('setSpinnerCustom', false);
          commit('setAlertaSolicitud', true);
          setTimeout(() => {
            commit('setAlertaSolicitud', false);
            if (payload.form.formVuelo.estatus != 7) {
              router.push({ name: 'detalleVueloHospedaje' })
            } else {
              router.push({ name: 'detalleSolicitudes' })
            }
          }, 3000);

        })
        .catch(error => {
        })
    },

    operacionesSolicitudAvion({ state, dispatch, commit }, payload) {

      let hd = state.headers;
      let datos = {
        intAceptada: 1,
        intAutorizada: 1,
        intReservada: 1
      }
      
      let dataOperacionesSolicitudAvion = {
        intIDOperacionesSolicitudesAvion: payload.operacionVuelo.intIDOperacionesSolicitudesAvion,
        // intFKDetalleSolicitudViaje: payload.idSolicitud,
        intFKDetalleSolicitudViaje: payload.operacionVuelo.intFKDetalleSolicitudViaje,
        intNoOpcion: payload.operacionVuelo.opcion,
        chrClaveReservacion: payload.operacionVuelo.respuesta,
        dtdFechaRespuesta: payload.operacionVuelo.fechaRespuesta,
        chrLineaArea: payload.operacionVuelo.aerolinea,
        intFKIDTipoVuelo: payload.tipoVuelo,
        chrClaveOrigen: payload.operacionVuelo.origen,
        chrClaveDestino: payload.operacionVuelo.destino,
        dtdFechaSalida: payload.operacionVuelo.salida,
        dtdFechaRegreso: payload.operacionVuelo.regreso,
        decCosto: payload.operacionVuelo.costo,
        chrEscala: payload.operacionVuelo.escalas != null > 0 ? 1 : 0,
        chrNoEscalas: payload.numEscalas,
        chrAceptada: datos.intAceptada,
        chrAutorizada: datos.intAutorizada,
        chrReservada: datos.intReservada,
        chrRFC: payload.operacionVuelo.rfcAerolinea,
        chrCostoExtraAvion: payload.operacionVuelo.costoExtraVuelo
      }

      axios.post(state.basePath + 'Api/Solicitud/OperacionesSolicitudAvion/',
        dataOperacionesSolicitudAvion,
        hd)
        .then(response => {
        
          if (payload.escalasAgregadas != null) {
            if (payload.escalasAgregadas.length > 0) {

              dispatch("saveEscalasVuelo", { resp: response.data.result, form: payload.escalasAgregadas })
            }
          }
          if (payload.operacionHospedaje == {} || payload.operacionHospedaje == undefined || payload.operacionHospedaje == null) {
            router.push({ name: "detalleSolicitudes" })
            commit("setOperacionesGuardadas", true)
          } else {
            if (state.operacionesGuardadas) {
              router.push({ name: "detalleSolicitudes" })
            } else {
              commit("setOperacionesGuardadas", true)
            }
          }
          state.operacionVuelo = {
            fechaRespuesta: moment().format("YYYY-MM-DD")
          },
            dispatch('reservacionViaje', { resp: response.data.result, form: payload })
        })
        .catch(error => {

        })
    },
    updateCostoExtraViaje({ state, dispatch, commit }, payload) {
      
      const hd = state.headers;
      let dataDetalleOperacionesViaje = {
        intFKDetalleSolicitudViaje: payload.intFKDetalleSolicitudViaje,
        chrCostoExtraAvion: payload.costoExtraVuelo
      }
      axios.post(state.basePath + 'Api/Solicitud/OperacionesUpdateSolicitudAvion/',
      dataDetalleOperacionesViaje,
        { hd }
      )
        .then(response => {
        

        })
        .catch(error => {
        })
    },
    saveEscalasVuelo({ state, commit }, payload) {
      
    
      let arrayEscalas = []

      payload.form.forEach(escala => {
      
        let objEscala = {
          intFKIDOperacionesSolicitudAvion: payload.resp.intIDOperacionesSolicitudesAvion,
          chrClaveOrigenEscala: escala.origenEscala,
          chrClaveDestinoEscala: escala.destinoEscala,
          dtdFechaSalidaEscala: escala.fechaSalidaEscala
        }
        arrayEscalas.push(objEscala);
      })

      let hd = state.headers;

      axios.post(state.basePath + 'Api/Solicitud/SaveEscalasVuelo/',
        arrayEscalas,
        hd)
        .then(response => {
        })
        .catch(error => {
        })
    },

    reservacionViaje({ state, commit }, payload) {

      let dataReservacionViaje = {
        intIDReservacionViaje: payload.form.operacionVuelo.intIDReservacionViaje,
        intFKIDOperacionesSolicitudesAvion: payload.resp.intIDOperacionesSolicitudesAvion,
        intNoOpcion: payload.resp.intNoOpcion,
        chrContabilizada: "",
        chrTipoCompra: 'Tarjeta',
        chrCodigoReservacion: payload.form.chrNumReservacion
      }

      const hd = state.headers
      axios.post(state.basePath + 'Api/Solicitud/ReservacionViaje/',
        dataReservacionViaje,
        { hd }
      )
        .then(response => {
          commit("setAlertaReservacion", true);
          setTimeout(() => {
            router.push({ name: 'detalleSolicitudes' })
            commit("setAlertaReservacion", false)
          }, 3000);
        })
        .catch(error => {

        })
    },

    detalleSolicitudHotel({ state, dispatch }, payload) {
     
      payload.formHospedaje.arrayListaHospedaje.forEach(hospedaje => {
       

        let dataDetalleSolicitudHotel = {
          intFKIDSolicitud: payload.resp.intIDSolicitud,
          intIDDetalleSolicitudHotel: hospedaje.intIDDetalleSolicitudHotel,
          chrClaveOrigen: hospedaje.origen,
          chrClaveDestino: hospedaje.destino,
          dtdFechaSalidad: hospedaje.fechaEntradaHotel,
          dtdFechaRegreso: hospedaje.fechaSalidaHotel,
          chrPresuspuesto: "",
          intFKIDTipoHabitacion: hospedaje.tipoHabitacionSelected,
          chrComentarios: hospedaje.comentarioHospedaje,
          chrEsHotel: hospedaje.hotelSelected == "1" ? 'SI' : "NO",
          intNoHabitaciones: hospedaje.habitaciones,
          chrNochesReservadas: hospedaje.nochesReservadas,
          chrCentroCostoHospedaje: hospedaje.centroCosteHospedaje,
          costoExtaHotel: hospedaje.costoExtaHotel
        }

        const hd = state.headers;

        axios.post(state.basePath + 'Api/Solicitud/DetalleSolicitudHotel/',
          dataDetalleSolicitudHotel,
          { hd }
        )
          .then(response => {

            if (hospedaje.colaboradorSelected == "Todos") {

              payload.formHospedaje.objFormVuelo.listaEmpleados.forEach(empleadosLista => {
                if (hospedaje.intIDDetalleSolicitudHotel > 0) {
                  if (empleadosLista.intIDDetalleSolicitudHotel == hospedaje.intIDDetalleSolicitudHotel) {
                    dispatch('detalleHuespedes', {
                      resp: response.data.result,
                      form: payload,
                      intIDDetalleSolicitudHospedaje: response.data.result.intIDDetalleSolicitudHotel,
                      intFKIDEmpleado: empleadosLista.numero,
                      intIDDetalleHuespedes: empleadosLista.intIDDetalleHuespedes
                    })
                  }
                } else {
                  dispatch('detalleHuespedes', {
                    resp: response.data.result,
                    form: payload,
                    intIDDetalleSolicitudHospedaje: response.data.result.intIDDetalleSolicitudHotel,
                    intFKIDEmpleado: empleadosLista.numero,
                    intIDDetalleHuespedes: empleadosLista.intIDDetalleHuespedes
                  })
                }

              })
            }
            else {
              let intIDDetalleHuespedesSelect = "";
              let intFKIDDetalleHuespedes = "";

              payload.formHospedaje.objFormVuelo.listaEmpleados.forEach(empleadosLista => {
                if (empleadosLista.numero == hospedaje.colaboradorSelected) {
                  intIDDetalleHuespedesSelect = empleadosLista.intIDDetalleHuespedes;
                  intFKIDDetalleHuespedes = empleadosLista.intIDDetalleSolicitudHotel;
                }
              })
              if (intFKIDDetalleHuespedes > 0) {
                if (intFKIDDetalleHuespedes == hospedaje.intIDDetalleSolicitudHotel) {
                  dispatch('detalleHuespedes', {
                    resp: response.data.result,
                    form: payload,
                    intIDDetalleSolicitudHospedaje: response.data.result.intIDDetalleSolicitudHotel,
                    intFKIDEmpleado: hospedaje.colaboradorSelected,
                    intIDDetalleHuespedes: intIDDetalleHuespedesSelect,
                  })
                }
              } else {
                dispatch('detalleHuespedes', {
                  resp: response.data.result,
                  form: payload,
                  intIDDetalleSolicitudHospedaje: response.data.result.intIDDetalleSolicitudHotel,
                  intFKIDEmpleado: hospedaje.colaboradorSelected,
                  intIDDetalleHuespedes: intIDDetalleHuespedesSelect,
                })
              }

            }
           
            if (hospedaje.costoExtaHotel != null || hospedaje.costoExtaHotel > 0) {

              dispatch('updateCostoExtraHotel', {
                intFKDetalleSolicitudHospedaje: response.data.result.intIDDetalleSolicitudHotel,
                costoExtra: hospedaje.costoExtaHotel
              })
            }
          })
          .catch(error => {
          })

      })

    },
    detalleHuespedes({ state, commit }, payload) {

      const hd = state.headers;
      let dataDetalleHuespedes = {
        intIDDetalleHuespedes: payload.intIDDetalleHuespedes != null ? payload.intIDDetalleHuespedes : "",
        intFKIDEmpleado: payload.intFKIDEmpleado,
        intFKIDDetalleSolicitudHotel: payload.intIDDetalleSolicitudHospedaje
      }
      axios.post(state.basePath + 'Api/Solicitud/DetalleHuespedes/',
        dataDetalleHuespedes,
        { hd }
      )
        .then(response => {
          commit('setSpinnerCustom', false);
          commit('setAlertaSolicitud', true);
          setTimeout(() => {
            commit('setAlertaSolicitud', false);
            if (payload.form.formHospedaje.estatus != 7) {
              router.push({ name: 'detalleVueloHospedaje' })
            } else {
              router.push({ name: 'detalleSolicitudes' })
            }
          }, 3000);

        })
        .catch(error => {
        })
    },
    operacionesSolicitudHotel({ state, dispatch, commit }, payload) {
      let datos = {
        intAceptada: 1,
        intAutorizada: 1,
        intReservada: 1
      }

      let dataOperacionesSolicitudHotel = {
        intIDOperacionesSolicitudHotel: payload.operacionHospedaje.intIDOperacionesSolicitudHotel,
        // intFKIDDetalleSolicitudHotel: payload.idSolicitud,
        intFKIDDetalleSolicitudHotel: payload.operacionHospedaje.intFKIDDetalleSolicitudHotel,
        intNoOpcion: payload.operacionHospedaje.opcion,
        chrCodigoConfirmacion: payload.operacionHospedaje.respuesta,
        dtdFechaRespuesta: payload.operacionHospedaje.fechaRespuesta,
        chrLineaHotel: payload.operacionHospedaje.hotel,
        dtdFechaEntrada: payload.operacionHospedaje.fechaEntrada,
        dtdFechaSalidad: payload.operacionHospedaje.fechaSalida,
        intNoHabitaciones: payload.operacionHospedaje.numHabitaciones,
        intFKIDTipoHabitacion: payload.operacionHospedaje.tipoHabitacion,
        decCosto: payload.operacionHospedaje.costo,
        intAceptada: datos.intAceptada,
        intAutorizada: datos.intAutorizada,
        intReservada: datos.intReservada,
        chrRFC: payload.operacionHospedaje.rfcHotel,
        chrCostoExtraHotel: payload.operacionHospedaje.costoExtraHospedaje
      }

      const hd = state.headers
      axios.post(state.basePath + 'Api/Solicitud/OperacionesSolicitudHotel/',
        dataOperacionesSolicitudHotel,
        { hd }
      )
        .then(response => {

          if (payload.operacionVuelo == {}) {
            router.push({ name: "detalleSolicitudes" })
          } else {
            if (state.operacionesGuardadas) {
              router.push({ name: "detalleSolicitudes" })
            } else {
              commit("setOperacionesGuardadas", true)
            }
          }
          state.operacionHospedaje = {
            fechaRespuesta: moment().format("YYYY-MM-DD")
          }
          dispatch('reservacionHotel', { resp: response.data.result, form: payload })
        })
        .catch(error => {
        })
    },
    updateCostoExtraHotel({ state, dispatch, commit }, payload) {
      
      const hd = state.headers;
      let dataDetalleOperacionesHotel = {
        intFKIDDetalleSolicitudHotel: payload.intFKDetalleSolicitudHospedaje,
        chrCostoExtraHotel: payload.costoExtra
      }
      axios.post(state.basePath + 'Api/Solicitud/OperacionesUpdateSolicitudHotel/',
        dataDetalleOperacionesHotel,
        { hd }
      )
        .then(response => {
        

        })
        .catch(error => {
        })
    },
    reservacionHotel({ state, commit }, payload) {

      let dataReservacionHotel = {
        intIDReservacionHotel: payload.form.operacionHospedaje.intIDReservacionHotel,
        intFKIDOperacionesSolicitudHotel: payload.resp.intIDOperacionesSolicitudHotel,
        intNoOpcion: payload.resp.intNoOpcion,
        chrContabilizada: "",
        chrTipoCompra: "Tarjeta",
        chrCodigoReservacion: payload.form.chrNumReservacion
      }

      const hd = state.headers

      axios.post(state.basePath + 'Api/Solicitud/ReservacionHotel/',
        dataReservacionHotel,
        { hd }
      )
        .then(response => {
          commit("setAlertaReservacion", true);
          setTimeout(() => {
            router.push({ name: 'detalleSolicitudes' })
            commit("setAlertaReservacion", false)
          }, 3000);
        })
        .catch(error => {
        })
    },

    getTipoVuelo({ state, commit }) {

      axios.get(state.basePath + 'Api/Catalogos/GetTipoVuelo/')
        .then(response => {
          commit('setTipoVuelo', response.data.result);
        })
        .catch(error => {
        })
    },

    getTipoHabitacion({ state, commit }) {
      axios.get(state.basePath + 'Api/Catalogos/GetTipoHabitacion/')
        .then(response => {
          commit('setTipoHabitacion', response.data.result);
        })
        .catch(error => {
        })
    },

    getTipoViaje({ state, commit }) {
      axios.get(state.basePath + 'Api/Catalogos/GetTipoViaje/')
        .then(response => {
          commit('setTipoViaje', response.data.result)
        })
        .catch(error => {
        })
    },

    getEstatusSolicitud({ state, commit }) {
      axios.get(state.basePath + 'Api/Catalogos/GetEstatusSolicitud/')
        .then(response => {

          commit('setEstatusSolicitud', response.data.result);
        })
        .catch(error => {
        })
    },

    getDatosBancarios({ state, commit }) {
      // Api/Catalogos/ConsultaDatosBancarios/
      axios.get(state.basePath + 'Api/Catalogos/ConsultaDatosBancarios/')
        .then(response => {
          commit('setDatosBancarios', response.data.result[(response.data.result.length - 1)]);
        })
        .catch(error => {
        })
    },
    getDatosFiscales({ state, commit }) {
      // Api/Catalogos/GetDatosFiscales/
      axios.get(state.basePath + 'Api/Catalogos/GetDatosFiscales/')
        .then(response => {
          commit('setDatosFiscales', response.data.result[(response.data.result.length - 1)])
        })
        .catch(error => {
        })
    },

    getTipoSolicitud({ state, commit }) {
      axios.get(state.basePath + 'Api/Catalogos/GetTipoSolicitud/')
        .then(response => {

          commit('setTipoSolicitud', response.data.result);
        })
        .catch(error => {
        })
    },
    getMensajeLoginConfig({ state, commit }) {

      axios.get(state.basePath + 'Api/Catalogos/GetConfiguracion/')
        .then(response => {

          commit("setMensajeLoginConfig", response.data.result[0]);
        })
        .catch(error => {

          commit("setMensajeLoginConfig", "")
        })
    },
    getSemaforo({ state, commit }) {
      axios.get(state.basePath + 'Api/Catalogos/GetSemaforo/')
        .then(response => {
          commit('setSemaforo', response.data.result);

        })
        .catch(error => {

        })
    },
    muestraDatosVueloHotel({ state, commit }, payload) {

      let objSolicitud = {
        intIDSolicitud: payload.intIDSolicitud,
        intFKIDDetalleSolicitudViaje: payload.intFKIDDetalleSolicitudViaje
      }

      const hd = state.headers;

      axios.post(state.basePath + 'Api/Solicitud/MuestraDatosVueloHotel/',
        objSolicitud,
        { hd }
      )
        .then(response => {

          commit('setObjVueloHotel', response.data.result)

          if (response.data.result.intFKIDTipoSolicitud == 1) {
            router.push({ name: "detalleSolicitudVueloHospedajeId", params: { id: payload.intIDSolicitud, idDetalle: payload.intFKIDDetalleSolicitudViaje } });
          } else if (response.data.result.intFKIDTipoSolicitud == 2) {
            router.push({ name: 'detalleSolicitudVueloId', params: { id: payload.intIDSolicitud, idDetalle: payload.intFKIDDetalleSolicitudViaje } })
          } else if (response.data.result.intFKIDTipoSolicitud == 3) {
            router.push({ name: 'detalleSolicitudHospedajeId', params: { id: payload.intIDSolicitud, idDetalle: payload.intFKIDDetalleSolicitudViaje } })
          }
        })
        .catch(error => {
        })
    },
    getEmpleados({ state, commit }, payload) {

      if (payload == undefined) {
        payload = "0000000"
      }

      let codigoEmpleado = {
        "chrCodigoEmpleado": payload
      }
      const hd = state.headers

      axios.post(state.basePath + 'Api/WebServices/GetEmpleados/',
        codigoEmpleado,
        { hd })
        .then(response => {

          Vue.cookie.set('cookieControlViajesUser', JSON.stringify(response.data), { expires: 1 });
          commit("setEmpleadoSelected", response.data)

        })
        .catch(error => {

        })
    },

    enviarSolicitudAdministrador({ state, commit }, payload) {
      const hd = state.headers;
      let objNotificacion = {
        intIDSolicitud: payload
      }
      commit("setAlertaEnviadaSolicitud", true);

      axios.post(state.basePath + 'Api/Notificaciones/EnviarSolicitudAdministrador/',
        objNotificacion,
        { hd }
      )
        .then(response => {
          router.push({ name: 'detalleVueloHospedaje' });
          commit("setAlertaEnviadaSolicitud", false);
        })
        .catch(error => {
          commit("setAlertaEnviadaSolicitud", false);
        })
    },

    enviarSolicitudUsuarioAuditorizador({ state, commit }, payload) {

      const hd = state.headers;
      let objNotificacion = {
        intIDSolicitud: payload
      }
      commit("setAlertaPrioridadesUsuario", true)

      axios.post(state.basePath + 'Api/Notificaciones/EnviarSolicitudUsuarioAuditorizador/',
        objNotificacion,
        { hd }
      )
        .then(response => {
          router.push({ name: 'detalleVueloHospedaje' });
          commit("setAlertaPrioridadesUsuario", false)
        })
        .catch(error => {
          commit("setAlertaPrioridadesUsuario", false)
        })
    },

    enviaSolicitudAuditorizadorAdministrador({ state, commit }, payload) {

      const hd = state.headers;
      let objNotificacion = {
        intIDSolicitud: payload.idSolicitud
      }

      commit("setAlertaPrioridadAutorizador", true)

      axios.post(state.basePath + 'Api/Notificaciones/EnviaSolicitudAuditorizadorAdministrador/',
        objNotificacion,
        { hd }
      )
        .then(response => {
          router.push({ name: "autorizadorSolicitudes" });
          commit("setAlertaPrioridadAutorizador", false)
        })
        .catch(error => {
          commit("setAlertaPrioridadAutorizador", false)
        })
    },

    rechazaPropuesta({ state }, payload) {
      let obj = {
        chrMotivoCancelacion: payload.comentario,
        intIDSolicitud: payload.id
      };
      const hd = state.headers;
      axios.post(state.basePath + 'Api/Notificaciones/CancelarCotizacionAdministrador/',
        obj,
        { hd }
      )
        .then(response => {
          router.push({ name: 'detalleSolicitudes' })
        })
        .catch(error => {
        })

    },
    rechazaCotizacion({ state, commit }, payload) {
      let obj = {
        chrMotivoCancelacion: payload.comentario,
        intIDSolicitud: payload.id
      };
      const hd = state.headers;
      axios.post(state.basePath + 'Api/Notificaciones/CancelarCotizacionUsuario/',
        obj,
        { hd }
      )
        .then(response => {
          commit("setAlertaCotizacionRechazada", true)
          setTimeout(() => {
            commit("setAlertaCotizacionRechazada", false)
            router.push({ name: 'detalleVueloHospedaje' })
          }, 3000);
        })
        .catch(error => {
        })
    },
    cotizarSolicitud({ state }, payload) {
      router.push({ name: "cargaImagenes", query: { solicitud: payload.id, tipoSolicitud: payload.tipoSolicitud, idDetalle: payload.idDetalle } });
    },
    consultarCotizaciones({ state, commit }, payload) {

      const hd = state.headers

      var solicitud = {
        intIDSolicitud: payload.idSolicitud,
        intFKIDDetalleSolicitudViajeHotel: payload.idDetalleSolicitud
      };

      axios.post(state.basePath + 'Api/Solicitud/ConsultaImagenes/',
        solicitud,
        { hd }
      )
        .then(response => {

          let arrayVueloImg = []
          let arrayHospedajeImg = []
          let arrayVueloImgRechazadas = []
          let arrayHospedajeImgRechazadas = []
          let vueloAnterior = ""
          response.data.result.forEach(img => {
            vueloAnterior = img.intFKIDDetalleSolicitudVueloHotel;
            // if(vueloAnterior != img.intFKIDDetalleSolicitudVueloHotel){
            if (img.intEsRechazado == null) {
              img.chrPath = state.basePath + '' + img.chrPath
              if (img.intFKIDTipoSolicitud == 2) {
                arrayVueloImg.push(img)
              } else {
                arrayHospedajeImg.push(img)
              }
            } else {
              img.chrPath = state.basePath + '' + img.chrPath
              if (img.intFKIDTipoSolicitud == 2) {
                arrayVueloImgRechazadas.push(img)
              } else {
                arrayHospedajeImgRechazadas.push(img)
              }
            }
            //}

          })


          if (arrayVueloImg.length > 0) {
            if (arrayVueloImg[0].intNoOpcionAutorizador != null) {
              arrayVueloImg.sort((a, b) => (a.intNoOpcionAutorizador > b.intNoOpcionAutorizador) ? 1 : -1)
            } else if (arrayVueloImg[0].intNoOpcionUsuario != null) {
              arrayVueloImg.sort((a, b) => (a.intNoOpcionUsuario > b.intNoOpcionUsuario) ? 1 : -1)
            }
          }


          if (arrayHospedajeImg.length > 0) {
            if (arrayHospedajeImg[0].intNoOpcionAutorizador != null) {
              arrayHospedajeImg.sort((a, b) => (a.intNoOpcionAutorizador > b.intNoOpcionAutorizador) ? 1 : -1)
            } else if (arrayHospedajeImg[0].intNoOpcionUsuario != null) {
              arrayHospedajeImg.sort((a, b) => (a.intNoOpcionUsuario > b.intNoOpcionUsuario) ? 1 : -1)
            }
          }

          commit('setImagenesCotizacionesVuelo', arrayVueloImg)
          commit('setImagenesCotizacionesHospedaje', arrayHospedajeImg)

          commit('setImagenesCotizacionesVueloRechazadas', arrayVueloImgRechazadas)
          commit('setImagenesCotizacionesHospedajeRechazadas', arrayHospedajeImgRechazadas)
        })
        .catch(error => {
        })
    },
    enviarCotizacionUsuario({ state, commit }, payload) {

      const hd = state.headers;

      var cotizacion = { intIDSolicitud: payload }

      axios.post(state.basePath + 'Api/Notificaciones/EnviaCotizacionUsuario/',
        cotizacion,
        { hd }
      )
        .then(response => {
        })
        .catch(error => {
        })
    },
    getResumenVueloPorEmpleado({ state, commit }, payload) {
      const currentCookie = JSON.parse(Vue.cookie.get("cookieControlViajes"))
      let obj = {
        intIDEmpleado: payload
      };
      const hd = state.headers;
      axios
        .post(
          state.basePath +
          "Api/Solicitud/GetResumenVueloPorEmpleado/",
          obj,
          { hd }
        )
        .then(response => {

          var arrayProvisional = [];
          var arrayNotificacion = [];

          response.data.result.forEach(resp => {


            let nombreCompleto =
              (resp.chrNombreEmpleado != null ? resp.chrNombreEmpleado : "") +
              " " +
              (resp.chrApellidoPaterno != null ? resp.chrApellidoPaterno : "") +
              " " +
              (resp.chrApellidoMaterno != null ? resp.chrApellidoMaterno : "");

            resp.nombreCompleto = nombreCompleto;

            switch (currentCookie.intIDPerfil) {
              case 1:
                if (resp.intIDEstatus == "2" || resp.intIDEstatus == "3") {
                  let obj = {
                    numSolicitud: resp.intIDSolicitud,
                    empleado: resp.nombreCompleto,
                    color: resp.intIDEstatus == "2" ? "success" : "danger"
                  }
                  arrayNotificacion.push(obj);
                }
                break;
              case 2:
                if (resp.intIDEstatus == "Pendiente" || resp.intIDEstatus == "5") {
                  let obj = {
                    numSolicitud: resp.intIDSolicitud,
                    empleado: resp.nombreCompleto,
                    color: resp.intIDEstatus == "2" ? "yellow" : "success"
                  }
                  arrayNotificacion.push(obj);
                }
                break;
              case 3:
                if (resp.intIDEstatus == "4") {
                  let obj = {
                    numSolicitud: resp.intIDSolicitud,
                    empleado: resp.nombreCompleto,
                    color: "yellow"
                  }
                  arrayNotificacion.push(obj);
                }
                break;
              default:
                break;
            }
            resp.dtdFechaSalidad = moment(resp.dtdFechaSalidad).format(
              "DD-MM-YYYY"
            );
            // VALIDAR SI ES VUELO SENCILLO O REDONDO
            var fechaRegreso = "";
            if (resp.dtdFechaRegreso != null) {
              fechaRegreso = moment(resp.dtdFechaRegreso).format(
                "DD-MM-YYYY"
              )

            } else {
              fechaRegreso = "";
            }
            resp.dtdFechaRegreso = fechaRegreso;
            arrayProvisional.push(resp);
          });
          commit("setItemsTableVuelo", arrayProvisional);
          // commit("setNotificaciones", arrayNotificacion)
          commit("setSpinnerCustom", false)
        })
        .catch(error => {
          commit("setSpinnerCustom", false)
        });
    },
    getResumenHotelPorEmpleado({ state, commit }, payload) {
      const currentCookie = JSON.parse(Vue.cookie.get("cookieControlViajes"))
      let obj = {
        intIDEmpleado: payload
      };
      const hd = state.headers;
      axios
        .post(
          state.basePath +
          "Api/Solicitud/GetResumenHotelPorEmpleado/",
          obj,
          { hd }
        )
        .then(response => {

          var arrayProvisional = [];
          var arrayNotificacion = [];
          response.data.result.forEach(resp => {
            let nombreCompleto =
              (resp.chrNombreEmpleado != null ? resp.chrNombreEmpleado : "") +
              " " +
              (resp.chrApellidoPaterno != null ? resp.chrApellidoPaterno : "") +
              " " +
              (resp.chrApellidoMaterno != null ? resp.chrApellidoMaterno : "");

            resp.nombreCompleto = nombreCompleto;
            switch (currentCookie.intIDPerfil) {
              case 1:
                if (resp.intIDEstatus == "2" || resp.intIDEstatus == "3") {
                  let obj = {
                    numSolicitud: resp.intIDSolicitud,
                    empleado: resp.nombreCompleto,
                    color: resp.intIDEstatus == "2" ? "success" : "danger"
                  }
                  arrayNotificacion.push(obj);
                }
                break;
              case 2:
                if (resp.intIDEstatus == "1" || resp.intIDEstatus == "5") {
                  let obj = {
                    numSolicitud: resp.intIDSolicitud,
                    empleado: resp.nombreCompleto,
                    color: resp.intIDEstatus == "2" ? "yellow" : "success"
                  }
                  arrayNotificacion.push(obj);
                }
                break;
              case 3:
                if (resp.intIDEstatus == "4") {
                  let obj = {
                    numSolicitud: resp.intIDSolicitud,
                    empleado: resp.nombreCompleto,
                    color: "yellow"
                  }
                  arrayNotificacion.push(obj);
                }
                break;
              default:
                break;
            }
            resp.dtdFechaSalidad = moment(resp.dtdFechaSalidad).format(
              "DD-MM-YYYY"
            );
            resp.dtdFechaRegreso = moment(resp.dtdFechaRegreso).format(
              "DD-MM-YYYY"
            );
            arrayProvisional.push(resp);
          });
          commit("setItemsTableHospedaje", arrayProvisional)
          // commit("setNotificaciones", arrayNotificacion)
          commit("setSpinnerCustom", false)
        })
        .catch(error => {
          commit("setSpinnerCustom", false)
        });
    },
    cargarDatosOperacionesVueloHospedaje({ state, commit }, payload) {

      let objSolicitud = {
        intIDSolicitud: payload.intIDSolicitud,
        intFKIDDetalleSolicitudViaje: payload.intFKIDDetalleSolicitudViaje
      };
      const hd = state.headers;
      axios
        .post(
          state.basePath + "Api/Solicitud/MuestraDatosVueloHotel/",
          objSolicitud,
          { hd }
        )
        .then(response => {

          let nombresEmpleadosHospedaje = [];
          if (response.data.result.intFKIDTipoSolicitud == 1) {

            commit("setOperacionVuelo", response.data.result.detalleSolicitudViaje)
            commit("setOperacionHospedaje", response.data.result.detalleSolicitudHotel)
            response.data.result.detalleSolicitudHotel.lstDtEmpleadoHuespedes.forEach(emp => {
              let nombreCompleto = (emp.chrNombre != null ? emp.chrNombre : "") + ' ' + (emp.chrApellidoPaterno != null ? emp.chrApellidoPaterno : "") + ' ' + (emp.chrApellidoMaterno != null ? emp.chrApellidoMaterno : "")
              nombresEmpleadosHospedaje.push(nombreCompleto);
            })
            commit("setNombresHuespedes", nombresEmpleadosHospedaje)
          } else if (response.data.result.intFKIDTipoSolicitud == 2) {
            commit("setOperacionVuelo", response.data.result.detalleSolicitudViaje)
          } else if (response.data.result.intFKIDTipoSolicitud == 3) {

            commit("setOperacionHospedaje", response.data.result.detalleSolicitudHotel)
            response.data.result.detalleSolicitudHotel.lstDtEmpleadoHuespedes.forEach(emp => {
              let nombreCompleto = (emp.chrNombre != null ? emp.chrNombre : "") + ' ' + (emp.chrApellidoPaterno != null ? emp.chrApellidoPaterno : "") + ' ' + (emp.chrApellidoMaterno != null ? emp.chrApellidoMaterno : "")
              nombresEmpleadosHospedaje.push(nombreCompleto);
            })
            commit("setNombresHuespedes", nombresEmpleadosHospedaje)
          }
        })
        .catch(error => {
        })
    },
    getCentroCosto({ state, commit }, payload) {

      commit("setLoadingSelect", true)
      if (payload == undefined) {
        payload = "";
      }
      let objCentroCosto = {
        chrCentroCostos: payload
      }
      const hd = state.headers
      axios.post(
        state.basePath + 'Api/WebServices/GetCentrosCosto/',
        objCentroCosto,
        { hd }
      )
        .then(response => {

          commit("setCentroCosteVuelo", response.data);
          commit("setLoadingSelect", false)

        })
        .catch(error => {

          commit("setLoadingSelect", false)

        })
    },
    cerrarSolicitud({ state }, payload) {
      let idSolicitud = {
        intIDSolicitud: payload
      }
      const hd = state.headers
      axios.post(
        state.basePath + 'Api/Solicitud/CerrarSolicitud/',
        idSolicitud,
        { hd }
      )
        .then(response => {

        })
        .catch(error => {

        })
    },
    notificacionesSolicitudes({ state, commit }, payload) {
      let idSolicitud = {
        intCodigoEmpleado: payload
      }
      const hd = state.headers
      axios.post(
        state.basePath + 'Api/Solicitud/NotificacionesSolicitudes/',
        idSolicitud,
        { hd }
      )
        .then(response => {

          let arrayNotificacion = []
          let rutaPorEstatus = ''

          response.data.result.forEach(data => {
            let nombreCompleto = (data.chrNombre != null ? data.chrNombre : '') + ' ' + (data.chrApellidPaterno != null ? data.chrApellidPaterno : '') + ' ' + (data.chrApellidoMaterno != null ? data.chrApellidoMaterno : '');

            switch (data.intFKIdEstatus) {
              case 1:
              case 3:
                rutaPorEstatus = "/detalle-solicitud-vuelo-hospedaje/" + data.intIDSolicitud
                break;
              case 2:
                if (state.userLogin.intIDPerfil == 2) {
                  rutaPorEstatus = "/detalle-solicitud-vuelo-hospedaje/" + data.intIDSolicitud
                } else if (state.userLogin.intIDPerfil == 3) {
                  rutaPorEstatus = "/prioridad-autorizador?idSolicitud=" + data.intIDSolicitud
                } else {
                  rutaPorEstatus = "/seleccion-propuestas?idSolicitud=" + data.intIDSolicitud
                }
                break;
              case 4:
                if (state.userLogin.intIDPerfil == 2) {
                  rutaPorEstatus = "/detalle-solicitud-vuelo-hospedaje/" + data.intIDSolicitud
                } else if (state.userLogin.intIDPerfil == 3) {
                  rutaPorEstatus = "/prioridad-autorizador?idSolicitud=" + data.intIDSolicitud
                } else {
                  rutaPorEstatus = "/seleccion-propuestas?idSolicitud=" + data.intIDSolicitud
                }
                break;
              case 5:
                if (state.userLogin.intIDPerfil == 2) {
                  rutaPorEstatus = "/compra-cotizacion?idSolicitud=" + data.intIDSolicitud
                } else if (state.userLogin.intIDPerfil == 3) {
                  rutaPorEstatus = "/prioridad-autorizador?idSolicitud=" + data.intIDSolicitud
                } else {
                  rutaPorEstatus = "/seleccion-propuestas?idSolicitud=" + data.intIDSolicitud
                }
                break;
              case 6:
                rutaPorEstatus = "/detalle-vuelo-hospedaje"
                break;
              case 7:
                if (state.userLogin.intIDPerfil == 2) {
                  rutaPorEstatus = "/compra-cotizacion?idSolicitud=" + data.intIDSolicitud
                } else {
                  rutaPorEstatus = "/detalle-vuelo-hospedaje"
                }
                break;
              default:
                break;
            }
            let objNotificacion = {
              nombre: nombreCompleto,
              idSolicitud: data.intIDSolicitud,
              color: data.chrColor,
              idEstatus: data.intFKIdEstatus,
              chrEstatus: data.chrEstatus,
              ruta: rutaPorEstatus
            }
            arrayNotificacion.push(objNotificacion)

          })

          commit("setNotificaciones", arrayNotificacion)
        })
        .catch(error => {

        })
    },
    marcarSolicitudVisto({ state, dispatch }, payload) {
      let objSolicitud = {
        intIDSolicitud: payload
      }
      let hd = state.headers;

      axios.post(state.basePath + 'Api/Solicitud/MarcarSolicitudVisto/',
        objSolicitud,
        { hd }
      )
        .then(response => {

          dispatch("notificacionesSolicitudes", state.userLogin.intIDEmpleado)
        })
        .catch(error => {

        })
    },
    consultaRutaHome({ state, commit }, payload) {
      let hd = state.headers;

      axios.get(state.basePath + 'Api/Catalagos/GetRutaHome/',

        { hd }
      )
        .then(response => {
          Vue.cookie.set('cookieControlViajesPantallaHome', response.data.result, { expires: 1 });
        })
        .catch(error => {
        })
    },
    obtieneIDsDetalleViajeHospedaje({ state, commit }, payload) {

      let idSolicitud = {
        intIDSolicitud: payload.intIDSolicitud
      };
      let hd = state.headers;
      axios
        .post(
          state.basePath + "Api/Solicitud/ObtieneSolicitud/",
          idSolicitud,
          {
            hd
          }
        )
        .then(response => {

          commit('setObtieneIDsDetalleViajeHospedaje', response.data);
        })
        .catch(error => { });
    }
  }
})
