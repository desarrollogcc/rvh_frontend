import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n);

const messages = {
    'es': {
        login: {
            title: 'Iniciar sesión',
            correo: 'Número de colaborador',
            password: 'Contraseña',
            recordarme: 'Recordarme',
            olvidoPass: 'Olvidé mi contraseña',
            entrar: 'Entrar',
            tituloImg: 'Sistema de reservación de vuelos & hospedaje'
        },
        etiquetaSistema: {
            lblEditar: 'Editar',
            lblRechazar: 'Rechazar',
            lblCotizar: 'Cotizar',
            lblCancelar: 'Cancelar',
            lblDescargar: 'Descarga',
            lblEnviar: 'Enviar',
            lblAgregar: 'Agregar',
            lblConsultar: 'Consultar',
            lblEliminar: 'Eliminar',
            lblAcciones: 'Acciones'
        },
        validaciones: {
            login: 'Campo requerido entre 5 y 8 caracteres numéricos.',
            password: 'Campo requerido con 8 o más caracteres alfanuméricos (al menos un número y una letra).',
            requerido: 'Campo requerido. ',
            numeric: 'Sólo caracteres numéricos. '
        },
        menu: {
            bienvenido: '¡Bienvenido!',
            empresa: 'Grupo Comercial Control',
            solicitudes: 'Solicitudes',
            estatus: 'Estatus de solicitud',
            reportes: 'Reportes',
            adminSolicitudes: 'Administrador de solicitudes',
            autorizadorSolicitudes: 'Autorizador de solicitudes',
            hola: 'Hola ',
            solicitudVueloHospedaje: 'Solicitudes de vuelo & hospedaje',
            itinerario: 'Itinerario',
            editPerfil: 'Mi perfil',
            cambiarPass: 'Cambiar contraseña',
            config: 'Configuración',
            faq: 'Preguntas frecuentes',
            cerrarSesion: 'Cerrar sesión',
            // altaUsuarios: 'Altas usuarios'
        },
        breadcrumb: {
            inicio: 'Inicio',
            solicitudVueloHospedaje: 'Solicitudes de vuelo & hospedaje',
            vueloHospedaje: 'Vuelo + Hospedaje',
            vuelo: 'Vuelo',
            hospedaje: 'Hospedaje',
            solicitudes: 'Solicitudes',
            lblDetalleSolicitudVueloHospedaje: 'Detalle solicitud Vuelo + Hospedaje',
            lblDetalleSolicitudes: 'Detalle solicitudes',
            lblDetalleSolicitudHospedaje: 'Detalle solicitud hospedaje',
            lblAdministradorSolcitudes:'Administrador de solicitudes',

        },
        solicitud: {
            lblSolicitud: 'Solicitud',
            lblNumeroFolio: 'Número de folio',
            lblComentarioSolicitud: 'Comentarios de la solicitud',
            lblElHospedajeEsHotel: '¿El hospedaje será hotel?',
            lblSolicitudCreadaExito: '¡Solicitud creada con éxito!',
            lblEnviarAlAdministrador: 'Enviar al administrador de solicitudes',
            lblNumeroColaborador: 'Número del colaborador',
            lblNombredelColaborador: 'Nombre del colaborador',
            lblFechaNacimiento: 'Fecha de nacimiento',
            lblDetalleSolicitudHospedeje: 'Detalle de solicitud de hospedaje',
            lblColTablaNumSolicitud: 'Número de solicitud',
            lblColTablaNumColaborador: 'Número de colaborador',
            lblColTablaNomColaborador: 'Nombre colaborador',
            lblColTablaCentroCosto: 'Centro costo hospedaje',
            lblColTablaHotel: 'Hotel',
            lblColTablaHabitaciones: 'Habitaciones',
            lblColTablaTipoHabitacion: 'Tipo de habitación',
            lblColTablaOrigen: 'Origen',
            lblColTablaDestino: 'Destino',
            lblColTablaFechaEntrada: 'Fecha entrada',
            lblColTablaFechaSalida: 'Fecha de salida',
            lblColTablaNocheReservadas: 'Noches reservadas',
        },
        reservaciones: {
            vueloHospedaje: 'Vuelo + Hospedaje',
            vuelo: 'Vuelo',
            hospedaje: 'Hospedaje',
            crearSolicitud: 'Crear solicitud',
            fechaSolicitud: 'Fecha de solicitud',
            tituloVuelo: 'Vuelo',
            tituloVuelo1: '1. Vuelos',
            numEmpleado: 'Número colaborador',
            nomEmpleado: 'Nombre colaborador',
            tipoViaje: 'Tipo de viaje',
            tipoVuelo: 'Tipo de vuelo',
            destino: 'Destino',
            fechaSalida: 'Fecha de salida',
            centroCosteVuelo: 'Centro de costo de vuelo',
            origen: 'Origen',
            fechaRegreso: 'Fecha de regreso',
            tituloHospedaje: 'Hospedaje',
            tituloHospedaje2: '2. Hospedaje',
            hotel: 'Hotel',
            numHabitaciones: 'Número de habitaciones',
            fechaEntrada: 'Fecha de entrada',
            fechaSalida: 'Fecha de salida',
            fechaNacimiento: 'Fecha de nacimiento',
            motivo: 'Motivo del viaje',
            tipoHabitacion: 'Tipo de habitación',
            aerolinea: 'Aerolínea de preferencia',
            centroCosteHospedaje: 'Centro de costo de hospedaje',
            nochesReservadas: 'Noches reservadas',
            btnEnviarSolicitud: 'Enviar solicitud',
            seleccionar: 'Seleccionar'
        },
        detalleSolicitudes: {
            cotizacion: 'Solicitudes para cotización',
            solicitudes: 'Solicitudes: ',
            fechaInicio: 'Fecha inicio: ',
            fechaFinal: 'Fecha final: ',
            lblMantenimientoVueloHotel: 'Mantenimiento de datos vuelo + hospedaje',
            headerTabla: {
                folioSolicitud: "Folio de solicitud",
                fechaRecepcion: "Fecha de recepción",
                horaRecepcion: "Hora de recepción",
                solicitante: "Solicitante",
                tipoSolicitud: "Tipo de solicitud",
                estatus: "Estatus",
                estado: "Estado",
            }
        },
        datosUsuario: {
            titulo: 'Mi información',
            numEmpleado: 'Número de colaborador',
            nombre: 'Nombre',
            puesto: 'Puesto del solicitante',
            departamento: 'Departamento',
            centroCosto: 'Centro de costo',
            autorizador: 'Autorizador',
        },
        operacionHotelVuelo: {
            lblNumeroDeOpcion: 'Número de opción',
            lblClaveReservacion: 'Clave de reservación',
            lblNumerodeRespuesta: 'Número de respuesta',
            lblFechaRespuesta: 'Fecha de respuesta',
            lblHotel: 'Hotel',
            lblRFCHotel: 'RFC del Hotel',
            lblNumeroHabitacion: 'Número de habitaciones:',
            lblTipoHabitacion: 'Tipo de habitación:',
            lblCostoDelHospedaje: 'Costo del hospedaje',
            lblCostoExtraHospedaje: 'Costo extra del hospedaje',
            lblClaveReservacion: 'Clave de reservación',
            lblTipoVuelo: 'Tipo de vuelo:',
            lblTieneEscalas: '¿Tiene escalas?',
            lblOrigenEscala: 'Origen escala',
            lblDestinoEscala: 'Destino escala',
            lblFechaEscala: 'Fecha salida escala',
            lblComentarios: 'Comentarios',
            lblPlaceHolderComentarioRechazo: 'Escribe un comentario con el motivo del rechazo',
            lblOrigen: 'Origen:',
            lblDestino: 'Destino:',
            lblFechaSalidad: 'Fecha salidad:',
            lblFechaRegreso: 'Fecha de regreso:',
            lblAerolinea: 'Aerolínea:',
            lblInformacionViaje: 'Información viaje',
            lblInformacionHospedaje: 'Información hospedaje',
            lblFechaEntrada: 'Fecha de entrada:',
            lblNumeroHabitaciones: 'Número de habitaciones',


        },
        operacionesGenerales: {
            lblTitularTarjeta: 'Titular de la tarjeta',
            lblNumeroTarjeta: 'Número de tarjeta',
            lblFechaVencimiento: 'Fecha de vencimiento',
            lblCodigoSegurida: 'Código de seguridad'
        },
        reporteDetalleSolicitudVueloHospedaje: {
            lblTituloTabla: 'Detalle solicitud de vuelo + hospedaje número ',
            lblMensajeSolicitudEnviada: 'Solicitud enviada a tu administrador.',
            lblEnviarAdministrador: 'Enviar al administrador',
            lblEditarComoAdministrador: 'Editar como administrador',
            lblRechazarSolicitud: 'Rechazar la solicitud',
            lblComentario: 'Escribe un comentario con el motivo del rechazo',
            lblDetalleSolicitudVuelo: 'Detalle solicitud de vuelo número',
            lblDetalleSolicitud: 'Detalle solicitud de hospedaje número '
        },
        compraCotizaciones: {
            lblCompraCotizaciones: 'Compra de cotizaciones',
            lblCotizacionesVuelo: 'Cotizaciones de vuelo',
            lblCotizacionesHospedaje: 'Cotizaciones de hospedaje',
            lblColaboradores: 'Colaboradores',
            lblCargaArchivos: 'Carga de archivos xml y pdf',
            lblSubirArchivos: 'Cargar archivos',
            lblArchivos: 'archivos',
            lblCorreoRecibira: 'Correo que recibirá la carta',
            lblDescargaPDF: 'Descargar PDF',
            lblGenerarCarta: 'Generar carta',
            lblReservacionCreada: 'Reservación creada con éxito.',
            lblTitularTarjeta: 'Titular de la tarjeta',
            lblNumeroTarjeta: 'Número de tarjeta',
            lblNombreBanco: 'Nombre del banco',
            lblFechaDeVencimiento: 'Fecha de vencimiento',
            lblCodigoSeguridad: 'Código de seguridad',
        },
        prioridadesCotizadas: {
            lblSelecionaPropuesta: 'Selección de propuestas',
            lblRechazarCotizacion: 'Rechazar cotización',
            lblMotivodeRechazo: 'Escribe un comentario con el motivo del rechazo',
            lblPrioridadesAsignadaExito: 'Prioridades asignadas con éxito.',
            lblEligePrioridades: 'Elige prioridades para todos los campos.',
            lblEligePrioridadesNoRepetidad: 'Elige prioridades no repetidas.',
        },
        autorizadorSolicitudes: {
            lblAutorizadorSolicitudes: 'Autorizador de solicitudes',
            lblSolicitudAutoriza: 'Solicitudes para autorizar',
            lblColTablaNumSolicitud: 'Número de solicitud',
            lblColTablaFechaEnvio: 'Fecha de envío',
            lblColTablaHoraRecepcion: 'Hora de recepción',
            lblColTablaFechaCotizacion: 'Fecha de cotización',
            lblColTablaHoraCotizacion: 'Hora de cotización',
            lblColTablaFechaAutorizacion: 'Fecha de autorización',
            lblColTablaSolicitante: 'Solicitante',
            lblColTablaTipoSolicitud: 'Tipo de solicitud',
            lblColTablaEstatus: 'Estatus',
            lblColTablaMotivoViaje: 'Motivo de viaje',
            lblColTablaNumeroCotizaciones: 'Número de cotizaciones'

        },
        router: {
            lblAdministradorSolicitudes: 'Administrador de solicitudes',
            lblAutorizadorSolicitudes: 'Autorizador de solicitudes',
            lblSeleccionPropuesta: 'Selección de propuestas',
        }
    },
    'en': {
    }
};

const i18n = new VueI18n({
    locale: 'es', // set locale
    fallbackLocale: 'en', // set fallback locale
    messages, // set locale messages
});

export default i18n;