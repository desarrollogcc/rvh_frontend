-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: sfseguridad
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.44-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accesosSesion`
--

DROP TABLE IF EXISTS `accesosSesion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accesosSesion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idSesion` varchar(36) DEFAULT NULL COMMENT 'Identificador de la sesion',
  `idSistema` varchar(10) DEFAULT NULL COMMENT 'Identificador del sistema.',
  `idModulo` varchar(20) DEFAULT NULL COMMENT 'Identificador del modulo.',
  `descripcionModulo` varchar(100) DEFAULT NULL COMMENT 'Descripcion del modulo',
  `idModuloPadre` varchar(20) DEFAULT NULL COMMENT 'Identificador del modulo padre ',
  `URL` varchar(200) DEFAULT NULL COMMENT 'URL del sistema',
  `pagina` varchar(100) DEFAULT NULL COMMENT 'Nombre de la página del modulo.',
  `imagenIcono` varchar(50) DEFAULT NULL COMMENT 'Nombre de la imagen que servira como icono en el menu de opciones.',
  `orden1` int(10) DEFAULT NULL COMMENT 'Ordenamiento en el menu de opciones 1',
  `orden2` int(10) DEFAULT NULL COMMENT 'Ordenamiento en el menu de opciones 2',
  `orden3` int(10) DEFAULT NULL COMMENT 'Ordenamiento en el menu de opciones 3',
  `orden4` int(10) DEFAULT NULL COMMENT 'Ordenamiento en el menu de opciones 4',
  `orden5` int(10) DEFAULT NULL COMMENT 'Ordenamiento en el menu de opciones 5',
  PRIMARY KEY (`id`),
  KEY `sesiones_accesosSesion` (`idSesion`),
  CONSTRAINT `sesiones_accesosSesion` FOREIGN KEY (`idSesion`) REFERENCES `sesiones` (`idSesion`)
) ENGINE=InnoDB AUTO_INCREMENT=5186 DEFAULT CHARSET=latin1 COMMENT='Tabla que contiene los accesos permitidos para la sesion.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accesosSesion`
--

LOCK TABLES `accesosSesion` WRITE;
/*!40000 ALTER TABLE `accesosSesion` DISABLE KEYS */;
INSERT INTO `accesosSesion` VALUES (234,'e8ac9f94-f441-11e9-a0f7-92b550b0bcb8','1','1','Registro','0','','menu',NULL,NULL,NULL,NULL,NULL,NULL),(235,'e8ac9f94-f441-11e9-a0f7-92b550b0bcb8','1','2','Solicitud de reservaciones','0','','reservaciones',NULL,NULL,NULL,NULL,NULL,NULL),(3714,'5ea9cd16-0260-11ea-aab4-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(3715,'5ea9cd16-0260-11ea-aab4-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(3716,'5ea9cd16-0260-11ea-aab4-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(3717,'d727cc05-026e-11ea-aab4-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(3718,'d727cc05-026e-11ea-aab4-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(3719,'d727cc05-026e-11ea-aab4-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(3720,'ee6f4768-026e-11ea-aab4-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(3721,'ee6f4768-026e-11ea-aab4-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(3722,'ee6f4768-026e-11ea-aab4-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(3723,'16b98951-026f-11ea-aab4-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(3724,'16b98951-026f-11ea-aab4-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(3725,'16b98951-026f-11ea-aab4-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(3726,'3594516e-026f-11ea-aab4-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(3727,'3594516e-026f-11ea-aab4-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(3728,'3594516e-026f-11ea-aab4-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(3729,'4f1e7a84-026f-11ea-aab4-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(3730,'4f1e7a84-026f-11ea-aab4-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(3731,'4f1e7a84-026f-11ea-aab4-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(3732,'6294a4d0-026f-11ea-aab4-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(3733,'6294a4d0-026f-11ea-aab4-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(3734,'6294a4d0-026f-11ea-aab4-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(3735,'709b6229-026f-11ea-aab4-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(3736,'709b6229-026f-11ea-aab4-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(3737,'709b6229-026f-11ea-aab4-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(3744,'aa2f5f56-026f-11ea-aab4-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(3745,'aa2f5f56-026f-11ea-aab4-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(3746,'aa2f5f56-026f-11ea-aab4-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(3747,'aa2f5f56-026f-11ea-aab4-92b550b0bcb8','1','4','Administrador de solicitudes','','HTTP://10.125.3.35/RESERVACIONES','administrador-solicitudes',NULL,0,0,0,0,0),(3748,'aa2f5f56-026f-11ea-aab4-92b550b0bcb8','1','5','Autorizador solicitudes','','HTTP://10.125.3.35/RESERVACIONES','autorizador-solicitudes',NULL,0,0,0,0,0),(3749,'aa2f5f56-026f-11ea-aab4-92b550b0bcb8','1','6','Configuracion','','HTTP://10.125.3.35/RESERVACIONES','configuracion',NULL,2,0,0,0,0),(3765,'e9e60bf4-026f-11ea-aab4-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(3766,'e9e60bf4-026f-11ea-aab4-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(3767,'e9e60bf4-026f-11ea-aab4-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(4068,'460635d8-2124-11ea-8a12-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(4069,'460635d8-2124-11ea-8a12-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(4070,'460635d8-2124-11ea-8a12-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(4240,'402ab9a0-2298-11ea-8a12-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(4241,'402ab9a0-2298-11ea-8a12-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(4242,'402ab9a0-2298-11ea-8a12-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(4339,'f8ed1c67-22aa-11ea-8a12-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(4340,'f8ed1c67-22aa-11ea-8a12-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(4341,'f8ed1c67-22aa-11ea-8a12-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(4342,'f8ed1c67-22aa-11ea-8a12-92b550b0bcb8','1','5','Autorizador solicitudes','','HTTP://10.125.3.35/RESERVACIONES','autorizador-solicitudes',NULL,0,0,0,0,0),(4430,'2de061be-3183-11ea-8a12-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(4431,'2de061be-3183-11ea-8a12-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(4432,'2de061be-3183-11ea-8a12-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(4767,'de8df652-47a1-11ea-a953-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(4768,'de8df652-47a1-11ea-a953-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(4769,'de8df652-47a1-11ea-a953-92b550b0bcb8','1','4','Administrador de solicitudes','','HTTP://10.125.3.35/RESERVACIONES','administrador-solicitudes',NULL,0,0,0,0,0),(4770,'de8df652-47a1-11ea-a953-92b550b0bcb8','1','6','Configuracion','','HTTP://10.125.3.35/RESERVACIONES','configuracion',NULL,2,0,0,0,0),(5155,'7c3d9cc4-54fa-11ea-8724-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(5156,'7c3d9cc4-54fa-11ea-8724-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(5157,'7c3d9cc4-54fa-11ea-8724-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(5158,'7c3d9cc4-54fa-11ea-8724-92b550b0bcb8','1','5','Autorizador solicitudes','','HTTP://10.125.3.35/RESERVACIONES','autorizador-solicitudes',NULL,0,0,0,0,0),(5176,'a23c48ba-56ae-11ea-8724-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(5177,'a23c48ba-56ae-11ea-8724-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(5178,'a23c48ba-56ae-11ea-8724-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(5179,'4e0697ee-57ff-11ea-8724-92b550b0bcb8','1','1','Solicitudes','','HTTP://10.125.3.35/RESERVACIONES','solicitud-de-reservaciones',NULL,0,0,0,0,0),(5180,'4e0697ee-57ff-11ea-8724-92b550b0bcb8','1','2','Estatus de solicitud','','HTTP://10.125.3.35/RESERVACIONES','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),(5181,'4e0697ee-57ff-11ea-8724-92b550b0bcb8','1','3','Reportes','','HTTP://10.125.3.35/RESERVACIONES','reportes',NULL,0,0,0,0,0),(5182,'4e0697ee-57ff-11ea-8724-92b550b0bcb8','1','4','Administrador de solicitudes','','HTTP://10.125.3.35/RESERVACIONES','administrador-solicitudes',NULL,0,0,0,0,0);
/*!40000 ALTER TABLE `accesosSesion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bitacoraAccesos`
--

DROP TABLE IF EXISTS `bitacoraAccesos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bitacoraAccesos` (
  `idUsuario` varchar(10) DEFAULT NULL COMMENT 'Identificador del usuario',
  `idSistema` varchar(10) DEFAULT NULL COMMENT 'Identificador del sistema al que hizo login',
  `idModulo` varchar(20) DEFAULT NULL COMMENT 'Identificador del modulo al que hizo acceso',
  `fechaAcceso` datetime DEFAULT NULL COMMENT 'Fecha y hora del acceso al modulo del sistema',
  `idSesion` varchar(36) DEFAULT NULL COMMENT 'Id de la sesion abierta por el usuario',
  `usuarioEspecial` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Bitacora de accesos a los sistemas B2B';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bitacoraAccesos`
--

LOCK TABLES `bitacoraAccesos` WRITE;
/*!40000 ALTER TABLE `bitacoraAccesos` DISABLE KEYS */;
INSERT INTO `bitacoraAccesos` VALUES ('1','1','1','2019-08-01 09:27:17','7216c4e1-b468-11e9-8681-92b550b0bcb8',NULL),('1','1','1','2019-08-01 09:29:16','7216c4e1-b468-11e9-8681-92b550b0bcb8',NULL),('1','1','1','2019-08-01 19:11:20','7216c4e1-b468-11e9-8681-92b550b0bcb8',NULL),('1','1','1','2019-08-01 19:11:22','7216c4e1-b468-11e9-8681-92b550b0bcb8',NULL),('1','1','1','2019-08-01 19:11:22','7216c4e1-b468-11e9-8681-92b550b0bcb8',NULL),('1','1','1','2019-08-01 19:11:23','7216c4e1-b468-11e9-8681-92b550b0bcb8',NULL),('1','1','1','2019-08-05 09:54:27','e011e12e-b78c-11e9-8681-92b550b0bcb8',NULL),('1','1','1','2019-08-05 09:54:50','e011e12e-b78c-11e9-8681-92b550b0bcb8',NULL),('1','1','1','2019-08-14 12:48:25','999ec15e-bebb-11e9-8694-92b550b0bcb8',NULL),('1','1','1','2019-08-23 11:51:49','41d8881a-c5c6-11e9-8694-92b550b0bcb8',NULL),('1','1','1','2019-08-23 13:28:43','c920043e-c5d3-11e9-8694-92b550b0bcb8',NULL),('1','1','1','2019-10-07 16:48:29','15282a56-e94c-11e9-a0f7-92b550b0bcb8',NULL),('1','1','1','2019-10-09 08:42:55','ab9418f7-ea9a-11e9-a0f7-92b550b0bcb8',NULL),('1','1','1','2019-10-09 08:46:38','31165053-ea9b-11e9-a0f7-92b550b0bcb8',NULL),('1','1','1','2019-10-09 08:49:25','31165053-ea9b-11e9-a0f7-92b550b0bcb8',NULL);
/*!40000 ALTER TABLE `bitacoraAccesos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogos`
--

DROP TABLE IF EXISTS `catalogos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogos` (
  `idCatalogo` int(11) NOT NULL,
  `NombreTabla` varchar(50) NOT NULL,
  `idTabla` varchar(50) DEFAULT NULL,
  `DescTabla` varchar(50) DEFAULT NULL,
  `IdStatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCatalogo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogos`
--

LOCK TABLES `catalogos` WRITE;
/*!40000 ALTER TABLE `catalogos` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalogos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comentariosbajas`
--

DROP TABLE IF EXISTS `comentariosbajas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comentariosbajas` (
  `IdComentario` int(10) NOT NULL AUTO_INCREMENT,
  `IdUsuarioBaja` varchar(50) DEFAULT NULL COMMENT 'usuario de baja',
  `IdUsuarioAdm` varchar(50) DEFAULT NULL COMMENT 'usuario que realizo la baja',
  `Fecha` datetime DEFAULT NULL,
  `Comentario` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`IdComentario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comentariosbajas`
--

LOCK TABLES `comentariosbajas` WRITE;
/*!40000 ALTER TABLE `comentariosbajas` DISABLE KEYS */;
/*!40000 ALTER TABLE `comentariosbajas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compania`
--

DROP TABLE IF EXISTS `compania`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compania` (
  `idCompania` int(11) NOT NULL,
  `Abreviacion` varchar(10) NOT NULL,
  `Descripcion` varchar(100) NOT NULL,
  `Estatus` int(11) NOT NULL,
  `FechaBaja` date DEFAULT NULL,
  `UsuarioBaja` date DEFAULT NULL,
  PRIMARY KEY (`idCompania`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compania`
--

LOCK TABLES `compania` WRITE;
/*!40000 ALTER TABLE `compania` DISABLE KEYS */;
/*!40000 ALTER TABLE `compania` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conexiones`
--

DROP TABLE IF EXISTS `conexiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conexiones` (
  `idBaseDatos` varchar(10) NOT NULL COMMENT 'Identificador de base de datos',
  `nombreBaseDatos` varchar(40) DEFAULT NULL COMMENT 'Nombre de la base de datos',
  `cadenaConexion` varchar(100) DEFAULT NULL COMMENT 'Cadena de conexión para .NET',
  PRIMARY KEY (`idBaseDatos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla de conexiones a las bases de datos';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conexiones`
--

LOCK TABLES `conexiones` WRITE;
/*!40000 ALTER TABLE `conexiones` DISABLE KEYS */;
/*!40000 ALTER TABLE `conexiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `correossoporte`
--

DROP TABLE IF EXISTS `correossoporte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `correossoporte` (
  `idCorreos` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador unico del registro (autonumerico)',
  `cuentasEmail` varchar(500) NOT NULL COMMENT 'Cuentas de correo del grupo de soporte a los sistemas los correos van separados por ;',
  `idSistema` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idCorreos`),
  KEY `sistemas_correosSoporte` (`idSistema`),
  CONSTRAINT `sistemas_correosSoporte` FOREIGN KEY (`idSistema`) REFERENCES `sistemas` (`idSistema`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `correossoporte`
--

LOCK TABLES `correossoporte` WRITE;
/*!40000 ALTER TABLE `correossoporte` DISABLE KEYS */;
/*!40000 ALTER TABLE `correossoporte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filtros`
--

DROP TABLE IF EXISTS `filtros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filtros` (
  `IdFiltro` int(11) DEFAULT NULL,
  `Filtro` varchar(50) DEFAULT NULL,
  `NombreTabla` varchar(50) DEFAULT NULL,
  `ColumnaID` varchar(50) DEFAULT NULL,
  `ColumnaDesc` varchar(50) DEFAULT NULL,
  `IdStatus` int(3) DEFAULT NULL,
  `IdSistema` varchar(10) DEFAULT NULL,
  `IP` varchar(50) DEFAULT NULL,
  `BaseDatos` varchar(50) DEFAULT NULL,
  `Usuario` varchar(50) DEFAULT NULL,
  `Password` varchar(50) DEFAULT NULL,
  `Puerto` varchar(10) DEFAULT NULL,
  KEY `FK__sistemas` (`IdSistema`),
  KEY `FK__status` (`IdStatus`),
  CONSTRAINT `FK__sistemas` FOREIGN KEY (`IdSistema`) REFERENCES `sistemas` (`idSistema`),
  CONSTRAINT `FK__status` FOREIGN KEY (`IdStatus`) REFERENCES `status` (`IdStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filtros`
--

LOCK TABLES `filtros` WRITE;
/*!40000 ALTER TABLE `filtros` DISABLE KEYS */;
/*!40000 ALTER TABLE `filtros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filtrostablas`
--

DROP TABLE IF EXISTS `filtrostablas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filtrostablas` (
  `IdTabla` int(11) NOT NULL,
  `NombreTabla` varchar(50) DEFAULT NULL,
  `IdStatus` int(3) DEFAULT NULL,
  PRIMARY KEY (`IdTabla`),
  KEY `FK__status2` (`IdStatus`),
  CONSTRAINT `FK__status2` FOREIGN KEY (`IdStatus`) REFERENCES `status` (`IdStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filtrostablas`
--

LOCK TABLES `filtrostablas` WRITE;
/*!40000 ALTER TABLE `filtrostablas` DISABLE KEYS */;
/*!40000 ALTER TABLE `filtrostablas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filtrousuarios`
--

DROP TABLE IF EXISTS `filtrousuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filtrousuarios` (
  `IdFiltro` int(11) NOT NULL,
  `IdUsuario` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `IdValor` varchar(50) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filtrousuarios`
--

LOCK TABLES `filtrousuarios` WRITE;
/*!40000 ALTER TABLE `filtrousuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `filtrousuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupoUsuarios`
--

DROP TABLE IF EXISTS `grupoUsuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupoUsuarios` (
  `idGrupo` int(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador unico del grupo de usuarios (auto-numerico)',
  `descripcionGrupo` varchar(50) DEFAULT NULL COMMENT 'Descripcion o nombre del grupo de usuarios',
  `esPublico` char(1) DEFAULT 'N' COMMENT 'Indica si el grupo es para todos los usuarios (asignado por default) valores S o N',
  PRIMARY KEY (`idGrupo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Tabla que agrupa usuarios que comparten las mismos modulos de los diferentes sistemas';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupoUsuarios`
--

LOCK TABLES `grupoUsuarios` WRITE;
/*!40000 ALTER TABLE `grupoUsuarios` DISABLE KEYS */;
INSERT INTO `grupoUsuarios` VALUES (1,'GlobalSoft','N');
/*!40000 ALTER TABLE `grupoUsuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupoUsuariosModulos`
--

DROP TABLE IF EXISTS `grupoUsuariosModulos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupoUsuariosModulos` (
  `idGrupo` int(11) NOT NULL COMMENT 'Identificador de grupo',
  `idSistema` varchar(10) NOT NULL COMMENT 'Identificador de sistema',
  `idModulo` varchar(20) NOT NULL COMMENT 'Identificador del modulo',
  PRIMARY KEY (`idGrupo`,`idSistema`,`idModulo`),
  KEY `modulos_grupoUsuariosModulos` (`idSistema`,`idModulo`),
  CONSTRAINT `grupoUsuarios_grupoUsuariosModulos` FOREIGN KEY (`idGrupo`) REFERENCES `grupoUsuarios` (`idGrupo`),
  CONSTRAINT `modulos_grupoUsuariosModulos` FOREIGN KEY (`idSistema`, `idModulo`) REFERENCES `modulos` (`idSistema`, `idModulo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla de relacion entre los modulos y los grupos de usuario';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupoUsuariosModulos`
--

LOCK TABLES `grupoUsuariosModulos` WRITE;
/*!40000 ALTER TABLE `grupoUsuariosModulos` DISABLE KEYS */;
INSERT INTO `grupoUsuariosModulos` VALUES (1,'1','1');
/*!40000 ALTER TABLE `grupoUsuariosModulos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulos`
--

DROP TABLE IF EXISTS `modulos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulos` (
  `idSistema` varchar(10) NOT NULL COMMENT 'Identificador unico del sistema',
  `idModulo` varchar(20) NOT NULL COMMENT 'Identificador unico del modulo',
  `descripcionModulo` varchar(100) NOT NULL COMMENT 'Descripcion o nombre del modulo',
  `idModuloPadre` varchar(20) DEFAULT NULL COMMENT 'En caso de relacionarse consigo mismo representa el id del modulo con el que se relaciona',
  `pagina` varchar(100) DEFAULT NULL COMMENT 'Pagina Web del modulo, aplica solamente cuando el modulo no tiene sub-modulos',
  `imagenIcono` varchar(50) DEFAULT NULL,
  `orden1` int(11) DEFAULT NULL COMMENT 'Ordenamiento en el menu de opciones 1',
  `orden2` int(11) DEFAULT NULL COMMENT 'Ordenamiento en el menu de opciones 2',
  `orden3` int(11) DEFAULT NULL COMMENT 'Ordenamiento en el menu de opciones 3',
  `orden4` int(11) DEFAULT NULL COMMENT 'Ordenamiento en el menu de opciones 4',
  `orden5` int(11) DEFAULT NULL COMMENT 'Ordenamiento en el menu de opciones 5',
  PRIMARY KEY (`idSistema`,`idModulo`),
  CONSTRAINT `sistemas_modulos` FOREIGN KEY (`idSistema`) REFERENCES `sistemas` (`idSistema`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Modulos de un sistema';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulos`
--

LOCK TABLES `modulos` WRITE;
/*!40000 ALTER TABLE `modulos` DISABLE KEYS */;
INSERT INTO `modulos` VALUES ('1','1','Solicitudes','','solicitud-de-reservaciones',NULL,0,0,0,0,0),('1','2','Estatus de solicitud','','detalle-vuelo-hospedaje',NULL,1,0,0,0,0),('1','3','Reportes','','reportes',NULL,0,0,0,0,0),('1','4','Administrador de solicitudes','','administrador-solicitudes',NULL,0,0,0,0,0),('1','5','Autorizador solicitudes','','autorizador-solicitudes',NULL,0,0,0,0,0),('1','6','Configuracion','','configuracion',NULL,2,0,0,0,0);
/*!40000 ALTER TABLE `modulos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reglanegocios`
--

DROP TABLE IF EXISTS `reglanegocios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reglanegocios` (
  `idRegla` varchar(10) NOT NULL COMMENT 'Identificador unico de la regla de negocio.',
  `descripcionRegla` varchar(250) DEFAULT NULL COMMENT 'Descripcion detallada de la regla de negocio.',
  `query` varchar(1024) DEFAULT NULL COMMENT 'Sentencia SQL o SP que ejecuta la regla de negocio.',
  `idBaseDatos` varchar(10) DEFAULT NULL COMMENT 'Identificador de base de datos sobre la cual se ejecuta la sentencia SQL de la regla de negocio.',
  PRIMARY KEY (`idRegla`),
  KEY `conexiones_reglaNegocios` (`idBaseDatos`),
  CONSTRAINT `conexiones_reglaNegocios` FOREIGN KEY (`idBaseDatos`) REFERENCES `conexiones` (`idBaseDatos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reglanegocios`
--

LOCK TABLES `reglanegocios` WRITE;
/*!40000 ALTER TABLE `reglanegocios` DISABLE KEYS */;
/*!40000 ALTER TABLE `reglanegocios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reglanegocios_old`
--

DROP TABLE IF EXISTS `reglanegocios_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reglanegocios_old` (
  `idRegla` int(11) NOT NULL AUTO_INCREMENT,
  `idModulo` varchar(20) NOT NULL,
  `objeto` varchar(40) NOT NULL,
  `query` varchar(1024) DEFAULT NULL,
  `idSistema` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idRegla`,`idModulo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Reglas de negocio para cada objeto de la pagina';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reglanegocios_old`
--

LOCK TABLES `reglanegocios_old` WRITE;
/*!40000 ALTER TABLE `reglanegocios_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `reglanegocios_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `idRol` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador unico del rol o perfil auto-numerico',
  `descripcionRol` varchar(50) DEFAULT NULL COMMENT 'Descripcion del rol o perfil',
  PRIMARY KEY (`idRol`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Roles o perfiles de usuario';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrador');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rolesModulos`
--

DROP TABLE IF EXISTS `rolesModulos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rolesModulos` (
  `idRol` int(11) NOT NULL COMMENT 'Identificador unico del rol o perfil',
  `idSistema` varchar(10) NOT NULL COMMENT 'Identificador unico del sistema que forma parte del rol o perfil',
  `idModulo` varchar(20) NOT NULL COMMENT 'Identificador unico del modulo del que forma parte el rol o perfil',
  PRIMARY KEY (`idRol`,`idSistema`,`idModulo`),
  KEY `modulos_rolesModulos` (`idSistema`,`idModulo`),
  CONSTRAINT `modulos_rolesModulos` FOREIGN KEY (`idSistema`, `idModulo`) REFERENCES `modulos` (`idSistema`, `idModulo`),
  CONSTRAINT `roles_rolesModulos` FOREIGN KEY (`idRol`) REFERENCES `roles` (`idRol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla que relaciona los roles con los modulos de los sistemas';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rolesModulos`
--

LOCK TABLES `rolesModulos` WRITE;
/*!40000 ALTER TABLE `rolesModulos` DISABLE KEYS */;
INSERT INTO `rolesModulos` VALUES (1,'1','1');
/*!40000 ALTER TABLE `rolesModulos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rolesUsuarios`
--

DROP TABLE IF EXISTS `rolesUsuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rolesUsuarios` (
  `idRol` int(11) NOT NULL,
  `idUsuario` varchar(10) NOT NULL,
  PRIMARY KEY (`idRol`,`idUsuario`),
  CONSTRAINT `roles_rolesUsuarios` FOREIGN KEY (`idRol`) REFERENCES `roles` (`idRol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla que relaciona los roles a los que pertenece un usuario';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rolesUsuarios`
--

LOCK TABLES `rolesUsuarios` WRITE;
/*!40000 ALTER TABLE `rolesUsuarios` DISABLE KEYS */;
INSERT INTO `rolesUsuarios` VALUES (1,'1');
/*!40000 ALTER TABLE `rolesUsuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sesiones`
--

DROP TABLE IF EXISTS `sesiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sesiones` (
  `idSesion` varchar(36) NOT NULL COMMENT 'UUID asignado a la sesion.',
  `fechaSesion` datetime DEFAULT NULL COMMENT 'Fecha hora de inicio de la sesion.',
  `idUsuario` varchar(10) DEFAULT NULL COMMENT 'Identificador de usuario o username.',
  `contrasena` varchar(50) DEFAULT NULL COMMENT 'Password del usuario.',
  `correoElectronico` varchar(50) DEFAULT NULL COMMENT 'Correo electronico del usuario',
  `idTipoUsuario` varchar(10) DEFAULT NULL COMMENT 'Identificador del tipo de usuario.',
  `privilegio` char(1) DEFAULT NULL COMMENT 'Privilegio del usuario A=Admin, U=Usuario final.',
  `usuarioEspecial` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idSesion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla que almacena las sesiones de los usuarios conectados a B2B.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sesiones`
--

LOCK TABLES `sesiones` WRITE;
/*!40000 ALTER TABLE `sesiones` DISABLE KEYS */;
INSERT INTO `sesiones` VALUES ('0af7c931-065e-11ea-aab4-92b550b0bcb8','2019-11-13 15:39:15','10043872','vm2007123','prueba4@globalsoftm.com','1','U',NULL),('16b98951-026f-11ea-aab4-92b550b0bcb8','2019-11-08 15:31:12','341974','vm2007123','prueba1@globalsoftm.com','1','U',NULL),('2de061be-3183-11ea-8a12-92b550b0bcb8','2020-01-07 13:23:26','1000028','vm2007123','prueba3@globalsoftm.com','1','U',NULL),('3594516e-026f-11ea-aab4-92b550b0bcb8','2019-11-08 15:32:04','280966','vm2007123','amundo@globalsoftm.com','1','U',NULL),('402ab9a0-2298-11ea-8a12-92b550b0bcb8','2019-12-19 13:46:29','9013758','vm2007123','prueba2@globalsoftm.com','1','U',NULL),('460635d8-2124-11ea-8a12-92b550b0bcb8','2019-12-17 17:23:46','01000014','control2019','prueba4@globalsoftm.com','1','U',NULL),('48af566c-001b-11ea-aab4-92b550b0bcb8','2019-11-05 16:26:16','1000014','vm2007123','prueba4@globalsoftm.com','1','U',NULL),('4e0697ee-57ff-11ea-8724-92b550b0bcb8','2020-02-25 12:47:42','1000031','vm2007123','prueba4@globalsoftm.com','1','A',NULL),('4f1e7a84-026f-11ea-aab4-92b550b0bcb8','2019-11-08 15:32:47','1960012','vm2007123','prueba3@globalsoftm.com','1','U',NULL),('5ea9cd16-0260-11ea-aab4-92b550b0bcb8','2019-11-08 13:45:50','100043872','vm2007123','prueba4@globalsoftm.com','1','U',NULL),('6294a4d0-026f-11ea-aab4-92b550b0bcb8','2019-11-08 15:33:19','10038070','vm2007123','prueba3@globalsoftm.com','1','U',NULL),('709b6229-026f-11ea-aab4-92b550b0bcb8','2019-11-08 15:33:43','938548','vm2007123','prueba4@globalsoftm.com','1','U',NULL),('7c3d9cc4-54fa-11ea-8724-92b550b0bcb8','2020-02-21 16:35:39','10019170','vm2007123','prueba2@globalsoftm.com','1','D',NULL),('a23c48ba-56ae-11ea-8724-92b550b0bcb8','2020-02-23 20:37:43','1000029','vm2007123','prueba3@globalsoftm.com','1','U',NULL),('a9ddc54a-eb63-11e9-a0f7-92b550b0bcb8','2019-10-10 08:41:29','12345','123456','jprieto@globalsoftm.com','1','1',NULL),('aa2f5f56-026f-11ea-aab4-92b550b0bcb8','2019-11-08 15:35:19','1000032','vm2007123','prueba4@globalsoftm.com','1','E',NULL),('d727cc05-026e-11ea-aab4-92b550b0bcb8','2019-11-08 15:29:25','825117','vm2007123','prueba3@globalsoftm.com','1','U',NULL),('de8df652-47a1-11ea-a953-92b550b0bcb8','2020-02-04 16:58:33','10015149','vm2007123','prueba3@globalsoftm.com','1','A',NULL),('e8ac9f94-f441-11e9-a0f7-92b550b0bcb8','2019-10-21 15:32:32','1','123456','jprieto@globalsoftm.com','1','1',NULL),('e9e60bf4-026f-11ea-aab4-92b550b0bcb8','2019-11-08 15:37:06','10034873','vm2007123','prueba2@globalsoftm.com','1','B',NULL),('ee6f4768-026e-11ea-aab4-92b550b0bcb8','2019-11-08 15:30:04','448761','vm2007123','prueba2@globalsoftm.com','1','U',NULL),('f8ed1c67-22aa-11ea-8a12-92b550b0bcb8','2019-12-19 16:00:30','10019171','vm2007123','prueba4@globalsoftm.com','1','D',NULL),('f99a69e1-4d2e-11ea-8724-92b550b0bcb8','2020-02-11 18:31:13','10036280','vm2007123','soporte@globalsoftm.com','1','E',NULL);
/*!40000 ALTER TABLE `sesiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sistemas`
--

DROP TABLE IF EXISTS `sistemas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sistemas` (
  `idSistema` varchar(10) NOT NULL COMMENT 'Identificador unico del sistema',
  `nombreSistema` varchar(50) NOT NULL COMMENT 'Nombre descriptivo del sistema',
  `URL` varchar(200) NOT NULL,
  `Filtro` int(1) DEFAULT NULL,
  PRIMARY KEY (`idSistema`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Sistemas que forman parte del B2B';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sistemas`
--

LOCK TABLES `sistemas` WRITE;
/*!40000 ALTER TABLE `sistemas` DISABLE KEYS */;
INSERT INTO `sistemas` VALUES ('1','Control de Viajes','HTTP://10.125.3.35/RESERVACIONES',NULL);
/*!40000 ALTER TABLE `sistemas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `IdStatus` int(3) NOT NULL DEFAULT '0',
  `Descripcion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`IdStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `superusuarios`
--

DROP TABLE IF EXISTS `superusuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `superusuarios` (
  `usuario` varchar(10) NOT NULL COMMENT 'Username del usuario',
  `contrasena` varchar(10) DEFAULT NULL COMMENT 'Contraseña',
  `nombreUsuario` varchar(50) DEFAULT NULL COMMENT 'Nombre del usuario',
  PRIMARY KEY (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Super usuarios del sistema, podran acceder a todos los modulos';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `superusuarios`
--

LOCK TABLES `superusuarios` WRITE;
/*!40000 ALTER TABLE `superusuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `superusuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipousuarios`
--

DROP TABLE IF EXISTS `tipousuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipousuarios` (
  `idTipoUsuario` varchar(10) NOT NULL COMMENT 'Identificador unico del tipo de usuario (manejar claves, ADMIN, USER, PROV, etc)',
  `descripcionTipoUsuario` varchar(50) DEFAULT NULL COMMENT 'Descripción del tipo de usuario',
  PRIMARY KEY (`idTipoUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tipos de usuario del sistema B2B';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipousuarios`
--

LOCK TABLES `tipousuarios` WRITE;
/*!40000 ALTER TABLE `tipousuarios` DISABLE KEYS */;
INSERT INTO `tipousuarios` VALUES ('1','Viajes');
/*!40000 ALTER TABLE `tipousuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_empleado`
--

DROP TABLE IF EXISTS `usuario_empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_empleado` (
  `idUsuario` varchar(10) DEFAULT NULL,
  `noEmpleado` varchar(50) DEFAULT NULL,
  `nombre` varchar(300) DEFAULT NULL,
  `razonsocial` varchar(100) DEFAULT NULL,
  `puesto` varchar(100) DEFAULT NULL,
  `departamento` varchar(100) DEFAULT NULL,
  `centroCosto` varchar(100) DEFAULT NULL,
  `fechaNac` date DEFAULT NULL,
  `Autorizador` varchar(50) DEFAULT NULL,
  `correoAutorizador` varchar(100) DEFAULT NULL,
  `nombreAutorizador` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_empleado`
--

LOCK TABLES `usuario_empleado` WRITE;
/*!40000 ALTER TABLE `usuario_empleado` DISABLE KEYS */;
INSERT INTO `usuario_empleado` VALUES ('1000028','1000028','Francisco Javier Rodríguez Villanueva','Control DOME S.A. de C.V.','SUBDIRECTOR DE COMPRAS','SUBDIRECCION DE ROPA','OF11320000','1978-11-18','10019170','Héctor Soso León','prueba2@globalsoftm.com');
/*!40000 ALTER TABLE `usuario_empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_movil`
--

DROP TABLE IF EXISTS `usuario_movil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_movil` (
  `idUsuario_Movil` int(11) NOT NULL AUTO_INCREMENT,
  `cveUsuarioRed` varchar(50) DEFAULT NULL,
  `idsistema` varchar(10) DEFAULT NULL,
  `usuarioSistema` varchar(50) DEFAULT NULL,
  `status` enum('Activo','Inactivo') DEFAULT NULL,
  PRIMARY KEY (`idUsuario_Movil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_movil`
--

LOCK TABLES `usuario_movil` WRITE;
/*!40000 ALTER TABLE `usuario_movil` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario_movil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuariocompania`
--

DROP TABLE IF EXISTS `usuariocompania`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuariocompania` (
  `idUsuario` varchar(10) NOT NULL,
  `idCompania` int(11) NOT NULL,
  PRIMARY KEY (`idUsuario`,`idCompania`),
  KEY `FK_UsuarioCompania_idCompania` (`idCompania`),
  CONSTRAINT `FK_UsuarioCompania_idCompania` FOREIGN KEY (`idCompania`) REFERENCES `compania` (`idCompania`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuariocompania`
--

LOCK TABLES `usuariocompania` WRITE;
/*!40000 ALTER TABLE `usuariocompania` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuariocompania` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuariopasswords`
--

DROP TABLE IF EXISTS `usuariopasswords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuariopasswords` (
  `IdPassword` int(11) NOT NULL,
  `IdUsuario` varchar(10) DEFAULT NULL,
  `Contrasena` varchar(50) DEFAULT NULL,
  `Fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`IdPassword`),
  KEY `FK_usuariopasswords_usuarios` (`IdUsuario`),
  CONSTRAINT `FK_usuariopasswords_usuarios` FOREIGN KEY (`IdUsuario`) REFERENCES `usuarios` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuariopasswords`
--

LOCK TABLES `usuariopasswords` WRITE;
/*!40000 ALTER TABLE `usuariopasswords` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuariopasswords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `idUsuario` varchar(10) NOT NULL COMMENT 'Nombre de usuario',
  `contrasena` varchar(100) NOT NULL COMMENT 'Password del usuario',
  `iDTipoUsuario` varchar(10) NOT NULL COMMENT 'Tipo de usuario P=Proveedor, I=Interno',
  `correoElectronico` varchar(50) DEFAULT NULL COMMENT 'Correo personal del usuario',
  `activo` tinyint(1) DEFAULT NULL COMMENT 'Verdadero igual a activo',
  `fechaRegistro` datetime DEFAULT NULL COMMENT 'Fecha de alta del usuario',
  `privilegio` char(1) DEFAULT NULL COMMENT 'A=Admin, U=Usuario final, E=Usuario Especial',
  `nombreUsuario` varchar(60) DEFAULT NULL,
  `noEmpleado` varchar(50) NOT NULL DEFAULT '',
  `IngSesion` tinyint(4) DEFAULT '0',
  `Lectura` int(1) NOT NULL DEFAULT '0',
  `idUsuarioAutoriza` varchar(10) DEFAULT NULL,
  `CentroCostos` varchar(10) DEFAULT NULL,
  `Puesto` varchar(100) DEFAULT NULL,
  `Descripcion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idUsuario`,`noEmpleado`),
  KEY `tipoUsuarios_usuarios` (`iDTipoUsuario`),
  CONSTRAINT `tipoUsuarios_usuarios` FOREIGN KEY (`iDTipoUsuario`) REFERENCES `tipousuarios` (`idTipoUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla básica de usuarios del sistema B2B';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES ('01000014','control2019','1','prueba4@globalsoftm.com',1,'2019-01-01 00:00:00','U','Xiomara','01000014',1,1,'0','1409999999','Tester','Tester'),('1000028','vm2007123','1','prueba3@globalsoftm.com',1,'2019-01-01 00:00:00','U','Francisco Javier Rodríguez Villanueva','1000028',1,1,'10019170','OF11320000','Desarrollo','Desarrollo'),('1000029','vm2007123','1','prueba3@globalsoftm.com',1,'2019-01-01 00:00:00','U','Araceli Piedad Mundo Sanchez','1000029',1,1,'10019170','OF11320000','Desarrollo','Desarrollo'),('1000031','vm2007123','1','prueba4@globalsoftm.com',1,'2019-01-10 00:00:00','A','VANESSA CASTILLEJA DE LA VEGA','1000031',1,1,'10019170','OF11320000','COORDINADOR DE COMPRAS','COORDINACION DAMAS'),('1000032','vm2007123','1','prueba4@globalsoftm.com',1,'2019-01-19 00:00:00','E','CLAUDIA SUSANA MORENO SALAZAR','1000032',1,1,'10019170','OF11320000','GERENTE DE FISCAL','GERENCIA DE FISCAL'),('10015149','vm2007123','1','prueba3@globalsoftm.com',1,'2019-01-01 00:00:00','A','Aldo Edigar García de la Cerda','10015149',1,1,'10019170','1402000000','1','1'),('10019170','vm2007123','1','prueba2@globalsoftm.com',1,'2019-01-01 00:00:00','D','Héctor Soso León','10019170',1,1,'0','1402010100','Desarrollo','Desarrollo'),('10019171','vm2007123','1','prueba4@globalsoftm.com',1,'2019-01-01 00:00:00','D','HORACIO HERNANDEZ RAMOS','10019171',1,1,'0','1402010100','DEsarrollo','desarrollo'),('10034873','vm2007123','1','prueba2@globalsoftm.com',1,'2019-01-01 00:00:00','B','Mario Nava Rodríguez','10034873',1,1,'10019170','12345','1','1'),('10036280','vm2007123','1','soporte@globalsoftm.com',1,'2019-01-01 00:00:00','E','Mariana TelloSaldaña','10036280',1,1,'10019170','12345','1','1'),('10038070','vm2007123','1','prueba3@globalsoftm.com',1,'2019-01-01 00:00:00','U','David Eduardo	Pimentel	Yanez','10038070',1,1,'10019170','1402000100','1','1'),('10043872','vm2007123','1','prueba4@globalsoftm.com',1,'2019-01-01 00:00:00','U','Victor','10043872',1,1,'10019170','1405000010','test','test'),('1960012','vm2007123','1','prueba3@globalsoftm.com',1,'2019-01-01 00:00:00','U','Rodolfo López Flores','1960012',1,1,'10019170','1402010003','Desarrollo','Desarrollo'),('280966','vm2007123','1','amundo@globalsoftm.com',1,'2019-01-19 00:00:00','U','Jorge Alberto	Uribe	Castañeda','280966',1,1,'10019170','OF11320000','Diseño','Diseño'),('341974','vm2007123','1','prueba1@globalsoftm.com',1,'2019-01-01 00:00:00','U','Guillermo Figueroa Valdez','341974',1,1,'10019170','OF11320000','Administrativo','Administrativo'),('448761','vm2007123','1','prueba2@globalsoftm.com',1,'2019-01-01 00:00:00','U','América Guadalupe García García','448761',1,1,'10019170','1405000100','Analista','Analista'),('825117','vm2007123','1','prueba3@globalsoftm.com',1,'2019-01-01 00:00:00','U','James Erik Gurrola	Flores','825117',1,1,'10019170','1405000104','Analista','Analista'),('9013758','vm2007123','1','prueba2@globalsoftm.com',1,'2019-01-01 00:00:00','U','Ángel Ignacio Martín Schevenin','9013758',1,1,'10019170','1401020000','1','1'),('938548','vm2007123','1','prueba4@globalsoftm.com',1,'2019-01-01 00:00:00','U','Héctor Raul Ortegón Puga','938548',1,1,'10019170','1405000104','Autorizador','Autorizador');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuariosGrupoUsuarios`
--

DROP TABLE IF EXISTS `usuariosGrupoUsuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuariosGrupoUsuarios` (
  `idUsuario` varchar(10) NOT NULL COMMENT 'Identificador de usuario',
  `idGrupo` int(11) NOT NULL COMMENT 'Identificador del grupo de usuarios',
  PRIMARY KEY (`idUsuario`,`idGrupo`),
  KEY `grupoUsuarios_usuariosGrupoUsuarios` (`idGrupo`),
  CONSTRAINT `grupoUsuarios_usuariosGrupoUsuarios` FOREIGN KEY (`idGrupo`) REFERENCES `grupoUsuarios` (`idGrupo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla de relacion que indica los grupos de usuarios que tiene un usuario';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuariosGrupoUsuarios`
--

LOCK TABLES `usuariosGrupoUsuarios` WRITE;
/*!40000 ALTER TABLE `usuariosGrupoUsuarios` DISABLE KEYS */;
INSERT INTO `usuariosGrupoUsuarios` VALUES ('1',1);
/*!40000 ALTER TABLE `usuariosGrupoUsuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuariosModulos`
--

DROP TABLE IF EXISTS `usuariosModulos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuariosModulos` (
  `idUsuario` varchar(10) NOT NULL COMMENT 'Identificador del usuario',
  `idSistema` varchar(10) NOT NULL COMMENT 'Identificador del sistema',
  `idModulo` varchar(20) NOT NULL COMMENT 'Identificador del modulo',
  `idGrupo` int(11) NOT NULL DEFAULT '0',
  `activo` bit(1) DEFAULT b'1',
  PRIMARY KEY (`idUsuario`,`idSistema`,`idModulo`,`idGrupo`),
  KEY `modulos_usuariosModulos` (`idSistema`,`idModulo`),
  CONSTRAINT `modulos_usuariosModulos` FOREIGN KEY (`idSistema`, `idModulo`) REFERENCES `modulos` (`idSistema`, `idModulo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla para relacionar los usuarios y sus modulos a los que tiene permiso de acceso';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuariosModulos`
--

LOCK TABLES `usuariosModulos` WRITE;
/*!40000 ALTER TABLE `usuariosModulos` DISABLE KEYS */;
INSERT INTO `usuariosModulos` VALUES ('01000014','1','1',1,''),('01000014','1','2',1,''),('01000014','1','3',1,''),('1000028','1','1',1,''),('1000028','1','2',1,''),('1000028','1','3',1,''),('1000029','1','1',1,''),('1000029','1','2',1,''),('1000029','1','3',1,''),('1000031','1','1',1,''),('1000031','1','2',1,''),('1000031','1','3',1,''),('1000031','1','4',1,''),('1000032','1','1',1,''),('1000032','1','2',1,''),('1000032','1','3',1,''),('1000032','1','4',1,''),('1000032','1','5',1,''),('1000032','1','6',1,''),('100043872','1','1',1,''),('100043872','1','2',1,''),('100043872','1','3',1,''),('10015149','1','1',1,''),('10015149','1','3',1,''),('10015149','1','4',1,''),('10015149','1','6',1,''),('10019170','1','1',1,''),('10019170','1','2',1,''),('10019170','1','3',1,''),('10019170','1','5',1,''),('10019171','1','1',1,''),('10019171','1','2',1,''),('10019171','1','3',1,''),('10019171','1','5',1,''),('10034873','1','1',1,''),('10034873','1','2',1,''),('10034873','1','3',1,''),('10038070','1','1',1,''),('10038070','1','2',1,''),('10038070','1','3',1,''),('1960012','1','1',1,''),('1960012','1','2',1,''),('1960012','1','3',1,''),('280966','1','1',1,''),('280966','1','2',1,''),('280966','1','3',1,''),('341974','1','1',1,''),('341974','1','2',1,''),('341974','1','3',1,''),('448761','1','1',1,''),('448761','1','2',1,''),('448761','1','3',1,''),('825117','1','1',1,''),('825117','1','2',1,''),('825117','1','3',1,''),('9013758','1','1',1,''),('9013758','1','2',1,''),('9013758','1','3',1,''),('938548','1','1',1,''),('938548','1','2',1,''),('938548','1','3',1,'');
/*!40000 ALTER TABLE `usuariosModulos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuariosession`
--

DROP TABLE IF EXISTS `usuariosession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuariosession` (
  `NoEmpleado` varchar(10) NOT NULL DEFAULT '',
  `Idstatus` int(11) NOT NULL,
  `ultimaSession` datetime DEFAULT NULL COMMENT 'utilizado para los 90 dias inactivos',
  `sesionActiva` char(1) NOT NULL DEFAULT 'N' COMMENT 'utilizado para saber si esta activa la session',
  `FechaCambio` datetime DEFAULT NULL COMMENT 'utilizado para el cambio de contrasena',
  `FechaBloqueado` datetime DEFAULT NULL COMMENT 'utilizado para los bloqueos de usuarios',
  PRIMARY KEY (`NoEmpleado`),
  KEY `Status` (`Idstatus`),
  CONSTRAINT `Status` FOREIGN KEY (`Idstatus`) REFERENCES `status` (`IdStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuariosession`
--

LOCK TABLES `usuariosession` WRITE;
/*!40000 ALTER TABLE `usuariosession` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuariosession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuariotienda`
--

DROP TABLE IF EXISTS `usuariotienda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuariotienda` (
  `idusuario` varchar(10) DEFAULT NULL,
  `cntienda` varchar(3) DEFAULT NULL,
  `cncompania` varchar(10) DEFAULT NULL,
  `sociedad` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuariotienda`
--

LOCK TABLES `usuariotienda` WRITE;
/*!40000 ALTER TABLE `usuariotienda` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuariotienda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `variablesesion`
--

DROP TABLE IF EXISTS `variablesesion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `variablesesion` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador unico de registro.',
  `idSesion` varchar(36) DEFAULT NULL COMMENT 'Identificador unico de la sesion de un usuario',
  `variable` varchar(30) DEFAULT NULL COMMENT 'Nombre de la variable de sesion',
  `valor` varchar(250) DEFAULT NULL COMMENT 'Valor de la variable de sesion',
  PRIMARY KEY (`id`),
  KEY `sesiones_variableSesion` (`idSesion`),
  CONSTRAINT `sesiones_variableSesion` FOREIGN KEY (`idSesion`) REFERENCES `sesiones` (`idSesion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabla para almacenar las variables de sesion durante la interaccion de un usuario con los diferentes sistemas B2B.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `variablesesion`
--

LOCK TABLES `variablesesion` WRITE;
/*!40000 ALTER TABLE `variablesesion` DISABLE KEYS */;
/*!40000 ALTER TABLE `variablesesion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'sfseguridad'
--
/*!50003 DROP PROCEDURE IF EXISTS `InsSuperUsuarios` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `InsSuperUsuarios`(IN pUsuario VARCHAR(10), pContrasena VARCHAR(10), pNombreUsuario VARCHAR(50))
BEGIN 	
	INSERT INTO SuperUsuarios(Usuario, Contrasena, NombreUsuario) VALUES (pUsuario, pContrasena, pNombreUsuario); 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `InsSuperUsuarios0` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `InsSuperUsuarios0`(IN pUsuario VARCHAR(10), IN pContrasena VARCHAR(10), IN pNombreUsuario VARCHAR(50))
BEGIN 	
	INSERT INTO SuperUsuarios(Usuario, Contrasena, NombreUsuario) VALUES (pUsuario, pContrasena, pNombreUsuario);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `InsSuperUsuarios2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `InsSuperUsuarios2`(IN pUsuario VARCHAR(10), pContrasena VARCHAR(10), pNombreUsuario VARCHAR(50))
BEGIN 	

	DECLARE	vResponse		VARCHAR(100);

	set vResponse = '';

	proc_label:BEGIN
	IF EXISTS (SELECT 1 FROM b2bSeguridad.SuperUsuarios WHERE usuario = pUsuario )
    then
				SET vResponse = 'Ya existe un Super Usuario con este nombre ';
				select vResponse;
				LEAVE proc_label;
	END IF;


	INSERT INTO SuperUsuarios(Usuario, Contrasena, NombreUsuario) VALUES (pUsuario, pContrasena, pNombreUsuario);
    select vResponse;
	END proc_label;
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `InsSuperUsuarios3` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `InsSuperUsuarios3`(IN pUsuario VARCHAR(10), IN pContrasena VARCHAR(10), IN pNombreUsuario VARCHAR(50))
BEGIN 	

	DECLARE	vResponse	VARCHAR(100);

	set vResponse = '';

	proc_label:BEGIN
	IF EXISTS (SELECT 1 FROM b2bSeguridad.SuperUsuarios WHERE usuario = pUsuario )
    then
				SET vResponse = 'Ya existe un Super Usuario con este nombre ';
				select vResponse;
				LEAVE proc_label;
	END IF;


	INSERT INTO SuperUsuarios(Usuario, Contrasena, NombreUsuario) VALUES (pUsuario, pContrasena, pNombreUsuario);
    select vResponse;
	END proc_label;
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `InsSuperUsuarios4` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `InsSuperUsuarios4`(IN pUsuario VARCHAR(10), IN pContrasena VARCHAR(10), IN pNombreUsuario VARCHAR(50))
BEGIN 	

	DECLARE	vResponse	VARCHAR(100);

	set vResponse = '';

	proc_label:BEGIN
	IF EXISTS (SELECT 1 FROM b2bSeguridad.SuperUsuarios WHERE usuario = pUsuario )
    then
				SET vResponse = 'Ya existe un Super Usuario con este nombre ';
				select vResponse;
				LEAVE proc_label;
	END IF;


	INSERT INTO SuperUsuarios(Usuario, Contrasena, NombreUsuario) VALUES (pUsuario, pContrasena, pNombreUsuario);
    select vResponse;
	END proc_label;
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `InsSuperUsuarios5` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `InsSuperUsuarios5`(IN pUsuario VARCHAR(10), IN pContrasena VARCHAR(10), IN pNombreUsuario VARCHAR(50))
BEGIN 	

	DECLARE	vResponse	VARCHAR(100);

	set vResponse = '';

	proc_label:BEGIN
	IF EXISTS (SELECT usuario FROM b2bSeguridad.SuperUsuarios WHERE usuario = pUsuario )
    then
				SET vResponse = 'Ya existe un Super Usuario con este nombre ';
				select vResponse;
				LEAVE proc_label;
	END IF;


	INSERT INTO SuperUsuarios(Usuario, Contrasena, NombreUsuario) VALUES (pUsuario, pContrasena, pNombreUsuario);
    select vResponse;
	END proc_label;
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `InsSuperUsuarios6` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `InsSuperUsuarios6`(in pUsuario VARCHAR(10), IN pContrasena VARCHAR(10), IN pNombreUsuario VARCHAR(50))
BEGIN 	

	INSERT INTO SuperUsuarios(Usuario, Contrasena, NombreUsuario) VALUES (pUsuario, pContrasena, pNombreUsuario);
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `jlghEliminarModulo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `jlghEliminarModulo`(
	in pIdSistema varchar(10),
	in pIdModulo varchar(20)
)
begin
delete from grupousuariosmodulos where idSistema =pIdSistema and idModulo = pIdModulo;
delete from usuariosModulos where idSistema = pIdSistema and idModulo=pIdModulo;
delete from modulos where idSistema = pIdSistema and idModulo = pIdModulo;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procCambiosPassword` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procCambiosPassword`(IN `PIdUsuario` VARCHAR(50), IN `PContrasena` VARCHAR(50))
BEGIN
			Declare PID int;
			Declare PCant int;
			Declare PPass int;
			
			If not exists(select * from usuariopasswords where idusuario=PIdUsuario) then
					
					insert into usuariopasswords (IdPassword,IdUsuario,Contrasena,fecha)
					values(1,PIdUsuario,PContrasena,now());
					Select 'S';
			else
					if not exists(select * from usuariopasswords where idUsuario=PIdUsuario and Contrasena=PContrasena) then
							set PID=(select max(Idpassword)+1 from usuariopasswords ); 
							set PCant=(select count(*) from usuariopasswords where  idusuario=PIdUsuario); 
					
							if PCant>=4 then 
											
											set PPass=(select IdPassword from usuariopasswords where idusuario=PIdUsuario order by fecha asc limit 1);
											delete from usuariopasswords 
											where IdPassword=PPass;
							end if;					
					
							insert into usuariopasswords (IdPassword,IdUsuario,Contrasena,fecha)
							values(PID,PIdUsuario,PContrasena,now());			

							Select 'S';
							
					else
							Select 'N';
					End If;			
				
			end if;
						
		

		
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proccomentariosbajas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `proccomentariosbajas`(IN `PIdComentario` vARCHAR(100), IN `PIdUsuarioBaja` VARCHAR(50), IN `PIdUsuarioAdm` VARCHAR(50), IN `PFecha` VARCHAR(50), IN `PComentario` VARCHAR(50), IN `PAccion` VARCHAR(50))
BEGIN
		insert into comentariosbajas(IdUsuarioBaja,IdUsuarioAdm,Fecha,Comentario) values(PIdUsuarioBaja,PIdUsuarioAdm,now(),PComentario);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procConsultaPagoPendienteDatos_Copia141118` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procConsultaPagoPendienteDatos_Copia141118`(IN `pclave` INT, IN `pIdProveedor` varchar(20), IN `pIdUsuario` VARCHAR(10), IN `pPerfil` varCHAR(10))
Begin
DECLARE pSuc varchar(80);
DECLARE pConcepto varchar(80);
DECLARE pDescripcion varchar(100);
DECLARE pFolio       varchar(80);
DECLARE pImporte  varchar(80);
DECLARE pSaldo      varchar(80);
DECLARE pVencimiento      varchar(15);
DECLARE pXML       varchar(100);
DECLARE pStatus     varchar(100);
DECLARE pTPrecio   Decimal(18,2);
DECLARE pTCantidad Decimal(18,2);
DECLARE pFolioFac varchar(20);
DECLARE pIdCorreccion INT;
DECLARE pExiste int;
DECLARE pEncontroNegativo char(2); 
DECLARE pTipo varchar(15);
DECLARE pBloqueo char(1);
DECLARE pExisteSucursal int;
DECLARE pExisteSerie int;
DECLARE pIva varchar(80);
DECLARE v_done INT DEFAULT FALSE;
DECLARE pCursorPedido CURSOR FOR
         SELECT *, case when (CONCEPTO = 'FACTURA BRUTO' and IMPORTE > 0 and BLOQUEO = 'B') then 'Subir XML' end as 'XML', 
					case when (CONCEPTO = 'FACTURA BRUTO' and IMPORTE > 0 and BLOQUEO = 'B') then 'Pago detenido por XML pendiente' end as 'STATUS',
         SUBSTRING(FOLIO,5) as folioFac
         from (SELECT          dp.numeroSucursal SUC,     
         CASE WHEN ISNULL(dp.claveMovimientoI) then
                   Case when ISNULL(c.decripcion) then
                            LTRIM(IFNULL(dp.ClaveMovimiento,'&nbsp;'))
                   End
         Else
                   IFNULL(dp.ClaveMovimientoI, '&nbsp;')
         End CONCEPTO,
         CASE WHEN NOT ISNULL(dp.textoSAP) THEN dp.textoSAP ELSE '' END DESCRIPCION,
         dp.factura FOLIO,dp.BloqueoDePago BLOQUEO, dp.importeFolio as IMPORTE,
         dp.saldoFolio as SALDO,
         DATE_FORMAT(dp.fechaVencimiento, '%d/%m/%y') VENCIMIENTO
                            FROM desglosepagospendientes dp LEFT OUTER JOIN
                   clavemovimientos c on dp.ClaveMovimiento=c.idClaveMovimiento
         WHERE idPagoPendiente = pclave) as tbl
         where concepto like '%Nota Cargo%'; 

Declare pCursorPedidos cursor for 
	select idCorreccion from tmpIdCorrecciones;		
         
DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = true;



SELECT P.idPagoPendiente, 
Case when isnull(s.SaldoPagar) then '&nbsp;'
else concat('$ ',format(replace(S.SaldoPagar,',',''),2)) end saldo, P.idBitacora idBit,
CASE WHEN ISNULL(Sociedad) THEN '***' ELSE Sociedad END Sociedad, 
pr.Nombre, pr.calle Dir, pr.localidad CD, pr.estado, 
pr.CP, pr.telefono, pr.fax, pr.RFC
         FROM pagospendientes P LEFT OUTER JOIN
                   saldopendientes S ON P.idPagoPendiente = S.idPagoPendiente
                   join bitacoras b on p.idBitacora=b.idBitacora
                   join proveedores pr on p.idProveedor=pr.idProveedor
         WHERE         P.idPagoPendiente = pclave;


DROP temporary table if exists tmpPago;
DROP temporary table if exists tmpPagoReplica;
DROP temporary table if exists tmpPagoSinCorrecciones;
create temporary table tmpPago(
         Suc     varchar(80),
         Concepto      varchar(80),
         Descripcion varchar(100),
         Folio   varchar(80),
         Bloqueo char(1),
         Importe        varchar(80),
         Saldo  varchar(80),
         Vencimiento   varchar(15),
         XML    varchar(100),
         Status varchar(100),
         serie varchar(20),
         tipo int,
         orden int
);
create temporary table tmpPagoReplica like tmpPago; 
create temporary table tmpPagoSinCorrecciones like tmpPago; 


DROP temporary table if exists tmpSucursalSerie;
create temporary table tmpSucursalSerie(
			IdPagoPendiente int,
         IdSuc   varchar(4),
         IdSerie      varchar(10)
);

IF (pPerfil = 'E' or pPerfil = 'U') THEN
	Select COUNT(*) into pExiste From b2bbas.proveedormedios where idproveedor = pIdProveedor;
		IF (pExiste > 0) THEN
			Select Tipo into pTipo From b2bbas.tipoproveedormedios where idproveedor = pIdProveedor;
				IF (pTipo = 'Generico') THEN	
						Select Count(*) into pExisteSucursal From b2bbas.desglosepagospendientes as DPP 
						Inner Join b2bbas.pagospendientes as PP on PP.idPagoPendiente = DPP.idPagoPendiente
						Inner Join b2bbas.proveedormedios as PM on PM.idProveedor = PP.idProveedor 
						Inner Join b2bbas.mediosplaza as MP on MP.idProveedorMedios = PM.idProveedorMedios 
						Inner Join b2bbas.plazasucursal as PS on PS.idPlaza = MP.idPlaza 
						Where PP.idProveedor = pIdProveedor
						And DPP.numeroSucursal = PS.idSucursal;
						
						SELECT COUNT(DISTINCT PM.serie) into pExisteSerie
						FROM b2bbas.desglosepagospendientes as DPP
						Inner Join b2bbas.pagospendientes as PP on PP.idPagoPendiente = DPP.idPagoPendiente
						Inner Join b2bbas.proveedormedios as PM on PM.idProveedor = PP.idProveedor
						Where PP.idProveedor = pIdProveedor
						and PM.serie like concat ('%', Substring(DPP.factura,5,length(PM.serie)),'%');
				END IF;	
		END IF;
		
		IF (pExisteSerie > 0) THEN 
						Insert into tmpSucursalSerie(idPagoPendiente,idSerie)
		 				SELECT DISTINCT DPP.idPagoPendiente,PM.serie 
						FROM b2bbas.desglosepagospendientes as DPP
						Inner Join b2bbas.pagospendientes as PP on PP.idPagoPendiente = DPP.idPagoPendiente 
						Inner Join b2bbas.proveedormedios as PM on PM.idProveedor = PP.idProveedor and PM.idUsuario = pIdUsuario
						Where PP.idProveedor = pIdProveedor
						and PM.serie like concat ('%', Substring(DPP.factura,5,length(PM.serie)),'%');
						
						insert into tmpPago
						SELECT Distinct *, case when (CONCEPTO = 'FACTURA BRUTO' and IMPORTE > 0 and BLOQUEO = 'B') then 'Subir XML' end as "XML", 
						case when (CONCEPTO = 'FACTURA BRUTO' and IMPORTE > 0 and BLOQUEO = 'B') then 'Pago detenido por XML pendiente' end as "STATUS",
						case when Folio is null or Folio = '' or Locate('-',Folio) = 0 then '0' else SUBSTRING(FOLIO,5) end as 'serie',
						case when CONCEPTO = 'FACTURA BRUTO' THEN 1 Else 0 end as 'tipo',
						case when CONCEPTO = 'FACTURA BRUTO' Or CONCEPTO = 'Cargo Logístico' Or CONCEPTO = 'Ingreso Volumétrica' THEN 1 Else 0 end as 'orden'
						from (SELECT          dp.numeroSucursal SUC,     
						CASE WHEN ISNULL(dp.claveMovimientoI) then
						         Case when ISNULL(c.decripcion) then
						                   LTRIM(IFNULL(dp.ClaveMovimiento,'&nbsp;'))
						         End
							Else
						         IFNULL(dp.ClaveMovimientoI, '&nbsp;')
						End CONCEPTO,
						CASE WHEN NOT ISNULL(dp.textoSAP) THEN dp.textoSAP ELSE '' END DESCRIPCION,
						dp.factura FOLIO,dp.BloqueoDePago BLOQUEO, dp.importeFolio as IMPORTE,
						dp.saldoFolio as SALDO,
						DATE_FORMAT(dp.fechaVencimiento, '%d/%m/%y') VENCIMIENTO
						                   FROM desglosepagospendientes dp LEFT OUTER JOIN
						         clavemovimientos c on dp.ClaveMovimiento=c.idClaveMovimiento
						         Inner Join tmpSucursalSerie as TSS on TSS.idPagoPendiente = dp.idPagoPendiente
						WHERE dp.idPagoPendiente = pclave 
							   and TSS.idSerie like concat ('%', Substring(DP.factura,5,length(TSS.idSerie)),'%')) as tbl
						where concepto not like '%Nota Cargo%';
										
		ELSEIF (pExisteSucursal > 0) THEN
						Insert into tmpSucursalSerie(idPagoPendiente,idSuc)
						Select DPP.idPagoPendiente, PS.idSucursal 
						From b2bbas.desglosepagospendientes as DPP 
						Inner Join b2bbas.pagospendientes as PP on PP.idPagoPendiente = DPP.idPagoPendiente
						Inner Join b2bbas.proveedormedios as PM on PM.idProveedor = PP.idProveedor
						Inner Join b2bbas.mediosplaza as MP on MP.idProveedorMedios = PM.idProveedorMedios 
						Inner Join b2bbas.plazasucursal as PS on PS.idPlaza = MP.idPlaza 
						Where PP.idProveedor = pIdProveedor
						And DPP.numeroSucursal = PS.idSucursal;
						
						insert into tmpPago
						SELECT Distinct *, case when (CONCEPTO = 'FACTURA BRUTO' and IMPORTE > 0 and BLOQUEO = 'B') then 'Subir XML' end as "XML", 
						case when (CONCEPTO = 'FACTURA BRUTO' and IMPORTE > 0 and BLOQUEO = 'B') then 'Pago detenido por XML pendiente' end as "STATUS",
						case when Folio is null or Folio = '' or Locate('-',Folio) = 0 then '0' else SUBSTRING(FOLIO,5) end as 'serie',
						case when CONCEPTO = 'FACTURA BRUTO' THEN 1 Else 0 end as 'tipo',
						case when CONCEPTO = 'FACTURA BRUTO' Or CONCEPTO = 'Cargo Logístico' Or CONCEPTO = 'Ingreso Volumétrica' THEN 1 Else 0 end as 'orden'
						from (SELECT          dp.numeroSucursal SUC,     
						CASE WHEN ISNULL(dp.claveMovimientoI) then
						         Case when ISNULL(c.decripcion) then
						                   LTRIM(IFNULL(dp.ClaveMovimiento,'&nbsp;'))
						         End
						Else
						         IFNULL(dp.ClaveMovimientoI, '&nbsp;')
						End CONCEPTO,
						CASE WHEN NOT ISNULL(dp.textoSAP) THEN dp.textoSAP ELSE '' END DESCRIPCION,
						dp.factura FOLIO,dp.BloqueoDePago BLOQUEO, dp.importeFolio as IMPORTE,
						dp.saldoFolio as SALDO,
						DATE_FORMAT(dp.fechaVencimiento, '%d/%m/%y') VENCIMIENTO
						                   FROM desglosepagospendientes dp LEFT OUTER JOIN
						         clavemovimientos c on dp.ClaveMovimiento=c.idClaveMovimiento
						         Inner Join tmpSucursalSerie as TSS on TSS.idPagoPendiente = dp.idPagoPendiente
						WHERE dp.idPagoPendiente = pclave 
							   and TSS.idSuc = dp.numeroSucursal) as tbl
						where concepto not like '%Nota Cargo%';
						
		ELSE 
						insert into tmpPago
						SELECT *, case when (CONCEPTO = 'FACTURA BRUTO' and IMPORTE > 0 and BLOQUEO = 'B') then 'Subir XML' end as "XML", 
						case when (CONCEPTO = 'FACTURA BRUTO' and IMPORTE > 0 and BLOQUEO = 'B') then 'Pago detenido por XML pendiente' end as "STATUS",
				case when Folio is null or Folio = '' or Locate('-',Folio) = 0 then '0' else SUBSTRING(FOLIO,5) end as 'serie',
				case when CONCEPTO = 'FACTURA BRUTO' THEN 1 Else 0 end as 'tipo',
				case when CONCEPTO = 'FACTURA BRUTO' Or CONCEPTO = 'Cargo Logístico' Or CONCEPTO = 'Ingreso Volumétrica' THEN 1 Else 0 end as 'orden'
				from (SELECT          dp.numeroSucursal SUC,     
				CASE WHEN ISNULL(dp.claveMovimientoI) then
			         Case when ISNULL(c.decripcion) then
		                   LTRIM(IFNULL(dp.ClaveMovimiento,'&nbsp;'))
		      	   End
				Else
		         IFNULL(dp.ClaveMovimientoI, '&nbsp;')
				End CONCEPTO,
				CASE WHEN NOT ISNULL(dp.textoSAP) THEN dp.textoSAP ELSE '' END DESCRIPCION,
				dp.factura FOLIO,dp.BloqueoDePago BLOQUEO, dp.importeFolio as IMPORTE,
				dp.saldoFolio as SALDO,
				DATE_FORMAT(dp.fechaVencimiento, '%d/%m/%y') VENCIMIENTO
				                   FROM desglosepagospendientes dp LEFT OUTER JOIN
		      		   clavemovimientos c on dp.ClaveMovimiento=c.idClaveMovimiento
				WHERE idPagoPendiente = pclave) as tbl
				where concepto not like '%Nota Cargo%';
		END IF;	
	
ELSE 
								insert into tmpPago
						SELECT *, case when (CONCEPTO = 'FACTURA BRUTO' and IMPORTE > 0 and BLOQUEO = 'B') then 'Subir XML' end as "XML", 
						case when (CONCEPTO = 'FACTURA BRUTO' and IMPORTE > 0 and BLOQUEO = 'B') then 'Pago detenido por XML pendiente' end as "STATUS",
				case when Folio is null or Folio = '' or Locate('-',Folio) = 0 then '0' else SUBSTRING(FOLIO,5) end as 'serie',
				case when CONCEPTO = 'FACTURA BRUTO' THEN 1 Else 0 end as 'tipo',
				case when CONCEPTO = 'FACTURA BRUTO' Or CONCEPTO = 'Cargo Logístico' Or CONCEPTO = 'Ingreso Volumétrica' THEN 1 Else 0 end as 'orden'
				from (SELECT          dp.numeroSucursal SUC,     
				CASE WHEN ISNULL(dp.claveMovimientoI) then
			         Case when ISNULL(c.decripcion) then
		                   LTRIM(IFNULL(dp.ClaveMovimiento,'&nbsp;'))
		      	   End
				Else
		         IFNULL(dp.ClaveMovimientoI, '&nbsp;')
				End CONCEPTO,
				CASE WHEN NOT ISNULL(dp.textoSAP) THEN dp.textoSAP ELSE '' END DESCRIPCION,
				dp.factura FOLIO,dp.BloqueoDePago BLOQUEO, dp.importeFolio as IMPORTE,
				dp.saldoFolio as SALDO,
				DATE_FORMAT(dp.fechaVencimiento, '%d/%m/%y') VENCIMIENTO
				                   FROM desglosepagospendientes dp LEFT OUTER JOIN
		      		   clavemovimientos c on dp.ClaveMovimiento=c.idClaveMovimiento
				WHERE idPagoPendiente = pclave) as tbl
				where concepto not like '%Nota Cargo%';
END IF;



DROP temporary table if exists tmpFolioProcesados;
create temporary table tmpFolioProcesados(
	Folio varchar(80),
    Descripcion varchar(10),
    idCorreccion int
);


DROP temporary table if exists tmpIdCorrecciones;
create temporary table tmpIdCorrecciones(
	IdCorreccion int
);


set pEncontroNegativo = 'NO';
OPEN pCursorPedido;
         LOOP1: LOOP
                   FETCH NEXT FROM pCursorPedido INTO pSuc,pConcepto,pDescripcion,pFolio,pBloqueo,pImporte,pSaldo,pVencimiento,pXML,pStatus,pFolioFac;
                   IF v_done THEN
                     SET v_done := false;
                     CLOSE pCursorPedido;
                     LEAVE LOOP1;
                   END IF;

                   SET pIdCorreccion = 0;
                   SET pTCantidad = 0;
                   SET pTPrecio = 0;
                   
                                                                         
				   IF EXISTS(SELECT IdCorreccion FROM correcciones WHERE serieFactura LIKE CONCAT('%',pFolioFac,'%') AND idproveedor= pIdProveedor) THEN
							insert into tmpIdCorrecciones
							SELECT IdCorreccion FROM b2bbas.correcciones 
							WHERE serieFactura LIKE CONCAT('%',pFolioFac,'%') 
							AND idproveedor= pIdProveedor 
							And folioCarta  in (Select distinct Substring(CveReferencia,1,10) from desglosepagos where factura like CONCAT('%',pFolioFac,'%') and idpago=pclave)
							order by idcorreccion desc; 
							
							
                            
                            OPEN pCursorPedidos;
													    LOOP2:LOOP
														  Fetch Next From pCursorPedidos Into pIdCorreccion; 
																	  IF v_done THEN
																	  SET v_done := false;
																	  CLOSE pCursorPedidos;
																	  LEAVE LOOP2;
																	 END IF;
                                                       
                            
							select iva from correcciontotales where idcorreccion = pIdCorreccion into pIva;
							
							
							if pIva =  0 then 
									SELECT (IFNULL(SUM((REPLACE(REPLACE(de,',',''),'$','')-REPLACE(REPLACE(para,',',''),'$',''))*REPLACE(CantidadPrecioCambio,',','')),0)*0)
									FROM desglosecorrecciones 
									WHERE idcorreccion = pIdCorreccion 
									AND cantidadpreciodescripcion = 'Precio'
									INTO pTPrecio;
							else
									 SELECT (IFNULL(SUM((REPLACE(REPLACE(de,',',''),'$','')-REPLACE(REPLACE(para,',',''),'$',''))*REPLACE(CantidadPrecioCambio,',','')),0)*1.16)
									FROM desglosecorrecciones 
									WHERE idcorreccion = pIdCorreccion  
									AND cantidadpreciodescripcion = 'Precio'
									INTO pTPrecio;
							end if; 
						  
                       
					   IF NOT EXISTS(SELECT Folio FROM  tmpFolioProcesados WHERE Folio = pFolioFac and Descripcion ='Precio' and idcorreccion =  pIdCorreccion) then 

							If pTPrecio > 0 THEN
									  insert into tmpPagoReplica  
									  values(pSuc,pConcepto,'Diferencia en precio',pFolio,pBloqueo,format(replace((-1)*pTPrecio,',',''),2),
														pSaldo,pVencimiento,pXML,pStatus,pFolioFac,0,1);
														insert into tmpFolioProcesados values(pFolioFac,'Precio', idCorreccion); 
							END IF;
							
								if pTPrecio < 0 then 
									SET pEncontroNegativo = 'SI'; 
								end if; 
						 END IF;
						
							if pIva =  0 then 
								SELECT (IFNULL(SUM((REPLACE(REPLACE(de,',',''),'$','')-REPLACE(REPLACE(para,',',''),'$',''))*REPLACE(CantidadPrecioCambio,',','')),0)*0)
								FROM desglosecorrecciones 
								WHERE idcorreccion = pIdCorreccion  
								AND cantidadpreciodescripcion = 'Cantidad'
								INTO pTCantidad;
							else
								SELECT (IFNULL(SUM((REPLACE(REPLACE(de,',',''),'$','')-REPLACE(REPLACE(para,',',''),'$',''))*REPLACE(CantidadPrecioCambio,',','')),0)*1.16)
								FROM desglosecorrecciones 
								WHERE idcorreccion = pIdCorreccion  
								AND cantidadpreciodescripcion = 'Cantidad'
								INTO pTCantidad;
							end if; 
                            
					IF NOT EXISTS(SELECT Folio FROM  tmpFolioProcesados WHERE Folio = pFolioFac and Descripcion ='Cantidad' and idcorreccion =  pIdCorreccion ) then 
							If pTCantidad > 0 THEN
									  insert into tmpPagoReplica  
									  values(pSuc,pConcepto,'Descuento por Faltante de Mercancia',pFolio,pBloqueo,format(replace((-1)*pTCantidad,',',''),2),
														pSaldo,pVencimiento,pXML,pStatus,pFolioFac,0,1);
										insert into tmpFolioProcesados values(pFolioFac,'Cantidad', idCorreccion); 
							END IF;
							if pTCantidad < 0 then 
							 SET pEncontroNegativo = 'SI'; 
							end if; 
					 END IF;
							
							IF pTCantidad = 0 AND pTPrecio = 0 THEN
									  insert into tmpPagoReplica  
							values(pSuc,pConcepto,pDescripcion,pFolio,pBloqueo,pImporte,pSaldo,pVencimiento,pXML,pStatus,pFolioFac,0,1);
							END IF;
							END LOOP LOOP2;
								
				   ELSE

							insert into tmpPagoReplica  
							values(pSuc,pConcepto,pDescripcion,pFolio,pBloqueo,pImporte,pSaldo,pVencimiento,pXML,pStatus,pFolioFac,0,1);
				   END IF;
							insert into tmpPagoSinCorrecciones   
							values(pSuc,pConcepto,pDescripcion,pFolio,pBloqueo,pImporte,pSaldo,pVencimiento,pXML,pStatus,pFolioFac,0,1);
							
         END LOOP LOOP1;
          if pEncontroNegativo = 'SI' then 
            insert into tmpPago
		    select * from tmpPagoSinCorrecciones; 
		 elseif pEncontroNegativo ='NO' then 
            insert into tmpPago 
			select  * from tmpPagoReplica;
	    end if;
SET @Saldo := 0, @Tipo := 0,@Orden := 0, @Serie := '', @Importe := 0, @Saldo2 := 0, @Importe2 := 0;

SELECT SUC,CONCEPTO,DESCRIPCION,FOLIO,format(replace(IMPORTE,',',''),2) as 'IMPORTE',format(replace(SALDO,',',''),2) AS 'SALDO',
                   VENCIMIENTO,XML,STATUS
FROM (
         SELECT
                   y.SUC,y.CONCEPTO,y.DESCRIPCION,y.FOLIO,
                   @Saldo2 := IF(@Serie = y.serie,@Saldo2+y.Importe,y.importe) as Saldo2,
                   y.IMPORTE,
                   y.VENCIMIENTO,y.XML,y.STATUS,
                   @Saldo := case when y.Orden = 1 THEN IF(@Serie = y.serie,@Saldo2,y.Importe) 
                                               ELSE y.Importe end as Saldo,
                   y.orden,
                   @Serie := y.serie as serie,y.tipo
         FROM (
                   select SUC,CONCEPTO,DESCRIPCION,FOLIO,IMPORTE,SALDO,
                            VENCIMIENTO,XML,STATUS,orden,serie,tipo 
                   from tmpPago order by orden desc,serie desc,tipo desc
                   ) y
         )z ;

select SUC,CONCEPTO,DESCRIPCION,FOLIO,IMPORTE,SALDO,
                            VENCIMIENTO,XML,STATUS,orden,serie,tipo 
                   from tmpPago order by orden desc,serie desc,tipo desc ;
                   
                  

End ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procConsultaPagosDatos_Copia141118` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procConsultaPagosDatos_Copia141118`(
in pclave INT,
in pIdProveedor varchar(20))
Begin

DECLARE pSuc	varchar(80);
DECLARE pConcepto	varchar(80);
DECLARE pDescripcion varchar(100);
DECLARE pFolio	varchar(80);
DECLARE pImporte	varchar(80);
DECLARE pSaldo	varchar(80);
DECLARE pTPrecio	Decimal(18,2);
DECLARE pTCantidad Decimal(18,2);
DECLARE pFolioFac varchar(20);
DECLARE pIdCorreccion INT;
DECLARE pIva varchar(80);
DECLARE pEncontroNegativo char(2);
DECLARE v_done INT DEFAULT FALSE;
DECLARE pCursorPedido CURSOR FOR 
	SELECT *,SUBSTRING(FOLIO,5) as folioFac FROM tempPago
	where CONCEPTO like '%Nota Cargo%'; 
Declare pCursorPedidos cursor for 
	select idCorreccion from tmpIdCorrecciones;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = true;


SELECT 
    DATE_FORMAT(fechaPago, '%d/%m/%y') Fecha,
    folioPago Folio,
    IFNULL(cuentaBancaria, '') Cuenta,
    CONCAT('$ ',
            FORMAT(REPLACE(importePago, ',', ''), 2)) Importe,
    b.idBitacora idBit,
    CASE
        WHEN ISNULL(Sociedad) THEN '***'
        ELSE Sociedad
    END Sociedad,
    p.Nombre,
    p.calle Dir,
    p.localidad CD,
    p.estado,
    p.CP,
    p.telefono,
    p.fax,
    p.RFC
FROM
    pagos
        JOIN
    bitacoras b ON pagos.idBitacora = b.idBitacora
        JOIN
    proveedores p ON pagos.idProveedor = p.idProveedor
WHERE
    idPago = pclave;



drop temporary table if exists tempPago;
CREATE TEMPORARY TABLE tempPago(
SUC	VARCHAR(80),
CONCEPTO	VARCHAR(30),
DESCRIPCION VARCHAR(80),
FOLIO	VARCHAR(80),
IMPORTE	VARCHAR(80),
SALDO	VARCHAR(80)
);

INSERT INTO tempPago	
SELECT 	dp.numeroSucursal SUC,
CASE WHEN NOT ISNULL(dp.ClaveMovimiento) then
	CASE WHEN ISNULL(c.decripcion) THEN dp.ClaveMovimiento
	ELSE IFNULL(SUBSTRING(c.decripcion,1,30), '&nbsp;') END 
ELSE dp.ClaveMovimiento END CONCEPTO, 
CASE WHEN NOT ISNULL(dp.textoSAP) THEN dp.textoSAP ELSE '' END DESCRIPCION,
dp.factura Folio, replace(dp.importeFolio,',','') as IMPORTE,
replace(dp.saldoFolio,',','') as SALDO
FROM desglosepagos dp 
LEFT OUTER JOIN clavemovimientos c ON dp.ClaveMovimiento = c.idClaveMovimiento
WHERE idPago = pclave AND dp.ClaveMovimiento REGEXP '^[0-9]+$' = 1;

INSERT INTO tempPago	
SELECT	dp.numeroSucursal SUC,	
IFNULL(SUBSTRING(dp.ClaveMovimiento,1,30), '&nbsp;') CONCEPTO,
CASE WHEN NOT ISNULL(dp.textoSAP) THEN dp.textoSAP ELSE '' END DESCRIPCION,
dp.factura Folio, replace(dp.importeFolio,',','') as IMPORTE,
replace(dp.saldoFolio,',','') as SALDO
FROM desglosepagos dp 
WHERE idPago = pclave AND dp.ClaveMovimiento REGEXP '^[0-9]+$' = 0;

DROP temporary table if exists tmpPagoDetalle;
DROP temporary table if exists tmpPagoDetalleReplica;
DROP temporary table if exists tmpPagoDetalleSinCorrecciones;
create temporary table tmpPagoDetalle(
	Suc	varchar(80),
	Concepto	varchar(80),
	Descripcion varchar(100),
	Folio	varchar(80),
	Importe	varchar(80),
	Saldo	varchar(80),
	serie varchar(20),
	tipo int,
	orden int
);
create temporary table tmpPagoDetalleReplica like tmpPagoDetalle; 
create temporary table tmpPagoDetalleSinCorrecciones like tmpPagoDetalle; 

insert into tmpPagoDetalle
SELECT *,
	case when Folio is null or Folio = '' or Locate('-',Folio) = 0 then '0' else SUBSTRING(FOLIO,5) end as 'serie',
	case when CONCEPTO = 'FACTURA BRUTO' THEN 1 Else 0 end as 'tipo',
	case when CONCEPTO = 'FACTURA BRUTO' Or CONCEPTO = 'Cargo Logístico' Or CONCEPTO = 'Ingreso Volumétrica' THEN 1 Else 0 end as 'orden'
FROM tempPago
WHERE concepto not like '%Nota Cargo%';


DROP temporary table if exists tmpFolioProcesados;
create temporary table tmpFolioProcesados(
	Folio varchar(80),
    Descripcion varchar(10),
    idCorreccion int
);

DROP temporary table if exists tmpIdCorrecciones;
create temporary table tmpIdCorrecciones(
	IdCorreccion int
);

    
SET pEncontroNegativo = 'NO';  
OPEN pCursorPedido;
	LOOP1: LOOP
		FETCH NEXT FROM pCursorPedido INTO pSuc,pConcepto,pDescripcion,pFolio,pImporte,pSaldo,pFolioFac;
		IF v_done THEN
		  SET v_done := false;
		  CLOSE pCursorPedido;
		  LEAVE LOOP1;
		END IF;

		SET pIdCorreccion = 0;
		SET pTCantidad = 0;
		SET pTPrecio = 0;
         	 
					IF EXISTS(SELECT IdCorreccion FROM correcciones WHERE serieFactura LIKE CONCAT('%',pFolioFac,'%') AND idproveedor= pIdProveedor) THEN
						delete from tmpIdCorrecciones; 
                                            insert into tmpIdCorrecciones
											  SELECT IdCorreccion FROM b2bbas.correcciones 
											WHERE serieFactura LIKE CONCAT('%',pFolioFac,'%') 
											AND idproveedor= pIdProveedor
											And folioCarta  in (Select distinct Substring(CveReferencia,1,10) from desglosepagos where factura like CONCAT('%',pFolioFac,'%') and idpago=pclave)
											order by idcorreccion desc;
											
                                       
														 OPEN pCursorPedidos;
													     LOOP2:LOOP
														  Fetch Next From pCursorPedidos Into pIdCorreccion; 
																	  IF v_done THEN
																	  SET v_done := false;
																	  CLOSE pCursorPedidos;
																	  LEAVE LOOP2;
																	 END IF;
									
								
									SELECT iva FROM correcciontotales
									WHERE idcorreccion = pIdCorreccion INTO pIva;
									
										
									 
									 if pIva =  0 then 
											SELECT (IFNULL(SUM((REPLACE(REPLACE(de,',',''),'$','')-REPLACE(REPLACE(para,',',''),'$',''))*REPLACE(CantidadPrecioCambio,',','')),0)*0)
											FROM desglosecorrecciones 
											WHERE idcorreccion = pIdCorreccion
											AND cantidadpreciodescripcion = 'Precio'
											INTO pTPrecio;
										else
											SELECT (IFNULL(SUM((REPLACE(REPLACE(de,',',''),'$','')-REPLACE(REPLACE(para,',',''),'$',''))*REPLACE(CantidadPrecioCambio,',','')),0)*1.16)
											FROM desglosecorrecciones 
										WHERE idcorreccion = pIdCorreccion
											AND cantidadpreciodescripcion = 'Precio'
											INTO pTPrecio;
									end if; 
									
									
								IF NOT EXISTS(SELECT Folio FROM  tmpFolioProcesados WHERE Folio = pFolioFac and Descripcion='Precio' and idcorreccion = pIdCorreccion ) then
                           
									If pTPrecio > 0 THEN
                                    
									insert into tmpPagoDetalleReplica 
									values(pSuc,pConcepto,'Diferencia en precio',pFolio,format(replace((-1)*pTPrecio,',',''),2),
									pSaldo,pFolioFac,0,1);
									insert into tmpFolioProcesados values(pFolioFac,'Precio',pIdCorreccion);
									END IF;
									
									  
									if pTPrecio < 0 then 
										SET pEncontroNegativo = 'SI'; 
									end if; 
									
									
							 	END IF;
								
								
								if pIva =  0 then 
									SELECT (IFNULL(SUM((REPLACE(REPLACE(de,',',''),'$','')-REPLACE(REPLACE(para,',',''),'$',''))*REPLACE(CantidadPrecioCambio,',','')),0)*0)
									FROM desglosecorrecciones 
									WHERE idcorreccion = pIdCorreccion
									AND cantidadpreciodescripcion = 'Cantidad'
									INTO pTCantidad;
								else 
									SELECT (IFNULL(SUM((REPLACE(REPLACE(de,',',''),'$','')-REPLACE(REPLACE(para,',',''),'$',''))*REPLACE(CantidadPrecioCambio,',','')),0)*1.16)
									FROM desglosecorrecciones 
									WHERE idcorreccion = pIdCorreccion
									AND cantidadpreciodescripcion = 'Cantidad'
									INTO pTCantidad;
								end if;  
								
								  IF NOT EXISTS(SELECT Folio FROM  tmpFolioProcesados WHERE Folio = pFolioFac and Descripcion='Cantidad' and idcorreccion =  pIdCorreccion ) then
									
                                    If pTCantidad > 0 THEN
                                   
										insert into tmpPagoDetalleReplica 
										values(pSuc,pConcepto,'Descuento por Faltante de Mercancia',pFolio,format(replace((-1)*pTCantidad,',',''),2),
										pSaldo,pFolioFac,0,1);
										insert into tmpFolioProcesados values(pFolioFac,'Cantidad',pIdCorreccion);
									END IF;
									
									
									if pTCantidad < 0 then 
										SET pEncontroNegativo = 'SI'; 
									end if; 
									
									
									
								  END IF;
										IF pTCantidad = 0 AND pTPrecio = 0 THEN
										insert into tmpPagoDetalleReplica 
										values(pSuc,pConcepto,pDescripcion,pFolio,pImporte,pSaldo,pFolioFac,0,1);
										END IF;
                                     END LOOP LOOP2;
							
							  
					ELSE
						insert into tmpPagoDetalleReplica 
						values(pSuc,pConcepto,pDescripcion,pFolio,pImporte,pSaldo,pFolioFac,0,1);
					END IF;
						 insert into tmpPagoDetalleSinCorrecciones 
						 values(pSuc,pConcepto,pDescripcion,pFolio,pImporte,pSaldo,pFolioFac,0,1);
		
	END LOOP LOOP1;
	
    
    if pEncontroNegativo = 'SI' then 
            insert into tmpPagoDetalle
		    select * from tmpPagoDetalleSinCorrecciones; 
     elseif pEncontroNegativo ='NO' then 
            insert into tmpPagoDetalle 
			select  * from tmpPagoDetalleReplica;
	end if;
    
    
SET @Saldo := 0, @Tipo := 0,@Orden := 0, @Serie := '', @Importe := 0, @Saldo2 := 0, @Importe2 := 0;


SELECT 
    SUC,
    CONCEPTO,
    DESCRIPCION,
    FOLIO,
    FORMAT(REPLACE(IMPORTE, ',', ''), 2) AS 'IMPORTE',
    FORMAT(REPLACE(SALDO, ',', ''), 2) AS 'SALDO'
FROM
    (SELECT 
        y.SUC,
            y.CONCEPTO,
            y.DESCRIPCION,
            y.FOLIO,
            @Saldo2:=IF(@Serie = y.serie, @Saldo2 + y.Importe, y.importe) AS Saldo2,
            y.IMPORTE,
            @Saldo:=CASE
                WHEN y.Orden = 1 THEN IF(@Serie = y.serie, @Saldo2, y.Importe)
                ELSE y.Importe
            END AS Saldo,
            y.orden,
            @Serie:=y.serie AS serie,
            y.tipo
    FROM
        (SELECT 
        SUC,
            CONCEPTO,
            DESCRIPCION,
            FOLIO,
            IMPORTE,
            SALDO,
            orden,
            serie,
            tipo
    FROM
        tmpPagoDetalle
    ORDER BY orden DESC , serie DESC , tipo DESC) y) z;

SELECT 
    *
FROM
    tmpPagoDetalleReplica;
DROP TEMPORARY TABLE tempPago;
DROP TEMPORARY TABLE tmpPagoDetalle;
End ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcConsultarGruposUsr` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcConsultarGruposUsr`(IN `PTipo` vARCHAR(50), IN `PIdGrupo` vARCHAR(50), IN `PNombre` vARCHAR(50))
BEGIN
			If PTipo='Sin parametros' then
					Select * From GrupoUsuarios order by DescripcionGrupo;
					
			elseif PTipo='Con parametros' then
					Select * From GrupoUsuarios where IdGrupo = PIdGrupo;
					
			elseif PTipo='GenerarID' then
					Select if(Max(IdGrupo) is null, 0 ,Max(IdGrupo)) + 1 From GrupoUsuarios;
					
			elseif PTipo='Con Descr' then					
					
					Select IdGrupo From GrupoUsuarios where IdGrupo =PIdGrupo  OR trim(DescripcionGrupo) = PNombre;
					
			elseif PTipo='UsrEnGrupos' then 
					
					Select IdUsuario,NombreUsuario, privilegio From Usuarios where activo = true and IdUsuario IN(select IdUsuario							 FROM UsuariosGrupoUsuarios WHERE IdGrupo = PIdGrupo);
					
			elseif PTipo='UsrSinGrupos' then
					
					Select IdUsuario,NombreUsuario, privilegio   From Usuarios where activo = true and IdUsuario NOT IN(select							 IdUsuario FROM UsuariosGrupoUsuarios WHERE IdGrupo = PIdGrupo);
					
			elseif PTipo='DistIdSistemas' then
					
					SELECT distinct(IdSistema) as 'IdSistema', IdModulo FROM GrupoUsuariosModulos where IdGrupo = PIdGrupo;
			elseif PTipo='SinPrms Tds' then					
					select G.IdGrupo, G.DescripcionGrupo as 'Grupo', S.IdSistema, S.NombreSistema as 'Sistema', M.IdModulo, M							.DescripcionModulo as 'Modulo' from GrupoUsuarios G, Sistemas S, Modulos M, GrupoUsuariosModulos GUM where GUM						.IdGrupo = G.IdGrupo And GUM.Idsistema = S.IdSistema And GUM.IdModulo = M.IdModulo And GUM.IdSistema = M.IdSistema					order by DescripcionGrupo;
			end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcConsultarGrupoUsuariosModulos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcConsultarGrupoUsuariosModulos`(IN `PTipo` vARCHAR(50), IN `PIdGrupo` vARCHAR(50), IN `PIdSistema` vARCHAR(50), IN `PIdModulo` vARCHAR(50))
BEGIN
		if PTipo='Usr Modulos' then
				
				Select IdGrupo From GrupoUsuariosModulos where IdGrupo = PIdGrupo;
		ELSEIF PTipo='Usr Grupos' then
				
				Select IdGrupo,IdUsuario  From UsuariosGrupoUsuarios where IdGrupo = PIdGrupo;
		ELSEIF PTipo='ConIdModulo' then
				
				Select IdGrupo FROM GrupoUsuariosModulos WHERE IdGrupo = PIdGrupo and IdSistema = PIdSistema and IdModulo = PIdModulo;	
		end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcConsultarModulos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcConsultarModulos`(IN `PTipo` varchar(20), IN `PIdSistema` vARCHAR(50), IN `PIdModulo` vARCHAR(50))
BEGIN
		if PTipo='Todos admin' then
			
			Select M.IdSistema, S.NombreSistema, M.IdModulo, M.DescripcionModulo, M.IdModuloPadre, M.Pagina, M.ImagenIcono,orden1				,orden2,orden3,orden4,orden5 From Modulos M, Sistemas S WHERE M.IdSistema = S.IdSistema;
			
		elseif PTipo='Todos editar' then
				
				Select UPPER(IdSistema) as IdSistema, UPPER(IdModulo) as IdModulo, DescripcionModulo From Modulos where IdSistema 					=PIdSistema and IdModulo <> PIdModulo;	
											
		elseif PTipo='existencia Orden' then
				
				Select count(*) From modulos where concat(orden1,orden2,orden3,orden4,orden5)=PIdSistema;
		elseif PTipo='Todos' then
				SELECT DescripcionModulo,UPPER(IdModulopadre) as IdModulopadre,Pagina,ImagenIcono,orden1,orden2,orden3,orden4						,orden5 FROM modulos where idsistema=PIdSistema and idmodulo=PIdModulo;
				
		ELSEIF PTipo='ModulosAsig' then				
				Select IdSistema, IdModulo, DescripcionModulo From Modulos  WHERE idSistema = PIdSistema And  IdModulo IN(select		   		 IdModulo FROM UsuariosModulos WHERE IdUsuario = PIdModulo AND IdSistema = PIdSistema AND Activo = 1);
		ELSEIF PTipo='ModulosNoAsig' then		
				Select IdSistema, IdModulo, DescripcionModulo From Modulos  WHERE idSistema = PIdSistema And  IdModulo NOT IN						(select IdModulo FROM UsuariosModulos WHERE IdUsuario = PIdModulo AND IdSistema = PIdSistema AND Activo = 1);
		else

					Select IdSistema From Modulos where IdModulo = PIdModulo; 
									
		end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcConsultarReglaNegocio` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcConsultarReglaNegocio`(IN `PIdRegla` VARCHAR(20))
BEGIN
select query from reglanegocios where idregla=PIdRegla;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcConsultarSistemas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcConsultarSistemas`(IN `PTipo` varchar(50), IN `PIdSistema` vARCHAR(50))
BEGIN
		if PTipo='Todos' then
				
				Select IdSistema, NombreSistema, URL From Sistemas order by NombreSistema;
				
		elseif PTipo='SistemaFiltros' then
				Select IdSistema, NombreSistema, URL From Sistemas 
				where Filtro=1
				order by NombreSistema
				;
		else 
				
				Select NombreSistema, Url,Filtro From Sistemas where IdSistema = PIdSistema;
		END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcConsultarUsuarios` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcConsultarUsuarios`(IN `PTipo` VARCHAR(50), IN `PIdUsuario` vaRCHAR(50), IN `PNombre` vaRCHAR(50))
BEGIN

		IF PTipo='Sin Parmtrs' then
				
				SELECT U.idUsuario, U.NombreUsuario, U.contrasena, U.iDTipoUsuario, U.correoElectronico, U.activo, U.privilegio,
				u.NoEmpleado FROM  Usuarios U
				Order by U.NombreUsuario;
				
		ELSEIF PTipo='Con Parmtrs' THEN
				
				SELECT U.idUsuario, U.NombreUsuario, U.contrasena, U.iDTipoUsuario, U.correoElectronico, U.activo, U.privilegio,
				u.NoEmpleado,U.lectura FROM  Usuarios U where U.idUsuario = PIdUsuario or NombreUsuario = PNombre;
				
		ELSEIF PTipo='tipousuarios' THEN
				
				Select * from tipousuarios;
		ELSEIF PTipo='Por NoEmpleado' THEN
				select count(*) as cant from usuarios where IdUsuario=PIdUsuario;
		ELSEIF PTipo='UsrLogin' THEN				
				
				Select IdUsuario, contrasena, privilegio, activo,Ingsesion,noEmpleado From Usuarios WHERE IdUsuario = PIdUsuario					 and contrasena = PNombre;
		ELSEIF PTipo='SinPrm' THEN				
				Select IdUsuario,NombreUsuario, privilegio From Usuarios WHERE Activo = true
					order by NombreUsuario;
		END IF;
		

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcConsultarUsuariosModulos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcConsultarUsuariosModulos`(IN `PTipo` vARCHAR(30), IN `PIdSistema` vARCHAR(50), IN `PIdModulo` vARCHAR(50), IN `PIdGrupo` vARCHAR(50), IN `PIDUsuario` vARCHAR(50))
BEGIN
		if PTipo='Consulta IdModulo' then
				Select IdModulo From UsuariosModulos where IdSistema =PIdSistema and PIdModulo;
				
		ELSEIF PTipo='Consulta IdUsuario' then
				
				Select IdUsuario From UsuariosModulos where IdUsuario = PIdSistema;
				
		ELSEIF PTipo='UsrModulos 3Prm' then				
				Select * FROM UsuariosModulos WHERE IdUsuario = PIDUsuario and IdSistema = PIdSistema and IdModulo = PIdModulo and					IdGrupo=PIdGrupo;
				
		ELSEIF PTipo='GrpModulo' then								
				Select IdGrupo from UsuariosModulos where IdUsuario = PIDUsuario and IdSistema = PIdSistema and IdModulo = PIdModulo					and Activo = 1;

		ELSEIF PTipo='ConcatIdSist' then												
				select CONCAT(IdSistema, IdModulo) FROM UsuariosModulos WHERE IdUsuario = PIDUsuario;
		end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procConsultasDinamicas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procConsultasDinamicas`(IN `PAccion` VARCHAR(50), IN `PTabla` VARCHAR(50), IN `PColumnaID` VARCHAR(50), IN `PColumnaDesc` VARCHAR(50), IN `PBD` VARCHAR(50))
BEGIN
		if PAccion='ConsultaTabla' then			
		    SET @sentencia = CONCAT('SELECT ',PColumnaID,',',PColumnaDesc,' FROM ',PBD,'.',PTabla );		    
		    select  @sentencia;		    
		    PREPARE stmt FROM @sentencia;
		    EXECUTE stmt;
		    DEALLOCATE PREPARE stmt;
		end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcCorreoUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcCorreoUsuario`(pIdUsuario varchar(10))
begin 
	Select correoElectronico from sfseguridad.usuarios where idusuario = pIdUsuario;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procDestruyeSesion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procDestruyeSesion`(IN `pSID` VARCHAR(36))
    COMMENT 'Destruye los datos almacenados en la sesion del usuario'
BEGIN
	
	DELETE FROM variableSesion WHERE idSesion = pSID;
	DELETE FROM accesosSesion WHERE idSesion = pSID;
	DELETE FROM sesiones WHERE idSesion = pSID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procDestruyeSesion2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procDestruyeSesion2`()
BEGIN
	
	DELETE FROM variableSesion ;
	DELETE FROM accesosSesion ;
	DELETE FROM sesiones ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procFiltros` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procFiltros`(IN `PAccion` VARCHAR(50), IN `PId` VARCHAR(50), IN `PDescripcion` VARCHAR(50), IN `PSistema` VARCHAR(50), IN `PTabla` VARCHAR(50), IN `PColumId` VARCHAR(50), IN `PColumdesc` VARCHAR(50), IN `PStatus` VARCHAR(50), IN `PIP` VARCHAR(50), IN `PBD` VARCHAR(50), IN `PUsr` VARCHAR(50), IN `PPassword` VARCHAR(50), IN `PPuerto` VARCHAR(50))
BEGIN
		declare PIdFiltro int;
		if PAccion='Consulta' then
					select F.IdFiltro as 'ID',F.Filtro as 'Descripcion',F.NombreTabla as 'Tabla',F.ColumnaId,F.ColumnaDesc,
					S.Descripcion as 'status',F.IP,F.BaseDatos,F.Usuario,F.Password ,F.Puerto
					from filtros F inner join status S on S.IdStatus=F.IdStatus
					where F.IdStatus in (101,102);
					
		elseif PAccion='Consulta2' then	
					select * from filtros
					where idfiltro=PId;		
					
		elseif PAccion='Agregar' then
					set PIdFiltro=(SELECT ifnull(MAX(idfiltro), 0) + 1 as 'ID' FROM	filtros);
		
					insert into filtros(IdFiltro,Filtro,NombreTabla,ColumnaID,ColumnaDesc,IdStatus,IdSistema,IP,BaseDatos,Usuario,Password,Puerto) 
					values(PIdFiltro,PDescripcion,PTabla,PColumId,PColumdesc,PStatus,PSistema,PIP,PBD,PUsr,PPassword,PPuerto);
					
		elseif PAccion='Modificar' then
					Update Filtros 
					set Filtro=PDescripcion,NombreTabla=PTabla,ColumnaID=PColumId,ColumnaDesc=PColumdesc,IdStatus=PStatus,Puerto=PPuerto,
					IdSistema=PSistema,IP=PIP,BAseDatos=PBD,Usuario=PUsr,Password=PPassword
					where IdFiltro=PId;
					
		elseif PAccion='Eliminar' then
					Update Filtros 
					set IdStatus='103'
					where IdFiltro=PId;
					
		elseif PAccion='Consulta3' then
		
					select *
					from filtros
					where idfiltro=PId;
						
		elseif PAccion='Consulta4' then		
					select IdTabla,NombreTabla from filtrostablas where idstatus=101;
					
		elseif PAccion='Consulta5' then		
					
					select column_name 'col' from information_schema.columns
					where table_schema =PBD and table_name=PTabla;
					
		end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procFiltrosAsignados` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procFiltrosAsignados`(IN `PSID` VARCHAR(50))
BEGIN
		Declare PUsr Varchar(50);
		set PUsr=(select distinct(IdUsuario) from sesiones where idsesion=PSID);

		select F.NombreTabla,U.IdValor,F.ColumnaID from filtros F inner join filtrousuarios U
		on F.IdFiltro=U.IdFiltro where U.IdUsuario=PUsr order by F.ColumnaID ASC;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procfiltrousuarios` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procfiltrousuarios`(IN `PAccion` VARCHAR(50), IN `PIdFiltro` VARCHAR(50), IN `PIdUsuario` VARCHAR(50), IN `PIdValor` VARCHAR(50))
BEGIN
		if paccion='Insertar' then
				insert into filtrousuarios(IdFiltro,IdUsuario,IdValor)
				values(PIdFiltro,PIdUsuario,PIdValor);
				
		elseif paccion='Consulta' then		
					
			select F.NombreTabla,F.ColumnaID,F.ColumnaDesc,FU.IdValor,F.IP,F.BaseDatos,F.Usuario,F.Password,F.puerto from filtrousuarios as FU
			inner join Filtros as F on FU.IdFiltro=F.IdFiltro
			where FU.idusuario=PIdUsuario and FU.IdFiltro=PIdFiltro;
			
		elseif paccion='Consulta2' then	
					
				select distinct(idfiltro) from filtrousuarios where idusuario=PIdUsuario;
		
		elseif PAccion='Borrar' then
				
					delete from filtrousuarios where idusuario=PIdUsuario and IdFiltro=PIdFiltro;

		end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procgetAuditor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procgetAuditor`()
begin 
 select '-1' as idusuario, ' --Seleccione--' as nombreusuario 
  union all 
 select idusuario, nombreusuario from usuarios where privilegio = 'B' and activo = 1; 
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procgrupousuarios` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procgrupousuarios`(IN `PidGrupo` VARCHAR(50), IN `PdescripcionGrupo` VARCHAR(50), IN `PesPublico` VARCHAR(50), IN `PXml` VARCHAR(50), IN `PAccion` VARCHAR(50))
BEGIN
			If PAccion='Consulta' then
					if PidGrupo='-1' then
							Select * From GrupoUsuarios order by DescripcionGrupo;
					
					else 
							Select * From GrupoUsuarios where IdGrupo = PidGrupo;
					end if;
			end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcInsertComentario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcInsertComentario`(IN `PIdBaja` vARCHAR(50), IN `PUsrAdm` vARCHAR(50), IN `PComentario` vARCHAR(50))
BEGIN
		insert into comentariosbajas(IdUsuarioBaja,IdUsuarioAdm,Fecha,Comentario) values(PIdBaja,PUsrAdm,now(),PComentario);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procLogin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `procLogin`(IN `pIdUsuario` vARCHAR(50), IN `pPassword` VARCHAR(50))
    COMMENT 'Valida login del usuario y registra la sesion de usuario y sus o'
BEGIN
	DECLARE vExiste INTEGER;
	DECLARE vSID VARCHAR(36);
	Declare uEspecial VarChar(10);

	if Instr(pidUsuario,'/') > 0 then
		select SUBSTRING_INDEX(pidUsuario, '/', -1) into uEspecial;
		select SUBSTRING_INDEX(pidUsuario, '/', 1) into pidUsuario;
	else
		set uEspecial = NULL;
	end if;

	SELECT COUNT(*) INTO vExiste FROM usuarios
	WHERE idUsuario = pIdUsuario AND contrasena like binary pPassword and Activo=1 ;
	
	
	IF (vExiste > 0) THEN
		SELECT UUID() INTO vSID;
		SELECT 'AUTORIZADO' AS autorizacion, vSID as SID;
		Select Privilegio from usuarios where idUsuario = pIdUsuario;
		


		delete from accesosSesion 
		where idsesion in (select idsesion from sesiones where idusuario=pIdUsuario);

		delete from sesiones where idusuario=pIdUsuario;
		update usuariosession set sesionactiva='N' 
		where NoEmpleado=(select NoEmpleado from usuarios where idusuario=pIdUsuario);

		
		INSERT INTO sesiones
			
			SELECT vSID,NOW(),
			u.idUsuario,
			u.contrasena,u.correoElectronico,
			case when isnull(uEspecial) then u.iDTipoUsuario else 'I' end uidTipoUsuario,
			u.privilegio, uEspecial
			FROM usuarios as u
			WHERE u.idUsuario = pidUsuario;
		
		

		INSERT INTO accesosSesion (idSesion,idSistema,idModulo,descripcionModulo,idModuloPadre,URL,pagina,imagenIcono,
		orden1,orden2,orden3,orden4,orden5)
			SELECT distinct vSID, idSistema, idModulo, descripcionModulo, idModuloPadre, URL, pagina, imagenIcono,
			orden1,orden2,orden3,orden4,orden5 FROM
				(
				select m.idSistema, m.idModulo, m.descripcionModulo, 
				m.idModuloPadre, s.URL, m.pagina,m.imagenIcono,m.orden1,m.orden2,m.orden3,m.orden4,m.orden5
				from sistemas as s, modulos as m, usuariosModulos as um, usuarios as u
				where s.idSistema = m.idSistema 
				and m.idSistema = um.idSistema
				and m.idModulo = um.idModulo
				and um.idUsuario = u.idUsuario
				
				
				and u.idUsuario = pIdUsuario
				and um.activo = 1
				UNION ALL
				
				select m.idSistema, m.idModulo, m.descripcionModulo, 
				m.idModuloPadre, s.URL, m.pagina, m.imagenIcono,m.orden1,m.orden2,m.orden3,m.orden4,m.orden5 
				from sistemas as s, modulos as m, rolesModulos as rm, usuarios as u, rolesUsuarios as ru, roles as r
				where s.idSistema = m.idSistema 
				and m.idSistema = rm.idSistema
				and m.idModulo = rm.idModulo
				and r.idRol = rm.idRol
				and r.idRol = ru.idRol
				
				and ru.idUsuario = u.idUsuario
				
				and u.idUsuario = pIdUsuario
				UNION ALL
				
				select m.idSistema, m.idModulo, m.descripcionModulo, 
				m.idModuloPadre, s.URL, m.pagina, m.imagenIcono,m.orden1,m.orden2,m.orden3,m.orden4,m.orden5 
				from sistemas as s, modulos as m, grupoUsuariosModulos as gum, usuarios as u, usuariosGrupoUsuarios as ugu, grupoUsuarios as gu
				where s.idSistema = m.idSistema 
				and m.idSistema = gum.idSistema
				and m.idModulo = gum.idModulo
				and gu.idGrupo = gum.idGrupo
				and gu.idGrupo = ugu.idGrupo
				
				and ugu.idUsuario = u.idUsuario
				
				and u.idUsuario = pIdUsuario
				) AS opcionesUsuario;
				COMMIT;
	ELSE

		if not exists(select idusuario from usuarios where idusuario=pIdUsuario) then
					select 'Usuario Incorrecto.' AS autorizacion;
		elseif  not exists(select idusuario from usuarios where contrasena=pPassword and idusuario=pIdUsuario) then
					select 'Contraseña Incorrecta.' AS autorizacion;
		elseif (select activo from usuarios where idusuario=pIdUsuario)=0 then
					select 'Usuario Inactivo.' AS autorizacion;
		end if;
		
	END IF;
	Select * From usuarios where idusuario=pIdUsuario;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procLogin_gs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `procLogin_gs`(IN `pIdUsuario` vARCHAR(50), IN `pPassword` VARCHAR(50))
    COMMENT 'Valida login del usuario y registra la sesion de usuario y sus o'
BEGIN
	DECLARE vExiste INTEGER;
	DECLARE vSID VARCHAR(36);
	Declare uEspecial VarChar(10);

	if Instr(pidUsuario,'/') > 0 then
		select SUBSTRING_INDEX(pidUsuario, '/', -1) into uEspecial;
		SELECT SUBSTRING_INDEX(pidUsuario, '/', 1) INTO pidUsuario;
	else
		set uEspecial = NULL;
	end if;

	SELECT 
    COUNT(*)
INTO vExiste FROM
    usuarios
WHERE
    idUsuario = pIdUsuario
        AND contrasena LIKE BINARY pPassword
        AND Activo = 1;
	
	
	IF (vExiste > 0) THEN
		SELECT UUID() INTO vSID;
		 
		DELETE FROM accesosSesion 
WHERE
    idsesion IN (SELECT 
        idsesion
    FROM
        sesiones
    
    WHERE
        idusuario = pIdUsuario);

		DELETE FROM sesiones 
WHERE
    idusuario = pIdUsuario;
		UPDATE usuariosession 
SET 
    sesionactiva = 'N'
WHERE
    NoEmpleado = (SELECT 
            NoEmpleado
        FROM
            usuarios
        WHERE
            idusuario = pIdUsuario);

		
		INSERT INTO sesiones
			
			SELECT vSID,NOW(),
			u.idUsuario,
			u.contrasena,u.correoElectronico,
			case when isnull(uEspecial) then u.iDTipoUsuario else 'I' end uidTipoUsuario,
			u.privilegio, uEspecial
			FROM usuarios as u
			WHERE u.idUsuario = pidUsuario;
		
		

		INSERT INTO accesosSesion (idSesion,idSistema,idModulo,descripcionModulo,idModuloPadre,URL,pagina,imagenIcono,
		orden1,orden2,orden3,orden4,orden5)
			SELECT distinct vSID, idSistema, idModulo, descripcionModulo, idModuloPadre, URL, pagina, imagenIcono,
			orden1,orden2,orden3,orden4,orden5 FROM
				(
				select m.idSistema, m.idModulo, m.descripcionModulo, 
				m.idModuloPadre, s.URL, m.pagina,m.imagenIcono,m.orden1,m.orden2,m.orden3,m.orden4,m.orden5
				from sistemas as s, modulos as m, usuariosModulos as um, usuarios as u
				where s.idSistema = m.idSistema 
				and m.idSistema = um.idSistema
				and m.idModulo = um.idModulo
				and um.idUsuario = u.idUsuario
				
				
				and u.idUsuario = pIdUsuario
				and um.activo = 1
				UNION ALL
				
				select m.idSistema, m.idModulo, m.descripcionModulo, 
				m.idModuloPadre, s.URL, m.pagina, m.imagenIcono,m.orden1,m.orden2,m.orden3,m.orden4,m.orden5 
				from sistemas as s, modulos as m, rolesModulos as rm, usuarios as u, rolesUsuarios as ru, roles as r
				where s.idSistema = m.idSistema 
				and m.idSistema = rm.idSistema
				and m.idModulo = rm.idModulo
				and r.idRol = rm.idRol
				and r.idRol = ru.idRol
				
				and ru.idUsuario = u.idUsuario
				
				and u.idUsuario = pIdUsuario
				UNION ALL
				
				select m.idSistema, m.idModulo, m.descripcionModulo, 
				m.idModuloPadre, s.URL, m.pagina, m.imagenIcono,m.orden1,m.orden2,m.orden3,m.orden4,m.orden5 
				from sistemas as s, modulos as m, grupoUsuariosModulos as gum, usuarios as u, usuariosGrupoUsuarios as ugu, grupoUsuarios as gu
				where s.idSistema = m.idSistema 
				and m.idSistema = gum.idSistema
				and m.idModulo = gum.idModulo
				and gu.idGrupo = gum.idGrupo
				and gu.idGrupo = ugu.idGrupo
				
				and ugu.idUsuario = u.idUsuario
				
				and u.idUsuario = pIdUsuario
				) AS opcionesUsuario;
				COMMIT;
	 
	END IF;
    
SELECT 
    if(vSID = null,'', 'AUTORIZADO') AS autorizacion,
    vSID AS SID,
    idUsuario,
    contrasena,
    iDTipoUsuario,
    noEmpleado,
    Lectura,
    correoElectronico AS correoElectronico,
    activo AS activo,
    fechaRegistro AS fechaRegistro,
    privilegio AS privilegio,
    nombreUsuario AS nombreUsuario,
    IngSesion AS IngSesion,
    idUsuarioAutoriza AS idUsuarioAutoriza,
    CentroCostos AS CentroCostos,
    Puesto AS Puesto,
    Descripcion AS Descripcion
FROM
    usuarios
WHERE
    idUsuario = pIdUsuario;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcModificarCont` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcModificarCont`(IN `PIdUsuario` vARCHAR(50), IN `PContrs` vARCHAR(50))
BEGIN
UPDATE usuarios SET contrasena=PContrs,ingsesion=1 WHERE Idusuario = PIdUsuario;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcModificarGruposUsr` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcModificarGruposUsr`(IN `PTipo` vARCHAR(50), IN `PIdGrupo` vARCHAR(50), IN `PDescripcion` vARCHAR(50), IN `PPublico` vARCHAR(50))
BEGIN
		if PTipo='Insertar' then
				INSERT INTO GrupoUsuarios(IdGrupo, DescripcionGrupo, EsPublico) VALUES (PIdGrupo,PDescripcion ,PPublico );
				
		Elseif PTipo='Modificar' then
				UPDATE GrupoUsuarios SET DescripcionGrupo = PDescripcion, EsPublico = PPublico WHERE IdGrupo = PIdGrupo;
		Elseif PTipo='Eliminar' then
				DELETE FROM GrupoUsuarios WHERE IdGrupo = PIdGrupo;
		end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcModificarGrupoUsuariosModulos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcModificarGrupoUsuariosModulos`(IN `PTIpo` vARCHAR(50), IN `PIdGrupo` vARCHAR(50), IN `PIdSistema` vARCHAR(50), IN `PIdModulo` vARCHAR(50))
BEGIN
		if PTipo='Insertar' then
				INSERT INTO GrupoUsuariosModulos(IdGrupo, IdSistema, IdModulo) VALUES (PIdGrupo,PIdSistema, PIdModulo);
		end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcModificarModulo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcModificarModulo`(IN `PTipo` vARCHAR(20), IN `PIdSistema` vARCHAR(50), IN `PIdModulo` vARCHAR(50), IN `PDescripcion` VARCHAR(50), IN `PModuloPadre` vARCHAR(50), IN `PPagina` vARCHAR(100), IN `PImg` vARCHAR(100), IN `Porden1` vARCHAR(10), IN `Porden2` vARCHAR(10), IN `Porden3` vARCHAR(10), IN `Porden4` vARCHAR(10), IN `Porden5` vARCHAR(10))
BEGIN
		IF PTipo='Modificar' then
				UPDATE Modulos SET IdModulo = PIdModulo, DescripcionModulo = PDescripcion, IdModuloPadre = PModuloPadre, Pagina =						 PPagina, ImagenIcono = PImg,orden1=Porden1, orden2=Porden2, orden3=Porden3, orden4=Porden4, orden5=Porden5 WHERE						 IdSistema = PIdSistema and IdModulo =PIdModulo;
		elseif PTipo='Eliminar' then
					DELETE FROM Modulos WHERE IdSistema = PIdSistema and IdModulo = PIdModulo;
		elseif PTipo='Insertar' then
				INSERT INTO Modulos(IdSistema, IdModulo, DescripcionModulo, IdModuloPadre, Pagina, ImagenIcono,orden1,orden2,orden3					,orden4,orden5) VALUES (PIdSistema, PIdModulo, PDescripcion, PModuloPadre, PPagina, PImg, Porden1, Porden2, Porden3,					Porden4, Porden5);
		End If;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcModificarSistema` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcModificarSistema`(IN `PTipo` vARCHAR(20), IN `PIdSistema` varCHAR(50), IN `PNombre` vaRCHAR(50), IN `PUrl` varCHAR(50), IN `PFiltro` vARCHAR(20))
BEGIN
	IF PTipo='Modificar' then
			UPDATE sistemas SET nombreSistema = PNombre, URL = PUrl,Filtro=PFiltro WHERE idSistema = PIdSistema;
	elseif Ptipo='Eliminar' then
			DELETE FROM sistemas WHERE idSistema = PIdSistema;
	elseif Ptipo='Insertar' then
			INSERT INTO sistemas (idSistema, nombreSistema, URL,Filtro) VALUES (PIdSistema, PNombre, PUrl,PFiltro);
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcModificarUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcModificarUsuario`(IN `PTipo` vARCHAR(50), IN `PIdUsuario` vARCHAR(50), IN `PContrasena` vARCHAR(50), IN `PTipoUsr` vARCHAR(50), IN `PCorreo` VARCHAR(100), IN `PActivo` vARCHAR(50), IN `PFRegistro` vARCHAR(50), IN `PPrivilegio` vARCHAR(50), IN `PNombre` vARCHAR(50), IN `PNoEmpledo` vARCHAR(50), IN `Plectura` vARCHAR(50))
BEGIN
		if PTipo='Insert Usr' then
				
				INSERT INTO Usuarios ( idUsuario, contrasena, idTipoUsuario, correoElectronico, activo, fechaRegistro, privilegio,
				nombreUsuario, NoEmpleado,Lectura) VALUES (PIdUsuario, PContrasena, PTipoUsr,PCorreo, 
				PActivo,PFRegistro, PPrivilegio,PNombre, PNoEmpledo,Plectura);
		
		elseif PTipo='Insert UsrSession' then		
						
				insert into alertas.usuariosession (NoEmpleado,Idstatus,sesionActiva) values(PNoEmpledo,'101','N');
		
		elseif PTipo='Insert UsrAlmacen' then		
				
				insert into alertas.usuarioalmacen (usuario,Idalmacen) values(PNoEmpledo,(select id_almacen from wms.almacen limit 1)				 );
				
		elseif PTipo='Modificar Usuario' then		
				UPDATE Usuarios SET Contrasena = PContrasena, correoElectronico = PCorreo , activo = PActivo, privilegio=PPrivilegio, 
				NombreUsuario = PNombre,Lectura=Plectura  WHERE idUsuario = PIdUsuario;
		Elseif PTipo='Eliminar' then
				DELETE FROM Usuarios WHERE IdUsuario =PIdUsuario ;
		elseif ptipo='CambiarContrasena' then
			UPDATE Usuarios SET Contrasena= PContrasena,ingsesion =1  WHERE idUsuario = PIdUsuario;
				
		end if;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcModificarUsuariosGrupoUsuarios` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcModificarUsuariosGrupoUsuarios`(IN `PTipo` vARCHAR(50), IN `PIdGrupo` vARCHAR(50), IN `PIdUsuaurio` vARCHAR(50))
BEGIN
		if PTipo='Eliminar' then
				DELETE from UsuariosGrupoUsuarios WHERE IdGrupo = PIdGrupo;
		elseif PTipo='Insertar' then
				INSERT INTO UsuariosGrupoUsuarios(IdGrupo, IdUsuario) VALUES (PIdGrupo, PIdUsuaurio);
		end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcModificarUsuariosModulos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcModificarUsuariosModulos`(IN `PTipo` vARCHAR(50), IN `PIdGrupo` vARCHAR(50), IN `PIdUsuario` vARCHAR(50), IN `PIdSistema` vARCHAR(50), IN `PIdModulo` vARCHAR(50))
BEGIN
		if PTipo='Eliminar' then
				DELETE from UsuariosModulos WHERE IdGrupo = 	PIdGrupo;
		elseif PTipo='EliminarxIdUsr' then				
				DELETE FROM  UsuariosModulos WHERE IdUsuario =PIdUsuario  and IdSistema = PIdSistema and Activo = 1;		
		elseif PTipo='Insertar' then
				INSERT INTO UsuariosModulos( IdUsuario, IdSistema, IdModulo, IdGrupo) VALUES (PIdUsuario ,PIdSistema ,PIdModulo						,PIdGrupo);
		end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procmodulos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.4.248` PROCEDURE `procmodulos`(IN `PidSistema` VARCHAR(50), IN `PidModulo` VARCHAR(50), IN `PdescripcionModulo` VARCHAR(50), IN `PidModuloPadre` VARCHAR(50), IN `Ppagina` VARCHAR(100), IN `PimagenIcono` VARCHAR(50), IN `Porden1` VARCHAR(50), IN `Porden2` VARCHAR(50), IN `Porden3` VARCHAR(50), IN `Porden4` VARCHAR(50), IN `Porden5` VARCHAR(50), IN `PXml` VARCHAR(50), IN `PAccion` VARCHAR(50))
BEGIN
		if PAccion='Consulta' then
				if PidSistema='-1' then 
						
						Select M.IdSistema, S.NombreSistema, M.IdModulo, M.DescripcionModulo, M.IdModuloPadre, M.Pagina, M.ImagenIcono,
						orden1,orden2,orden3,orden4,orden5 
						From Modulos M 
						inner join Sistemas S ON M.IdSistema = S.IdSistema;	
				else
						
						Select IdSistema From Modulos where IdSistema = PidSistema limit 1; 	
				ENd if;
				
		Elseif PAccion='Consulta2' then
				Select UPPER(IdSistema) as IdSistema, UPPER(IdModulo) as IdModulo, DescripcionModulo From Modulos where 
				IdSistema=PIdSistema and IdModulo <> PIdModulo;	

		elseif PAccion='existeOrden' then
				
				Select count(*) From modulos where concat(orden1,orden2,orden3,orden4,orden5)=PIdSistema;
				
		elseif PAccion='exisIdModulo' then
					Select * From Modulos where IdModulo = PIdModulo; 
					
		elseif PAccion='Insertar' then
					INSERT INTO Modulos(IdSistema, IdModulo, DescripcionModulo, IdModuloPadre, Pagina, ImagenIcono,orden1,orden2,
					orden3,orden4,orden5) 
					VALUES (PIdSistema, PIdModulo, PdescripcionModulo, PidModuloPadre, PPagina, PimagenIcono, Porden1, Porden2, Porden3,
					Porden4, Porden5);
					
		elseif PAccion='Modificar' then
				UPDATE Modulos 
				SET IdModulo = PIdModulo, DescripcionModulo = PDescripcionModulo, IdModuloPadre = PidModuloPadre, Pagina = PPagina,
				ImagenIcono = PimagenIcono,orden1=Porden1, orden2=Porden2, orden3=Porden3, orden4=Porden4, orden5=Porden5 
				WHERE	IdSistema = PIdSistema and IdModulo =PIdModulo;
				
		elseif PAccion='Eliminar' then
					DELETE FROM Modulos WHERE IdSistema = PIdSistema and IdModulo = PIdModulo;
											
		end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procObtenerUsuario_Auditoria` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procObtenerUsuario_Auditoria`()
begin 
	Select  idUsuario as 'ID', nombreUsuario as 'Desc' 
    from usuarios where  noEmpleado <>'' 
    and idusuario in(select distinct idusuario from usuariosmodulos where idsistema = 'AUDITORIA') order by idUsuario; 
    
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procObtieneCorreos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procObtieneCorreos`()
begin 
	select distinct idusuario,correoElectronico from sfseguridad.usuarios where correoElectronico is not null and correoElectronico <> '';
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procPermisoPagina` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `procPermisoPagina`(IN `pSID` VARCHAR(36), IN `pPagina` VARCHAR(100))
    COMMENT 'Consulta si la sesion de un usuario tiene permisos sobre una pag'
BEGIN
	DECLARE permiso INT;
	DECLARE idProv VARCHAR(10);
	DECLARE idUsr VARCHAR(10);
	DECLARE idSis VARCHAR(10);
	DECLARE idMod VARCHAR(20);
	DECLARE uEspecial VARCHAR(10);
	
	SELECT EXISTS (SELECT * FROM accesosSesion WHERE idSesion = pSID AND pagina = pPagina) INTO permiso;
	
	IF permiso = 1 THEN
		SELECT idUsuario INTO idUsr FROM sesiones WHERE idSesion = pSID;
		SELECT usuarioEspecial INTO uEspecial FROM sesiones WHERE idSesion = pSID;
		SELECT idSistema INTO idSis FROM accesosSesion WHERE idSesion = pSID AND pagina = pPagina limit 1;
		SELECT idModulo INTO idMod FROM accesosSesion WHERE idSesion = pSID AND pagina = pPagina limit 1;
		IF NOT EXISTS (SELECT * FROM bitacoraAccesos WHERE idUsuario = IdUsr AND idSistema = idSis AND idModulo = idMod
			AND idSesion = pSID and usuarioEspecial = uEspecial) THEN
			INSERT INTO bitacoraAccesos VALUES (idUsr,idSis,IdMod,NOW(),PSID,uEspecial);
		END IF;
	END IF;
	
	SELECT permiso as permiso;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procPermisosSesion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `procPermisosSesion`(IN `pSID` VARCHAR(36))
BEGIN
	DECLARE PIng int;
	
	SELECT * FROM sesiones WHERE idSesion = pSID; 
	set PIng=(select U.Lectura from sesiones as S
				inner join usuarios as U on S.idUsuario=U.idUsuario
				where S.idSesion=pSID);
	
	
	SELECT idSistema, idModulo, descripcionModulo, idModuloPadre, CONCAT(URL,'/',pagina,'?SID=',pSID,'&L=',PIng) AS URL, imagenIcono, concat(orden1,orden2,orden3,orden4,orden5) as 'Value'
	FROM accesosSesion WHERE idSesion = pSID
	ORDER BY orden1,orden2,orden3,orden4,orden5;
	

	
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procPermisosSesion_gs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `procPermisosSesion_gs`(IN `pSID` VARCHAR(36))
BEGIN
	DECLARE PIng int;
	
	/*SELECT * FROM sesiones WHERE idSesion = pSID; */
	set PIng=(select U.Lectura from sesiones as S
				inner join usuarios as U on S.idUsuario=U.idUsuario
				where S.idSesion=pSID);
	
	SELECT idSistema, idModulo, descripcionModulo, idModuloPadre, CONCAT(URL,'/',pagina,'?SID=',pSID,'&L=',PIng)  AS URL, 
    if(imagenIcono = null, '', imagenIcono) as imagenIcono, 
    if(concat(orden1,orden2,orden3,orden4,orden5) = null ,'' ,concat(orden1,orden2,orden3,orden4,orden5) )as 'Value'
	FROM accesosSesion
    WHERE idSesion = pSID
	ORDER BY orden1,orden2,orden3,orden4,orden5;
    

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procSeguridadUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procSeguridadUsuario`(IN `pIdProveedor` VARCHAR(10), IN `pIdUsuario` VARCHAR(10), IN `pPassword` VARCHAR(50))
    COMMENT 'Consulta permisos de acceso de los usuarios'
BEGIN
	
	select u.idUsuario,u.contrasena,u.correoElectronico,u.iDTipoUsuario,u.privilegio 
	from usuarios as u
	where	u.idUsuario = pIdUsuario
	and u.contrasena = pPassword;
	
	
	SELECT distinct idSistema, idModulo, descripcionModulo, idModuloPadre, URL FROM
	(
	select m.idSistema, m.idModulo, m.descripcionModulo, 
	m.idModuloPadre, CONCAT(s.URL,'/',m.pagina) as URL 
	from sistemas as s, modulos as m, usuariosModulos as um, usuarios as u
	where s.idSistema = m.idSistema 
	and m.idSistema = um.idSistema
	and m.idModulo = um.idModulo
	and u.idUsuario = pIdUsuario
	and u.contrasena = pPassword
	and um.activo = 1
	UNION ALL
	
	select m.idSistema, m.idModulo, m.descripcionModulo, 
	m.idModuloPadre, CONCAT(s.URL,'/',m.pagina) as URL 
	from sistemas as s, modulos as m, rolesModulos as rm, usuarios as u, rolesUsuarios as ru, roles as r
	where s.idSistema = m.idSistema 
	and m.idSistema = rm.idSistema
	and m.idModulo = rm.idModulo
	and r.idRol = rm.idRol
	and r.idRol = ru.idRol
	and ru.idUsuario = u.idUsuario
	and u.idUsuario = pIdUsuario
	and u.contrasena = pPassword
	UNION ALL
	
	select m.idSistema, m.idModulo, m.descripcionModulo, 
	m.idModuloPadre, CONCAT(s.URL,'/',m.pagina) as URL 
	from sistemas as s, modulos as m, grupoUsuariosModulos as gum, usuarios as u, usuariosGrupoUsuarios as ugu, grupoUsuarios as gu
	where s.idSistema = m.idSistema 
	and m.idSistema = gum.idSistema
	and m.idModulo = gum.idModulo
	and gu.idGrupo = gum.idGrupo
	and gu.idGrupo = ugu.idGrupo
	and ugu.idUsuario = u.idUsuario
	and u.idUsuario = pIdUsuario
	and u.contrasena = pPassword) AS opcionesUsuario
	ORDER BY idSistema, idModuloPadre, idModulo;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ProcSessionUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `ProcSessionUsuario`(IN `PTipo` VARCHAR(50), IN `PIdUsuario` VARCHAR(50))
BEGIN
		declare NEmpleado varchar(50);
		declare IdUsu varchar(100);
		set NEmpleado=(select noEmpleado from usuarios where idusuario=PIdusuario);
		
		if PTipo='session' then
			select US.ultimaSession, ifnull(US.sesionActiva,'N') sesionActiva, ifnull(US.Idstatus,101) Idstatus,US.FechaCambio,US.FechaBloqueado,U.IngSesion,U.Lectura 
				from usuariosession as US
				right join usuarios as U on U.noEmpleado=US.NoEmpleado
				where U.idUsuario=PIdUsuario;
			
				
		elseif PTipo='Bloquear' then
				update usuariosession set idstatus=103,FechaBloqueado=now() where noempleado=NEmpleado;
				
		elseif PTipo='AbrirSession' then	
				update usuariosession set ultimasession=now(),sesionActiva='S' where NoEmpleado=NEmpleado;	
					
		elseif PTipo='CerrarSession' then	
				
				update usuariosession set sesionActiva='N' where NoEmpleado=NEmpleado;
				
		elseif PTipo='CerrarSession2' then
				
				set IdUsu=(select distinct(IdUsuario) from sesiones where idsesion=PIdUsuario);
						
				set NEmpleado=(select noEmpleado from usuarios where idusuario=IdUsu);
				update usuariosession set sesionActiva='N' where NoEmpleado=NEmpleado;
				
		elseif PTipo='DesBloquear' then
				update usuariosession set idstatus=101 where noempleado=NEmpleado;
				

				
      end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procsistemas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procsistemas`(IN `PidSistema` VARCHAR(50), IN `PnombreSistema` VARCHAR(50), IN `PURL` VARCHAR(50), IN `PFiltro` VARCHAR(50), IN `PXML` VARCHAR(50), IN `Paccion` VARCHAR(50))
BEGIN
		if PAccion='Consulta' then
				if PidSistema='-1' then 
						
						Select IdSistema, NombreSistema, URL From Sistemas order by NombreSistema;
				else
						
						Select NombreSistema, Url,Filtro From Sistemas where IdSistema = PIdSistema;
				end if;				
				
		elseif PAccion='SistemaFiltros' then
				Select IdSistema, NombreSistema, URL From Sistemas 
				where Filtro=1
				order by NombreSistema;
				
		elseif PAccion='Insertar' then
				INSERT INTO sistemas (idSistema, nombreSistema, URL,Filtro) 
				VALUES (PIdSistema, PnombreSistema, PURL,PFiltro);
				
		elseif PAccion='Modificar' then
				UPDATE sistemas SET nombreSistema = PnombreSistema, URL = PURL,Filtro=PFiltro WHERE idSistema = PidSistema;
				
		elseif PAccion='Eliminar' then
				DELETE FROM sistemas WHERE idSistema = PIdSistema;
				
		
				
		END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procUsrLog` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procUsrLog`(IN `PTipo` VARCHAR(50), IN `PIdUsuario` VARCHAR(50), IN `PContrasena` VARCHAR(50))
BEGIN
			Declare PID int;
			Declare PCant int;
			Declare PPass int;
		IF PTipo='Consulta' THEN
				
				select * from usuarios where Idusuario=PIdUsuario and contrasena=PContrasena;
			
		elseif Ptipo='Consulta2' then 
				
				select * from usuarios where idusuario=PIdUsuario;
				
		elseif PTipo='ModificarContrasena' then
				
				UPDATE Usuarios SET Contrasena= PContrasena,ingsesion =1 WHERE idUsuario = PIdUsuario;
				
				update usuariosession set FechaCambio=now() 
				where NoEmpleado=(select noempleado from usuarios where idusuario=PIdUsuario);
				
		elseif PTipo='ModificarIngsesion' then
				UPDATE Usuarios SET ingsesion =0 WHERE idUsuario = PIdUsuario;
				
				
		Elseif PTipo='AdminContrasenas' then

			set PID=(select ifnull(max(Idpassword),0)+1 from usuariopasswords ); 
			If not exists(select * from usuariopasswords where idusuario=PIdUsuario) then
					
				
					insert into usuariopasswords (IdPassword,IdUsuario,Contrasena,fecha)
					values(PID,PIdUsuario,PContrasena,now());
					Select 'S';
			else
						if not exists(select * from usuariopasswords where idUsuario=PIdUsuario and Contrasena=PContrasena) then
						
							set PCant=(select count(*) from usuariopasswords where  idusuario=PIdUsuario); 
					
							if PCant>=4 then 
											
											set PPass=(select IdPassword from usuariopasswords where idusuario=PIdUsuario order by fecha asc limit 1);
											delete from usuariopasswords 
											where IdPassword=PPass;
							end if;					

							insert into usuariopasswords (IdPassword,IdUsuario,Contrasena,fecha)
							values(PID,PIdUsuario,PContrasena,now());			

							Select 'S';
							
						else
							Select 'N';
						End If;			
				
			end if;

		elseif PTipo='ModificarIngSesion' then
				
				UPDATE Usuarios SET ingsesion =0 WHERE idUsuario = PIdUsuario;
		END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procUsuarios` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.7.48` PROCEDURE `procUsuarios`(
IN `PidUsuario` VARCHAR(50), 
IN `Pcontrasena` VARCHAR(50), 
IN `PiDTipoUsuario` VARCHAR(50), 
IN `PcorreoElectronico` VARCHAR(50), 
IN `Pactivo` VARCHAR(50), 
IN `PfechaRegistro` VARCHAR(50), 
IN `Pprivilegio` VARCHAR(50), 
IN `PnombreUsuario` VARCHAR(50), 
IN `PnoEmpleado` VARCHAR(50), 
IN `PIngSesion` VARCHAR(50), 
IN `PLectura` VARCHAR(50), 
IN `pxml` INT, 
IN `PAccion` VARCHAR(50),
IN `pCnTienda` VARCHAR(10))
begin
	declare c integer;
    Declare pSociedad varchar(5);
    
		IF PAccion='Consulta' THEN
				if PidUsuario='-1' then							
							SELECT U.idUsuario, U.NombreUsuario, U.contrasena, U.iDTipoUsuario, U.correoElectronico, U.activo, U.privilegio,
							u.NoEmpleado FROM  Usuarios U where activo=1;
							
							
				else							
							SELECT U.idUsuario, U.NombreUsuario, U.contrasena, U.iDTipoUsuario, U.correoElectronico, U.activo, U.privilegio,
							u.NoEmpleado,U.lectura FROM  Usuarios U where U.idUsuario = PIdUsuario or NombreUsuario = PnombreUsuario;				
							
				end if;
		elseif PAccion='Consulta2' THEN		
				if PiDTipoUsuario='C' then 
						SELECT count(U.idusuario) FROM  Usuarios U where U.idUsuario = PIdUsuario or NoEmpleado=PnoEmpleado;
				else
						SELECT count(U.idusuario) FROM  Usuarios U where NoEmpleado=PnoEmpleado;
				End if;
							
				
		elseif PAccion='tipousuarios' THEN							
				Select * from tipousuarios;							
							
		Elseif PAccion='Eliminar' then
				DELETE FROM Usuarios WHERE IdUsuario =PIdUsuario ;
				
		elseif PAccion='Modificar' then		
				UPDATE Usuarios SET Contrasena = PContrasena, correoElectronico = PcorreoElectronico , activo = Pactivo, privilegio=Pprivilegio, 
				NombreUsuario = PnombreUsuario,Lectura=PLectura  WHERE idUsuario = PIdUsuario;
				
								
		elseif PAccion='insert' then		
				INSERT INTO Usuarios ( idUsuario, contrasena, idTipoUsuario, correoElectronico, activo, fechaRegistro, privilegio,
				nombreUsuario, NoEmpleado,Lectura,ingsesion) VALUES (PIdUsuario, PContrasena, PiDTipoUsuario,PcorreoElectronico, 
				Pactivo,PfechaRegistro, Pprivilegio,PnombreUsuario, PnoEmpleado,PLectura,0);
				
		elseif PAccion='InsertUsrSession' then								
				insert into usuariosession (NoEmpleado,Idstatus,sesionActiva) values(PnoEmpleado,'101','N');
		elseif pAccion = 'Tiendas' then
				select concat(cnTienda,'-',cncompania) as cntienda,dxTienda from vales.catalogo_tiendas;
		elseif pAccion='InsertUsrTda' then
                set pSociedad = (select sociedad from vales.catalogo_tiendas where cntienda = SUBSTRING_INDEX(pCnTienda,'-',1));
				INSERT INTO  UsuarioTienda values(PIdUsuario,SUBSTRING_INDEX(pCnTienda,'-',1),SUBSTRING_INDEX(pCnTienda,'-',-1), pSociedad);
		elseif pAccion='UpdateUsrTda' then
				set pSociedad = (select sociedad from vales.catalogo_tiendas where cntienda = SUBSTRING_INDEX(pCnTienda,'-',1));
                set c = (Select count(*) from UsuarioTienda where idUsuario = PIdUsuario);
                if c = 0 then
					INSERT INTO  UsuarioTienda values(PIdUsuario,SUBSTRING_INDEX(pCnTienda,'-',1),SUBSTRING_INDEX(pCnTienda,'-',-1), pSociedad);
                else
					Update UsuarioTienda set cntienda = SUBSTRING_INDEX(pCnTienda,'-',1),cncompania = SUBSTRING_INDEX(pCnTienda,'-',-1), Sociedad=pSociedad  where idusuario = PIdUsuario;
                end if;
				
		elseif pAccion = 'ConsultaUsrTda' then
				SELECT idUsuario,concat(cnTienda,'-',cncompania) as cntienda from UsuarioTienda where idUsuario = PIdUsuario;
		END IF;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procUsuariosGrupoUsuarios` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procUsuariosGrupoUsuarios`(IN `PidUsuario` VARCHAR(50), IN `PidGrupo` VARCHAR(50), IN `PXml` VARCHAR(50), IN `PAccion` VARCHAR(50))
BEGIN
	IF PAccion='Consulta' then
				SELECT IdGrupo, IdUsuario FROM UsuariosGrupoUsuarios WHERE IdGrupo = PidGrupo;
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procusuariosmodulos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procusuariosmodulos`(IN `PidUsuario` VARCHAR(50), IN `PidSistema` VARCHAR(50), IN `PidModulo` VARCHAR(50), IN `PidGrupo` VARCHAR(50), IN `Pactivo` VARCHAR(50), IN `PXml` VARCHAR(50), IN `PAccion` VARCHAR(50))
BEGIN
		if PAccion='Consulta' then
				Select IdModulo From UsuariosModulos where IdSistema =PIdSistema and idmodulo=PIdModulo;

		ELSEIF PTipo='Consulta2' then				
				Select IdUsuario From UsuariosModulos where IdUsuario = PidUsuario;

		end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procVariableSesion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `procVariableSesion`(IN `pSID` varCHAR(36), IN `pVariable` varCHAR(30), IN `pValor` varCHAR(250))
    COMMENT 'Procedimiento para almacenar variable de sesion'
BEGIN
	DECLARE Existe INT;
	
	SELECT EXISTS (SELECT * FROM variableSesion WHERE idSesion = pSID AND variable = pVariable) INTO Existe;
	
	IF (Existe = 0) THEN
		INSERT INTO variableSesion (idSesion, variable, valor)
		VALUES (pSID, pVariable, pValor);
	ELSE
		
		UPDATE variableSesion SET valor = pValor
		WHERE idSesion = pSID AND variable = pVariable;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proc_CorreoDM` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `proc_CorreoDM`()
BEGIN
select correoelectronico from usuarios where idtipousuario = 'd';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proc_CrearUsuariosAuditoria` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `proc_CrearUsuariosAuditoria`(pcnEmpleado varchar(8),pdxempleado varchar(1000),pcnpuesto01 varchar(100), pdxpuesto01 varchar(100))
begin

if not  exists (select * from sfseguridad.usuarios where noEmpleado = pcnEmpleado ) then
	insert into usuarios (idUsuario,contrasena, idTipoUsuario, correoElectronico,activo,fechaRegistro,privilegio,nombreUsuario,noEmpleado,IngSesion,Lectura,idUsuarioAutoriza,CentroCostos,Puesto,Descripcion)
				values(pcnEmpleado,CONCAT('control',pcnEmpleado),'C', null,1,now(),null,pdxempleado,pcnEmpleado,1,0,null,null,pcnpuesto01,pdxpuesto01);
end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `proc_CRUDMovil` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `proc_CRUDMovil`(PcveUsuarioRed varchar(50),Pidsistema varchar(10),
PusuarioSistema varchar(50),Pstatus varchar(10), Paccion int,pIdUsuario_Movil int)
begin 

  IF(Paccion = 1) THEN 
    SELECT ' -1' AS 'ID', ' --Seleccione--' as 'DESC'
     UNION ALL 
	SELECT  IDSISTEMA AS 'ID', NOMBRESISTEMA AS 'DESC' FROM SISTEMAS; 
  ELSEIF (Paccion = 2) THEN 
	IF NOT EXISTS(SELECT * FROM usuario_Movil WHERE cveUsuarioRed=PcveUsuarioRed AND IDSISTEMA = Pidsistema AND usuarioSistema = PusuarioSistema) THEN 
		INSERT INTO usuario_Movil(cveUsuarioRed,idsistema,usuarioSistema,status) VALUES(PcveUsuarioRed,Pidsistema,PusuarioSistema,Pstatus); 
		SELECT 'NO EXISTE'; 
	ELSE
		SELECT 'EXISTE'; 
	END IF;  
     
  ELSEIF (Paccion = 3) THEN 
	SELECT * FROM usuario_Movil  ORDER BY CVEUSUARIORED; 
  ELSEIF (Paccion = 4) THEN 
	SELECT * FROM usuario_Movil WHERE IDUSUARIO_MOVIL = pIdUsuario_Movil  ORDER BY CVEUSUARIORED; 
  ELSEIF(Paccion = 5) THEN 
    UPDATE usuario_Movil SET cveUsuarioRed = PcveUsuarioRed, idsistema = Pidsistema,  usuarioSistema = PusuarioSistema, status = Pstatus
    WHERE IDUSUARIO_MOVIL = pIdUsuario_Movil; 
  ELSEIF (Paccion = 6) THEN 
	SELECT * FROM usuario_Movil where cveUsuarioRed = PcveUsuarioRed   ORDER BY CVEUSUARIORED; 
  END IF; 
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `prueba` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `prueba`()
BEGIN
IF exists(select * from usuario) then
		select 'si existe';
else
		select 'no existe';

end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SelUsuarios` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `SelUsuarios`()
BEGIN 
	Select IdProveedor, CONCAT (IdProveedor , IdUsuario)  AS Clave, IdUsuario From Usuarios; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SPcomentariosbajas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `SPcomentariosbajas`(IN `PIdComentario` VARCHAR(50), IN `PIdUsuarioBaja` VARCHAR(50), IN `PIdUsuarioAdm` VARCHAR(50), IN `PFecha` VARCHAR(50), IN `PComentario` VARCHAR(100), IN `PAccion` VARCHAR(50))
BEGIN
		insert into comentariosbajas(IdUsuarioBaja,IdUsuarioAdm,Fecha,Comentario) values(PIdUsuarioBaja,PIdUsuarioAdm,now(),PComentario);		
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spConsultaPerfil` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `spConsultaPerfil`(IN `idUsuarioparam` VARCHAR(10))
BEGIN
	SELECT idTipoUsuario from usuarios WHERE idUsuario = idUsuarioparam;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spConsultaPerfilSistemaVales` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`USERDES`@`10.0.2.3%` PROCEDURE `spConsultaPerfilSistemaVales`(IN `idUsuarioparam` VARCHAR(10))
begin

	SELECT idTipoUsuario from usuarios WHERE idUsuario = idUsuarioparam;

	SELECT cnTienda, sociedad from usuariotienda WHERE idUsuario = idUsuarioparam;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-25 12:59:37
