import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Logout from './views/Logout.vue'
import SolicitudReservaciones from '@/views/SolicitudReservaciones/SolicitudReservaciones.vue'
import DetalleSolicitudes from '@/views/SolicitudReservaciones/DetalleSolicitudes.vue'
import EstatusSolicitud from "@/views/EstatusSolicitud/EstatusSolicitud.vue";
import Reportes from '@/views/Reportes/Reportes.vue'
import VueloHospedaje from "@/views/SolicitudReservaciones/TipoSolicitud/VueloHospedaje.vue";
import Vuelo from "@/views/SolicitudReservaciones/TipoSolicitud/Vuelo.vue";
import Hospedaje from "@/views/SolicitudReservaciones/TipoSolicitud/Hospedaje.vue";
import AltaUsuario from "@/views/AltaUsuario.vue";
import Perfil from "@/views/Perfil.vue";
import Configuracion from "@/views/Configuracion.vue";
import DetalleSolicitudVueloHospedaje from "@/views/DetalleSolicitudes/DetalleSolicitudVueloHospedaje.vue";
import DetalleSolicitudVuelo from "@/views/DetalleSolicitudes/DetalleSolicitudVuelo.vue";
import DetalleSolicitudHospedaje from "@/views/DetalleSolicitudes/DetalleSolicitudHospedaje.vue";
import CargaImagenes from "@/views/CargaImagenes.vue";
import AdministradorSolicitudes from "@/views/AdministradorSolicitudes/AdministradorSolicitudes.vue";
import DetalleSolicitudVueloId from "@/views/DetalleSolicitudes/DetalleSolicitudVueloId.vue"
import DetalleSolicitudHospedajeId from "@/views/DetalleSolicitudes/DetalleSolicitudHospedajeId.vue"
import DetalleSolicitudVueloHospedajeId from "@/views/DetalleSolicitudes/DetalleSolicitudVueloHospedajeId.vue";
import SeleccionPropuesta from "@/views/SeleccionPropuesta/SeleccionPropuesta.vue";
import OperacionesVuelo from "@/views/Operaciones/OperacionesVuelo.vue";
import OperacionesHospedaje from "@/views/Operaciones/OperacionesHospedaje.vue";
import AutorizadorSolicitudes from "@/views/Autorizador/AutorizadorSolicitudes.vue";
import SeleccionarPrioridades from "@/views/Autorizador/Prioridades/SeleccionarPrioridades.vue";
import CompraCotizacion from "@/views/AdministradorSolicitudes/CompraCotizacion.vue";

import store from './store';

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [ 
    { path: "/SistemaReservaciones/", component: Home, meta: { requiresAuth: true } },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout,
      meta: { requiresAuth: true }
    },
    {
      path: '/menu',
      name: 'home',
      component: Home,
      meta: { requiresAuth: true }
    },
    {
      path: '/solicitud-de-reservaciones',
      name: 'solicitud-de-reservaciones',
      component: SolicitudReservaciones,
      meta: { requiresAuth: true },
    },
    {
      path: '/estatus-de-solicitud',
      name: 'estatus-de-solicitud',
      component: EstatusSolicitud,
      meta: { requiresAuth: true }
    },
    {
      path: '/reportes',
      name: 'reportes',
      component: Reportes,
      meta: { requiresAuth: true }
    },
    {
      path: '/vuelo-hospedaje',
      name: 'vuelo-hospedaje',
      component: VueloHospedaje,
      meta: { requiresAuth: true },
    },
    {
      path: '/vuelo-hospedaje/:id',
      name: 'vuelo-hospedajeId',
      component: VueloHospedaje,
      meta: { requiresAuth: true },
    },
    {
      path: '/vuelo',
      name: 'vuelo',
      component: Vuelo,
      meta: { requiresAuth: true },
    },
    {
      path: '/vuelo/:id',
      name: 'vueloId',
      component: Vuelo,
      meta: { requiresAuth: true },
    },
    {
      path: '/hospedaje',
      name: 'hospedaje',
      component: Hospedaje,
      meta: { requiresAuth: true },
    },
    {
      path: '/hospedaje/:id',
      name: 'hospedajeId',
      component: Hospedaje,
      meta: { requiresAuth: true },
    },
    {
      path: '/perfil',
      name: 'perfil',
      component: Perfil,
      meta: { requiresAuth: true }
    },
    {
      path: '/alta-usuarios',
      name: 'alta-usuarios',
      component: AltaUsuario,
      meta: { requiresAuth: true }
    },
    {
      path: '/detalle-solicitudes',
      name: 'detalleSolicitudes',
      component: DetalleSolicitudes,
      meta: { requiresAuth: true },
      beforeEnter: (to, from, next) => {
        const currentCookie = JSON.parse(Vue.cookie.get("cookieControlViajes"))
        if (currentCookie.intIDPerfil != 2) {
          if (currentCookie.intIDPerfil != 5) {
            next({
              name: 'home'
            })
          } else {
            next();
          }
        } else {
          next();
        }
      }
    },
    {
      path: '/configuracion',
      name: 'configuracion',
      component: Configuracion,
      meta: { requiresAuth: true },
      beforeEnter: (to, from, next) => {
        const currentCookie = JSON.parse(Vue.cookie.get("cookieControlViajes"))
        if (currentCookie.intIDPerfil != 2) {
          if (currentCookie.intIDPerfil != 5) {
            next({
              name: 'home'
            })
          } else {
            next();
          }
        } else {
          next();
        }
      }
    },
    {
      path: '/detalle-vuelo-hospedaje',
      name: 'detalleVueloHospedaje',
      component: DetalleSolicitudVueloHospedaje,
      meta: { requiresAuth: true }
    },
    {
      path: '/detalle-vuelo',
      name: 'detalleVuelo',
      component: DetalleSolicitudVuelo,
      meta: { requiresAuth: true }
    },
    {
      path: '/detalle-hospedaje',
      name: 'detalleHospedaje',
      component: DetalleSolicitudHospedaje,
      meta: { requiresAuth: true }
    },
    {
      path: '/carga-de-imagenes',
      name: 'cargaImagenes',
      component: CargaImagenes,
      meta: { requiresAuth: true },
      beforeEnter: (to, from, next) => {
        const currentCookie = JSON.parse(Vue.cookie.get("cookieControlViajes"))
        if (currentCookie.intIDPerfil != 2) {
          if (currentCookie.intIDPerfil != 5) {
            next({
              name: 'home'
            })
          } else {
            next();
          }
        } else {
          next();
        }
      }
    },
    {
      path: '/administrador-solicitudes',
      name: 'administrador-solicitudes',
      component: AdministradorSolicitudes,
      meta: { requiresAuth: true },
      beforeEnter: (to, from, next) => {
        const currentCookie = JSON.parse(Vue.cookie.get("cookieControlViajes"));
        
        if (currentCookie.intIDPerfil != 2) {
          if (currentCookie.intIDPerfil != 5) {
            next({
              name: 'home'
            })

          } else {
            next();
          }
        } else {
          next();
        }
      }
    },
    {
      path: '/detalle-solicitud-vuelo/:id/:idDetalle',
      name: 'detalleSolicitudVueloId',
      component: DetalleSolicitudVueloId,
      meta: { requiresAuth: true }
    },
    {
      path: '/detalle-solicitud-hospedaje/:id/:idDetalle',
      name: 'detalleSolicitudHospedajeId',
      component: DetalleSolicitudHospedajeId,
      meta: { requiresAuth: true }
    },
    {
      path: '/detalle-solicitud-vuelo-hospedaje/:id/:idDetalle',
      name: 'detalleSolicitudVueloHospedajeId',
      component: DetalleSolicitudVueloHospedajeId,
      meta: { requiresAuth: true }
    },
    {
      path: '/seleccion-propuestas',
      name: 'seleccionHospedaje',
      component: SeleccionPropuesta,
      meta: { requiresAuth: true },
    },
    {
      path: '/reserva-vuelo',
      name: 'operacionVuelo',
      component: OperacionesVuelo,
      meta: { requiresAuth: true }
    },
    {
      path: '/reserva-hospedaje',
      name: 'operacionHospedaje',
      component: OperacionesHospedaje,
      meta: { requiresAuth: true }
    },
    {
      path: '/autorizador-solicitudes',
      name: 'autorizadorSolicitudes',
      component: AutorizadorSolicitudes,
      meta: { requiresAuth: true },
      beforeEnter: (to, from, next) => {
        const currentCookie = JSON.parse(Vue.cookie.get("cookieControlViajes"))
        if (currentCookie.intIDPerfil != 3) {
          if (currentCookie.intIDPerfil != 5) {
            next({
              name: 'home'
            })
          } else {
            next();
          }
        } else {
          next();
        }
      }
    },
    {
      path: '/prioridad-autorizador',
      name: 'prioridad-autorizador',
      component: SeleccionarPrioridades,
      meta: { requiresAuth: true }
    },
    {
      path: '/compra-cotizacion',
      name: 'compra-cotizacion',
      component: CompraCotizacion,
      meta: { requiresAuth: true }
    },
  ],
  scrollBehavior() {
    return { x: 0, y: 0 }
  }
})

router.beforeEach((to, from, next) => {
  const rutaProtegida = to.matched.some(record => record.meta.requiresAuth);
  const user = store.state.userLogged;

  if (rutaProtegida) {
    if (user === false) {
      next({
        name: 'login'
      })
    } else {
      next();
    }
  } else {
    next();
  }
})


export default router;
